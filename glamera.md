## Glamera  Application
## App url 
- http://localhost:2228/
- http://192.168.1.100:3000/
# # contant-type: 
- `application/json`
## Used end points 
- api/Client/GlameraClientProfile `  get client profile call it in home view if user login and call it after user login`
- api/Client/Update  `  call it inside edit profile for client`
- api/AccountSetup/GetBasic `  call when use search on salons depend on search criteria`
- api/Employee/GetBasic `  call to get employee in salon call it when schedule service `
- api/Account/RegisterGlameraUser `  call in register glamera user` 
- api/Account/VerifyPhoneNumber  `  call in verification code `
- api/Account/GlameraChangePassword  ` change password `
- api/Account/ResendVerifyCode   `  resend code verification `
- api/Account/GlameraForgetPassword   ` forget password `
- api/Booking/GlameraCreate  `  create booking `
- api/Booking/Update  `  updateBooking  `
- api/Booking/GlameraGet  `  getMy booking `
- api/Booking/GetUnRatedByClient  `  get My UnRated  booking  is    the same   as   Booking/GlameraGet `
- api/Booking/GlameraBookingDetail  `    get detail for booking `
- api/Ranks/Create    `   create rank for employee or salon or service` 
- api/BookingService/GetEmployeeTimeSlots       `  getHourSlots` 
- api/SystemCodes/GetDropdowns  `  getDropDowens lockup  data ` 
- api/GlameraHome/GetOffersAndMostPopular   `  home view to get most populare and offers `
- api/Category/GlameraGategories  `  get Catgories In Salon with services `  
- api/ClientFavorites/Create  `   to add favourite ` 
- api/ClientFavorites/Delete   `  to delete favourite  ` 
- api/ClientFavorites/GetFavAccounts   `   getIds Salons for favourite ` 
- api/Booking/CancelBooking  `  update Booking Status by   Canceling Booking ` 
- api/OffersSetting/GetAccountOffers  `   getOffers `
- api/OffersSetting/GetPackageById  `   get Service exist  in packages  ` 
- api/Account/contactus  `   contact us ` 
- api/ClientMemberships/Get `  client membership `
- api/ClientPointsBalance/MembershipPointsHistory  ` get Card History `
- api/ClientsOffers/Get `  get Voucher And Gift Card  ` 
- api/GlameraUserNotification/Get  `  get All Notifcation By Client `
- api/Transactions/GetAvilablePaymentMethods   `   get Available Payment methods  to choose  from  while  payment ` 
- api/AcceptPayment/GetAuthToken `  getAuthToken ` 
- api/AcceptPayment/GetOder `  getPaymentToken  ` 
- api/Client/GlameraClientProfile `   get Client Profile  `
- api/client/UpdateProfileSMS `  update Client Profile SMS && Notifcation flags `
- api/client/UpdateImage `  update client image `
- api/WorkingHours/CheckDay   `  checkday if  day   is  off  
- api/BlogPost/GlameraBlog   `  get blog  post s   `
- api/BlogPostComment/get   `  get blog  post comments    `
- api/Booking/UpdateBookingStatus   `  UpdateBookingStatus    `
- api/WorkingHours/GetAccountOffDays   `  Get Account Off Days    
- api/Ranks/GetAccountRankDetail   `  Get account reviews    
- api/GlameraHome/GetOfferBranches   ` Get Offer Branches   
- api/GlameraHome/GBHotLine   ` Get Hot Line 
- api/Account/CheckGlameraPhone   ` check if  glamera user  phone  not taken 
- api/GlameraHome/PopUpData   ` get  PopUpData that  should be visible  
- api/GlameraHome/GetGlameraOffers` Get  Offers  
- api/GlameraHome/GetGlameraPackages` Get  Packages  




## Register 
- api : `api/Account/RegisterGlameraUser`
- Method: `post`
	
- Example :
		`{
  "Email": "",
  "Gender": 1,
  "FullName": "redae",
  "Password": "P@$$w0rd",
  "MobileNumber": "01100797944",
  "CountryCode":"+20"
}
`
- Response : 
	- *faild*:
	`{
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "",
            "Meta": "Name 01100797944 is already taken.",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 107,
    "ValidationResult": null
}`
	- *success*:
	`{
    "Result": {
        "UserId": 10,
        "UserName": "01100797944",
        "AccountSetupId": 0,
        "EmployeeId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`



## Verify Phone Number  :
- api : `api/Account/VerifyPhoneNumber`
- Method: `post`
- parameters:
	- UserId [`Optional  if  you  don't send i will  get  user  by phone number  as  user name   `] 
	- PhoneNumber [`require `]
	- Code	[`require `]
	
- Example :
		`{
		    "UserId": 10,
  			"PhoneNumber": 01100797944,
  			"Code": 900328
			}
		`
- Response : 
	- *faild*:
	`{
    "Result": false,
    "DetailedMessages": [
        {
            "Property": "",
           "Meta": "Failed to verify phone",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 107,
    "ValidationResult": null
	}
	`
	- *success*:
	`{
    "Result": {
        "UserId": 2098,
        "UserName": null,
        "AccountSetupId": 0,
        "EmployeeId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
	}
	` 
## Token  :
- api : `token`
- Method: `post`

- Example :
		`username=01100797944&password=L4X8BLZT&grant_type=password&accountSetupId=null&glameraUser=1
		`
        `AccessToken=207762332348562&glameraUser=1&grant_type=password&ProviderType=1`
- Response : 
	- *faild*:
	`{
    "error": "1",
    "error_description": "The user name  is incorrect."
     }
	`
    `{
    "error": "4",
    "error_description": "token not   exist ."
     }
	`
    `{
    "error": "2",
    "error_description": "The password is incorrect.."
}
	`
     `{
    "error": "3",
    "error_description": "You need to confirm your phone number."
}
	`
    {
    "access_token": "FLGpgYo3RiHnvvYtPYGpKMoRzFxfTivXMIi8WL6WDGPCLZg34ugg4dxi0EkyxhZoVHEvAKxh4_SFkHNp7y0Yj6oHLUe8gCbi7SKaK5bGlYwvBc7cuKupQ_8Vv19aDzOUo5MWgh2Fip2PuagYgRcxHbjsXzqphGoin2bTAgWYOEsU6Uxj_xX4vxBbplkJm2iau1L7401Gq4ZLQ4tetehBsU4iXk7v47cdUwLkbKb8-Wv2Y-1thNHGwTGl23HpZJN7LuXDZaw9f5oVWdpD6cRucC2gRbcYMK0lVZFzG8hyt6jrcN3dN4YomGY3aCE718r7IIeMVFlanW7oIiqeOq_1zl7BU7ct3ZHfshHZbsl5pGTY02PZYBYGFl4xVwoeODbYv6ZUc9ueyWsbSyzSVLZuiefh7ONQQjyoCdNHl93nxZnvWaGMCydBcuq-ifZD9IxmhitObulDXLg1QAu5XelHm3sxq6I3qAPXFFyt8DURPDKuledU7S1ICSDp_wMYSzPbc3-QhK9ColltgjQIeBXe7Q",
    "token_type": "bearer",
    "expires_in": 1209599,
    "userName": "01060499088",
    "userId": "1046",
    "clientId": "4340",
    "countryId": "",
    ".issued": "Sun, 09 Jun 2019 08:21:46 GMT",
    ".expires": "Sun, 23 Jun 2019 08:21:46 GMT"
}
}
	` 

## Forgot password:
- Api : `api/Account/GlameraForgetPassword`
- Method: `post`

- Example :
		`{
    "UserName": "01100797944",
    "MobileNumber": "01100797944"
   }
	`
- Response : 
	- *faild*:
	` {
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "",
            "Meta": "No user with such name. ",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 107,
    "ValidationResult": null
}
{
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "",
            "Meta": "Phone number you entered  and   user  phone number dosen't match .",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 107,
    "ValidationResult": null
}
`
  - *success*:
	`{
    "Result": {
        "UserId": 10,
        "UserName": null,
        "AccountSetupId": 0,
        "EmployeeId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
    }
	` 
## Glamera Change Password:
- Api : `api/Account/GlameraChangePassword`
- Method: `post`

- Example :
		`{
    "UserName": "01100797944",
    "MobileNumber": "01100797944"
   }
	`
- Response : 
	- *faild*:
	` {
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "InCorrectPassword",
            "Meta": "Incorrect password.",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 102,
    "ValidationResult": null
}
`
  - *success*:
	`{
    "Result": {
        "UserId": 10,
        "UserName": "01100797944",
        "AccountSetupId": 0,
        "EmployeeId": null,
        "Token": "iIzgVPiHcAEsOLXjFn2wHnnEJVS7gRtSGpXU7EwiuOQPrEBXYppj8hFF6V0SSOrsV/7P0AvGL98BzJqNkJmANF4O3Jpgugu1NbkcjFpwbUH2quGRTjn/Xb7fVJx8eqaf6OHqugA5Ck6+K5FcwJKy1A=="
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
	` 

## Resend Verify Phone Code:
- Api : `api/Account/ResendVerifyCode`
- Method: `post`
- parameters:
    - UserId [`Optional  if  you  don't send i will  get  user  by phone number  as  user name   `] 
	- PhoneNumber [`require `]
    - CountryCode [`require `]
- Example :
		`{"PhoneNumber":"01100797944","UserId": null , "CountryCode":"+20"}
	`
- Response : 
	- *faild*:
	` `
	- *success*:
	`{
    "Result": {
        "UserId": 1036,
        "UserName": null,
        "AccountSetupId": 0,
        "EmployeeId": null,
        "Token": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
	` 
    
## Delete Glamera User:
- Api : `api/Account/DeleteGlameraUser`
- Method: `post`
- parameters:
   
	- UserName [`require `]
   
- Example :
		`{ "UserId": null, "UserName": "2033666658" }
	`
- Response : 
	- *faild*:
	` `
	- *success*:
	`{
    "Result": null,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
	` 

## Get Glamera Client Profile data:
- Api : `api/Client/GlameraClientProfile`
- Method: `post`

- Example :
		`30
        `
- Response : 
	- *faild*:
	``
	- *success*:
	`{
    "Result": {
        "Image": null,
        "Name": "yousef shaaban",
        "PostCode": null,
        "Occupation": null,
        "Refreshment": null,
        "PreferredEmployee": null,
        "PreferredEmployeeID": null,
        "Status": 1,
        "Notes": null,
        "CardNumber": null,
        "Allergies": false,
        "AllergiesComment": null,
        "ArthritisOrRheumatism": false,
        "Asthma": false,
        "BloodPressure": false,
        "Cancer": false,
        "Diabetes": false,
        "DoctorName": null,
        "DoctorNumber": null,
        "Epilepsy": false,
        "FirmMassage": false,
        "Heart": false,
        "Hormone": false,
        "Hysterectomy": false,
        "Implants": false,
        "Medical": null,
        "Others": false,
        "OtherComment": null,
        "PatchTestdate": null,
        "PatchTestDone": false,
        "Pregnant": false,
        "PregnantComment": null,
        "SensitiveSkin": false,
        "SkinColor": null,
        "SkinType": null,
        "Smoke": false,
        "SmokeComment": null,
        "Surgery": false,
        "SurgeryComment": null,
        "Thyroid": false,
        "Veins": false,
        "Water": false,
        "ReminderSMS": false,
        "ReminderEmail": false,
        "DoNotMarket": false,
        "LastMinuteBookingList": false,
        "CustomFieldOneKey": null,
        "CustomFieldOneValue": null,
        "CustomFieldTwoKey": null,
        "CustomFieldTwoValue": null,
        "CustomFieldThreeKey": null,
        "CustomFieldThreeValue": null,
        "CustomFieldFourKey": null,
        "CustomFieldFourValue": null,
        "CustomFieldFiveKey": null,
        "CustomFieldFiveValue": null,
        "CustomFieldSixKey": null,
        "CustomFieldSixValue": null,
        "CustomFieldSevenKey": null,
        "CustomFieldSevenValue": null,
        "CustomFieldEightKey": null,
        "CustomFieldEightValue": null,
        "CustomFieldNineKey": null,
        "CustomFieldNineValue": null,
        "CustomFieldTenKey": null,
        "CustomFieldTenValue": null,
        "UserName": null,
        "Password": null,
        "ImagePath": null,
        "AddedDate": "2019-02-18T21:08:05.643",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": null,
        "ModifyUserId": null,
        "ReceiveNotifications": null,
        "ReceiveSMS": null,
        "Bookings": null,
        "AccountSetupClients": [
            {
                "Id": 2055,
                "AccountSetupId": 14,
                "ClientId": 30
            }
        ],
        "Id": 30,
        "NameAr": "yousef shaaban",
        "NameEn": "yousef shaaban",
        "Phone": null,
        "Mobile": "201060499088",
        "WorkNumber": null,
        "Email": null,
        "StateOrGovernorateId": 0,
        "AreaOrRegionId": 0,
        "Address": null,
        "BirthDate": null,
        "DOBDay": null,
        "DOBMonth": null,
        "DOBYear": null,
        "Gender": 1,
        "SearchName": "yousefshaaban",
        "RankScore": 3
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`

## Update  Client Profile data (ReceiveSMS && ReceiveNotifications ) :
- Api : `api/Client/UpdateProfileSMS`
- Method: `post`

- Example :
		`{"ReceiveSMS":1, "ReceiveNotifications": 1}
        `
- Response : 
	- *faild*:
	``
	- *success*:
	`{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`


## Get account  Basic data:
- Api : `api/AccountSetup/GetBasic`
- Method: `post`
- All Search Criteria : `{
  "Id": null,
  "FavoritesAccounts": false,
  "HasOffer": false,
  "ClientId": null,
  "Latitude": null,
  "Longitude": null,
  "ServiceLocation": null,
  "RankScore": null,
  "CountryId": null,
  "CityId": null,
  "AreaId": null,
  "TypeOfSalon": null,
  "ClassId": null,
  "CompanyId": null,
  "PriceFrom": null,
  "PriceTo": null,
  "Distance": null,
  "BusinessType":null
}

`

- Example :
		`{"Id":3096}
        `
        `{"Latitude":30.0593591 , "Longitude":31.3420736}`
- Response : 
	- *faild*:
	``
	- *success*:
	`{
    "AccountSetup": null,
    "AccountSetupBasic": [
        {
            "Id": 12,
            "SiteName": "Mon Etoile Salon de Coiffeur",
            "PhoneNumber": null,
            "MobileNumber": "01265222789",
            "Email": null,
            "WebsiteAddress": null,
            "CompanyName": "Mon Etoile Salon de Coiffeur",
            "Status": 0,
            "RankScore": null,
            "Latitude": 30.0444,
            "Longitude": 31.2364,
            "CityId": 1044,
            "AreaId": 1216,
            "HaseInHomeService": null,
            "ServedGender": null,
            "HasTaxiService": null,
            "MostPopular": null,
            "MakeupArtist": 0,
            "Address": "NADA Compound October 6, Al Jizah, Egypt ",
            "CountryId": 909,
            "HasOffer": false,
            "IsFavorite": false,
            "WorkingTime": {
                "StartTime": "09:30:00",
                "EndTime": "23:30:00"
            },
            "DefaultCurrency": null,
            "ActiveClassifications": [],
            "AccountImages": [],
            "Distance": 10314.810290875485,
            "AddedDate": "2019-05-12T14:13:49.4962139+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": null,
            "ModifyUserId": null
        }
    ],
    "AccountSetupSummarized": null,
    "AccountMessageSetting": null,
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}`
    
## Get User  Branches:
- api : `api/Company/getBranches`
- Method: `post`
- parameters:
	- UserName [` `]   
	
- Example :
		`"yu123" `
- Response : 
	- *faild*:
	``
	- *success*:
	`{
  "Result": [
    {
      "RegionTimeZoneId": 60,
      "RegionTimeZone": null,
      "CultureInfoId": 15,
      "Culture": null,
      "SiteNameCulture": null,
      "AddressCulture": null,
      "Country": null,
      "PostCode": null,
      "ManagerFirstName": null,
      "ManagerFirstNameCulture": null,
      "ManagerLastName": null,
      "ManagerLastNameCulture": null,
      "CompanyNameCulture": null,
      "CompanyAddress": null,
      "CompanyAddressCulture": null,
      "CompanyRegionId": 0,
      "Regions": null,
      "CompanyStateId": 0,
      "States": null,
      "CompanyPostCode": null,
      "CompanyPhoneNumber": null,
      "CompanyMobileNumber": null,
      "CompanyEmail": null,
      "CompanyWebSiteAddress": null,
      "CompanyOwnerFirstName": null,
      "CompanyOwnerFirstNameCulture": null,
      "CompanyOwnerLastName": null,
      "CompanyOwnerLastNameCulture": null,
      "SunStartTime": null,
      "MonStartTime": null,
      "TuesStartTime": null,
      "WednesStartTime": null,
      "ThursStartTime": null,
      "FriStartTime": null,
      "SatStartTime": null,
      "SunEndTime": null,
      "MonEndTime": null,
      "TuesEndTime": null,
      "WednesEndTime": null,
      "ThursEndTime": null,
      "FriEndTime": null,
      "SatEndTime": null,
      "IsSunOff": false,
      "IsMonOff": false,
      "IsTuesOff": false,
      "IsWednesOff": false,
      "IsThursOff": false,
      "IsFriOff": false,
      "IsSatOff": false,
      "SpecificDays": null,
      "AllowTransactionVoiding": false,
      "TaxSystemOneId": null,
      "TaxOneRate": null,
      "TaxOneOrderInBill": null,
      "TaxSystemOne": null,
      "TaxSystemTwoId": null,
      "TaxTwoRate": null,
      "TaxTwoOrderInBill": null,
      "TaxSystemTwo": null,
      "TaxSystemThreeId": null,
      "TaxThreeRate": null,
      "TaxThreeOrderInBill": null,
      "TaxSystemThree": null,
      "AllowDeposit": false,
      "AccountCurrencies": null,
      "EnableTradingSystem": false,
      "DeclareTheRegistersBeforeTrading": null,
      "DeclareTheRegistersAfterTrading": null,
      "PctPaidForServicePerformed": 0,
      "PctPaidForProductSold": 0,
      "GiveLoyaltyDollarsPointsToYourClientsOnEveryTransaction": false,
      "NewClientsAreIncludedInTheLoyalitySystem": false,
      "ServicePointsAndDollarsAreGivenForEveryXDollarSpent": 0,
      "ServiceLoyaltyPointsGivenForEveryXDollarSpent": 0,
      "ServiceLoyaltyMoneyGivenForEveryXDollarSpent": 0,
      "ProductPointsAndDollarsAreGivenForEveryXDollarSpent": 0,
      "ProductLoyaltyPointsGivenForEveryXDollarSpent": 0,
      "ProductLoyaltyMoneyGivenForEveryXDollarSpent": 0,
      "LoyaltyPointsAreGivenPerVisit": 0,
      "LoyaltyMoneyAreGivenPerVisit": 0,
      "XPointsCount": null,
      "XPointsValue": null,
      "DashboardDisplayAll": false,
      "DashboardUpcomingAppointments": false,
      "DashboardPendingOnlineBookings": false,
      "DashboardSalesGraphs": false,
      "DashboardTasks": false,
      "DashboardWaitingLists": false,
      "PrintReceiptAdvertText": null,
      "PrintReceiptAdvertTextCulture": null,
      "PrintReceiptIncludeClientphoneNumber": false,
      "MessageOnNewBooking": false,
      "MessageOnReminder": false,
      "MessageOnBirthDay": false,
      "MessageOnWelcome": false,
      "MessageOnRetention": false,
      "DoYouHaveReceiptPrinter": false,
      "DoYouHaveCashDrawer": false,
      "WorkingHours": null,
      "BusinessSize": 49,
      "BusinessType": 1,
      "CompanyId": 2064,
      "Company": null,
      "Id": 2074,
      "SiteName": null,
      "PhoneNumber": null,
      "MobileNumber": null,
      "Email": null,
      "WebsiteAddress": null,
      "CompanyName": null,
      "State": 1,
      "RankScore": null,
      "Latitude": null,
      "Longitude": null,
      "CityId": null,
      "AreaId": null,
      "HaseInHomeService": null,
      "ServedGender": null,
      "HasTaxiService": null,
      "MostPopular": null,
      "MakeupArtist": 0,
      "Address": null,
      "CountryId": 910,
      "DefaultStartTime": null,
      "DefaultEndTime": null,
      "DefaultCurrency": null,
      "ActiveClassifications": null,
      "AccountImages": null,
      "AddedDate": "2019-03-31T12:12:49.293",
      "ModifiedDate": null,
      "IPAddress": null,
      "AddUserId": null,
      "ModifyUserId": null
    }
  ],
  "DetailedMessages": null,
  "Code": null,
  "Exception": null,
  "State": 100,
  "ValidationResult": null
} ` 

## Get App Services :
- api : `/api/AppServices/Get`
- Method: `post`
- parameters:
	
	
- Example :
		`{}`
- Response : 
	- *faild*:
	``
	- *success*:
	`{
  "AppServicess": [
    {
      "Id": 382,
      "NameAr": "قص أظافر",
      "NameEn": "قص أظافر",
      "CategoryId": 45,
      "BookingMinutes": 15,
      "ProcessingMinutes": null,
      "FinishingMinutes": null,
      "Code": "cc10",
      "RowStatus": 1,
      "Price": 0
    }
  ],
  "TotalCount": 191,
  "DetailedMessages": null,
  "Code": null,
  "Exception": null,
  "State": 0
}` 

##  Get Account Working Hours :
- Api : `api/WorkingHours/Get`
- Method: `POST`
- parameters:
	- AccountSetupId [`require `] 
- Example :
		`{
        "AccountSetupId":2074
        }`
	- Response : 
	- *faild*:
	` `
	- *success*:
	`{
    "WorkingHoursDTO": [
        {
            "Id": 9491,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 6,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9490,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": true,
            "EmployeeId": null,
            "DayNo": 5,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9489,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 4,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9488,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 3,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9487,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 2,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9486,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 1,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9485,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 0,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        }
    ],
    "TotalCount": 7,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 0
}` 
##  Create Account Working Hours:
- Api : `api/WorkingHours/CreateList`
- Method: `POST`

- Example :
		` [
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 6,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
           
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": true,
            "EmployeeId": null,
            "DayNo": 5,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 4,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 3,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 2,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 1,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 0,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        }
    ]`
    
	- Response : 
	- *faild*:
	` `
	- *success*:
	`{
    "Result": [
        {
            "Id": 9506,
            "AccountSetupId": 2074,
            "DayNo": 6,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9507,
            "AccountSetupId": 2074,
            "DayNo": 5,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": true,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9508,
            "AccountSetupId": 2074,
            "DayNo": 4,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9509,
            "AccountSetupId": 2074,
            "DayNo": 3,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9510,
            "AccountSetupId": 2074,
            "DayNo": 2,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9511,
            "AccountSetupId": 2074,
            "DayNo": 1,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9512,
            "AccountSetupId": 2074,
            "DayNo": 0,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}` 
	
## Get Company Images:
- Api : `/api/CompanyImages/Get`
- Method: `POST`
- parameters:
	- CompanyId [`required `]
- Example :
		`{"CompanyId":10}`
   - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":[{"Id":23,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"45221903_109322770069809_4678415314692603904_n_20190217172822743.jpg","MainImage":null,"SharedForAll":null,"RowStatus":1},{"Id":22,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"48427771_134789634189789_6830310714444349440_n_20190217172757403.jpg","MainImage":null,"SharedForAll":null,"RowStatus":1},{"Id":21,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"gdLKDDcC_400x400_20190217160701495.jpg","MainImage":null,"SharedForAll":null,"RowStatus":1},{"Id":2,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"Capture_20190212180808181.JPG","MainImage":true,"SharedForAll":null,"RowStatus":1},{"Id":1,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"download_20190212165258406.jpg","MainImage":true,"SharedForAll":null,"RowStatus":1}],"TotalCount":5,"DetailedMessages":null,"Code":null,"Exception":null,"State":0}`     

##  Get image  accounts  (where image should appear):
- Api : `api/CompanyImageAccounts/Get`
- Method: `POST`
- parameters:
	- CompanyImagesId [`required `]
- Example :
   {"CompanyImagesId":21}
   
    - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":[{"Id":16,"CompanyImagesId":21,"AccountSetupId":14,"MainFlag":true,"RowStatus":1,"ImageSort":1}],"TotalCount":1,"DetailedMessages":null,"Code":null,"Exception":null,"State":0}`     


##  Delete Image :
- Api : `api/CompanyImages/Delete`
- Method: `POST`
- parameters:
	- CompanyImagesId [`required `]
- Example :
   22
   
    - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":true,"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}`     

##  Post Company Image File :
- Api : `api/Upload/PostImageFile`
- Method: `POST`
- parameters:
	- ImagesFolder [=`CompanyImgs `]
    - AccountSetupId [`Required `]
    - image [`binary `]

 - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":{"FileName":"light-in-the-darkness_20190404094204011.jpg","FolderName":"CompanyImgs"},"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}`  

##  Post Company Image File :
- Api : `api/CompanyImages/Create`
- Method: `POST`
- parameters:
	

- Example :
      `[{"RowStatus":1,"ImageFolder":"CompanyImgs","ImageName":"light-in-the-darkness_20190404094204011.jpg","AccountSetupId":14,"CompanyId":10}]`
 - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":[{"Id":27,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"light-in-the-darkness_20190404094204011.jpg","RowStatus":1,"MainImage":null,"SharedForAll":null,"CompanyImageAccounts":null,"AddedDate":"2019-04-04T09:46:58.5161825+02:00","ModifiedDate":null,"IPAddress":null,"AddUserId":"14","ModifyUserId":null}],"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}`  


##  Post payment  online   :
- Api : `api/Transactions/GlameraPayment`
- Method: `POST`
- parameters:
- Example :
      `{
      "BookingId": 5390,
      "ClientId": 3125,
       "TransactionsPayments": [
      {
        
        "PaymentTypeId": 3,
        "Amount": 4,
        "CurrencyId": -1,
        "VisaTypeId": 2,
        "VisaNumber": "23324554",
        "VisaTypeName": "ALEXBANK ",
        "CurrencyFactor": 1
      }
    ]
    }
    `
 
 - Response : 
	- *faild*:
    
	- *success*:
    ` {
    "Result": {
        "Id": 3998,
        "AccountSetupId": -1,
        "TransactionType": 1,
        "TransactionStatus": 0,
        "TransactionId": null,
        "Code": 0,
        "EnteredById": -1,
        "employee": null,
        "ClientId": 3125,
        "client": null,
        "Date": "2019-04-07T18:56:20.4688578+02:00",
        "CashierDailyId": -1,
        "CashierDaily": null,
        "CashierId": -1,
        "Cashier": null,
        "GiftCardCode": null,
        "TrProductServices": [],
        "excludedTrProductAndService": null,
        "TotalPrice": 0,
        "AmountDue": 0,
        "TotalTaxInc": 0,
        "TransactionsPayments": [],
        "BookingId": 5390,
        "Booking": null,
        "PettyCashItemsId": null,
        "PettyCashItems": null,
        "AddedDate": "2019-04-07T18:56:20.4688578+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": null,
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  Application packages  (أنا عروسه)   :
- Api : `api/AppPackage/Get`
- Method: `POST`
- parameters:
- Example :
      `{
    "BusinessType": 1949,
    "AreaId": 0,
    "CityId": 1036,
    "Lang": "ar"
}
    `
 
 - Response : 
	- *faild*:
    
	- *success*:
    ` {
    "Result": [
        {
            "Id": 1,
            "NameAr": "Glamera Package 1 ar ",
            "NameEn": "Glamera Package 1 en ",
            "FullDescAr": "Glamera Package 1 desc ar",
            "FullDescEn": "Glamera Package 1 desc en",
            "StartDate": null,
            "EndDate": null,
            "RowStatus": 1,
            "TotalPrice": 1000,
            "ImageFolder": null,
            "ImageName": null,
            "MultiProvider": false,
            "LocationRestricted": false,
            "BusinessTypeRestricted": false,
            "RankScore": 2.9,
            "AppPackageDetail": [
                {
                    "Id": 1,
                    "AppPackageId": 1,
                    "AppServiceId": 192,
                    "Price": 30,
                    "Quantity": 1,
                    "ExclusionFlag": 0,
                    "Notes": " no  ",
                    "RowStatus": 1,
                    "RankScore": 1.6,
                    "AppServices": null,
                    "NameAr": "قص أطراف الشعر",
                    "NameEn": "قص أطراف الشعر"
                },
                {
                    "Id": 2,
                    "AppPackageId": 1,
                    "AppServiceId": 193,
                    "Price": 40.5,
                    "Quantity": 1,
                    "ExclusionFlag": 0,
                    "Notes": " notes  on service  ",
                    "RowStatus": 1,
                    "RankScore": 0,
                    "AppServices": null,
                    "NameAr": "قص غرة",
                    "NameEn": "قص غرة"
                }
            ]
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`

##  Package  service Providers     :
- Api : `api/AppPackageDetailProvider/Get`
- Method: `POST`
- parameters:
- Example :
      `{
    "AppPackageDetailId": 2
}
    `
 
 - Response : 
	- *faild*:
    
	- *success*:
    ` {
    "Result": [
        {
            "Id": 2,
            "ProviderAccountId": 14,
            "AppPackageDetailId": 2,
            "AppServiceId": 193,
            "Price": 200,
            "Notes": null,
            "ProviderName": "mo7sen beauty",
            "Address": "Nacer City ,Makram ",
            "ProviderScore": 4.1
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`

##  Accept Payment Get Auth Token    :
- Api : `api/AcceptPayment/GetAuthToken`
- Method: `POST`
- parameters:
- Example :
      `{}
    `
 
 - Response : 
	- *faild*:
    
	- *success*:
    ` {
    "Result": {
        "token": "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiR0Z6Y3lJNklrMWxjbU5vWVc1MElpd2ljR2hoYzJnaU9pSmhjbWR2YmpJa1lYSm5iMjR5YVNSMlBURTVKRzA5TlRFeUxIUTlNaXh3UFRJa1lUQktkMVJITldwTlJuQlZWakpXYVNReFRWTTBhRkJvY1VZdlVFSndhVkZFY1RNelkwNTNJaXdpY0hKdlptbHNaVjl3YXlJNk1qVTVOQ3dpWlhod0lqb3hOVFUxTWpZMk5qRTBmUS5PekRHOVdIV2QwaHV4dFNOWVA4TjZiRjh5T3YwcGd5WDZkTk9feUlJT0VqZ3BvdEtWUGRadUgwUEVWam1aMjFNYUFmcHowWldSMUI0Xzg3TzBoMl9iQQ==",
        "order_url": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

##  Accept Payment Get Get Oder  url    :
- Api : `api/AcceptPayment/GetOder`
- Method: `POST`
- parameters:
- Example :
      `{"token": "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiR0Z6Y3lJNklrMWxjbU5vWVc1MElpd2ljR2hoYzJnaU9pSmhjbWR2YmpJa1lYSm5iMjR5YVNSMlBURTVKRzA5TlRFeUxIUTlNaXh3UFRJa1lUQktkMVJITldwTlJuQlZWakpXYVNReFRWTTBhRkJvY1VZdlVFSndhVkZFY1RNelkwNTNJaXdpY0hKdlptbHNaVjl3YXlJNk1qVTVOQ3dpWlhod0lqb3hOVFUxTWpZMk5qRTBmUS5PekRHOVdIV2QwaHV4dFNOWVA4TjZiRjh5T3YwcGd5WDZkTk9feUlJT0VqZ3BvdEtWUGRadUgwUEVWam1aMjFNYUFmcHowWldSMUI0Xzg3TzBoMl9iQQ==",
	"amount_cents":"20",
	"currency":"EGP"
}
    `
 
 - Response : 
	- *faild*:
    `{
    "Result": {
        "token": null,
        "order_url": null
    },
    "DetailedMessages": [
        {
            "Property": "",
            "Meta": "Unauthorized",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 102,
    "ValidationResult": null
}`
	- *success*:
    ` {
    "Result": {
        "token": null,
        "order_url": "https://accept.paymobsolutions.com/i/53bM"
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  App package  booking with selected  services     :
- Api : `api/AppPackage/Booking`
- Method: `POST`
- parameters:
- Example :
      `{
    "AppPackageId": 1,
    "ClientId": 30,
    "GlameraUserId": null,
    "AppPackageBookingServices": [
        {
            "AppServiceId": 193,
            "Quantity": 1,
            "ProviderAccountId": 14,
            "AppPackageDetailProviderId": null,
            "AppPackageDetailId": null,
            "ExpectedExecDate": "2019-04-16T16:00:00",
            "ExpectedEmployeeId": null,
            "ProviderPrice": 20
        },
        {
            "AppServiceId": 193,
            "Quantity": 1,
            "ProviderAccountId":2077,
            "AppPackageDetailProviderId": null,
            "AppPackageDetailId": null,
            "ExpectedExecDate": "2019-04-16T15:00:00",
            "ExpectedEmployeeId": null,
            "ProviderPrice": 30
        }
    ]
}
    `
 
 - Response : 
	- *faild*:
    `{
    "Result": false,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

##  Be Beautiful ( Consultation Types ) :
- Api : `api/ConsultationType/Get`
- Method: `POST`
- parameters:
- Model :
      ` public class ConsultationType :BaseClass
    {
        public int Id { get; set; } 
 	 	public string NameAr { get; set; } 
 	 	public string NameEn { get; set; } 
 	 	public string FulldescAr { get; set; } 
 	 	public string FulldescEn { get; set; } 
 	 	public int LevelNo { get; set; } 
 	 	public int ParentId { get; set; } 
 	 	public int BusinessType { get; set; } 
 	 	public int RowStatus { get; set; } 
 	 	
    } `
 
 - Response : 
	- *Success*:
    `{
    "ConsultationTypes": [
        {
            "Id": 3,
            "NameAr": "ConsultationType 1",
            "NameEn": "ConsultationType 1",
            "FulldescAr": "ConsultationType desc",
            "FulldescEn": "ConsultationType desc 2",
            "LevelNo": 1,
            "ParentId": 0,
            "BusinessType": 0,
            "RowStatus": 1
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`

##  Update Partial for  glamera client     :
- Api : `api/Client/UpdatePartial`
- Method: `POST`
- parameters:
- Example :
      ` { "Id":30 , "Phone": "33" , "Email": "test@test2" , "UpdatedProps": "Phone,Email"}`
 
 - Response : 
	- *Success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  Get time slots     :
- Api : `api/BookingService/GetEmployeeTimeSlots`
- Method: `POST`
- parameters:
- Example :
      ` {"AccountSetupId":"3095" ,"EmployeeId": 3139 , "CheckRoster" : true ,"DateFrom":"2019-05-08"}`
 
 - Response : 
	- *Success*:
    `{
    "BookingService": [],
    "HourSlots": [
        {
            "Hour": "10 AM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "11 AM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "12 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "01 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "02 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "03 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "04 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "05 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "06 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "07 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "08 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "09 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "10 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "45",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        },
        {
            "Hour": "11 PM",
            "SingleHourSlots": [
                {
                    "TimeSlotEnd": "00",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "15",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                },
                {
                    "TimeSlotEnd": "30",
                    "SlotStatus": 0,
                    "Hour": null,
                    "BookingType": 0
                }
            ]
        }
    ],
    "TotalCount": 0,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`
##  Check Day  if is  off  or  normal  working day    :
- Api : `api/WorkingHours/CheckDay`
- Method: `POST`
- parameters:
- Example :
      ` {"AccountSetupId":3095,
"CheckDateStatus":1,
"Date":"2019-05-08"
}`
 
 - Response : 
	- *Success*:
    `{
    "Result": {
        "StartTime": "10:00:00",
        "FinishTime": "23:45:00",
        "DayOff": false,
        "ShiftId": 1
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

##  Get  Account Offers     :
- Api : `api/OffersSetting/GetAccountOffers`
- Method: `POST`
- Example:
        `{"AccountSetupId":14}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "Id": 1071,
            "NameAr": "عرض تجريبي",
            "NameEn": "test offer",
            "DescriptionAr": "عروض ببلاش",
            "DescriptionEn": "Free services",
            "Services": []
        },
        {
            "Id": 2065,
            "NameAr": "عرض ٢",
            "NameEn": "offer2",
            "DescriptionAr": "عروض كتير",
            "DescriptionEn": "More offers",
            "Services": []
        },
        {
            "Id": 2067,
            "NameAr": "أوفر ٣",
            "NameEn": "offer 3",
            "DescriptionAr": "كتير كتير كتير",
            "DescriptionEn": "More more more",
            "Services": [
                {
                    "NameAr": "تشقير الحواجب",
                    "NameEn": "تشقير الحواجب",
                    "Id": 9054,
                    "BookingMinutes": 15,
                    "RankScore": 0,
                    "Price": 400,
                    "OffersSettingId": 2067,
                    "CompanyServicesId": 4768,
                    "DiscountType": 2,
                    "DiscountValue": 20,
                    "MaxDiscountValue": 100,
                    "PriceAfterDiscount": 400
                },
                {
                    "NameAr": "حنة للحواجب",
                    "NameEn": "حنة للحواجب",
                    "Id": 9055,
                    "BookingMinutes": 15,
                    "RankScore": null,
                    "Price": 600,
                    "OffersSettingId": 2067,
                    "CompanyServicesId": 4769,
                    "DiscountType": 1,
                    "DiscountValue": 300,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 300
                }
            ]
        },
        {
            "Id": 2068,
            "NameAr": "عرض ٣",
            "NameEn": "offer 3",
            "DescriptionAr": "عرض اخر",
            "DescriptionEn": "Offer again",
            "Services": [
                {
                    "NameAr": "تشقير الحواجب",
                    "NameEn": "تشقير الحواجب",
                    "Id": 9054,
                    "BookingMinutes": 15,
                    "RankScore": 0,
                    "Price": 400,
                    "OffersSettingId": 2068,
                    "CompanyServicesId": 4768,
                    "DiscountType": 2,
                    "DiscountValue": 10,
                    "MaxDiscountValue": 200,
                    "PriceAfterDiscount": 400
                },
                {
                    "NameAr": "غسيل شعر عادي",
                    "NameEn": "غسيل شعر عادي",
                    "Id": 9053,
                    "BookingMinutes": 15,
                    "RankScore": null,
                    "Price": 200,
                    "OffersSettingId": 2068,
                    "CompanyServicesId": 4767,
                    "DiscountType": 1,
                    "DiscountValue": 150,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 50
                }
            ]
        },
        {
            "Id": 2074,
            "NameAr": "offer now",
            "NameEn": "offer now",
            "DescriptionAr": "",
            "DescriptionEn": "",
            "Services": [
                {
                    "NameAr": "حنة للحواجب",
                    "NameEn": "حنة للحواجب",
                    "Id": 9055,
                    "BookingMinutes": 15,
                    "RankScore": null,
                    "Price": 600,
                    "OffersSettingId": 2074,
                    "CompanyServicesId": 4769,
                    "DiscountType": 2,
                    "DiscountValue": 20,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 120
                }
            ]
        },
        {
            "Id": 2075,
            "NameAr": "last offer",
            "NameEn": "last offer",
            "DescriptionAr": "",
            "DescriptionEn": "",
            "Services": [
                {
                    "NameAr": "حنة للحواجب",
                    "NameEn": "حنة للحواجب",
                    "Id": 9055,
                    "BookingMinutes": 15,
                    "RankScore": null,
                    "Price": 600,
                    "OffersSettingId": 2075,
                    "CompanyServicesId": 4769,
                    "DiscountType": 2,
                    "DiscountValue": 25,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 450
                }
            ]
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`

##  Get categories with services  for account    :
- Api : `api/Category/GlameraGategories`
- Method: `POST`
- Example:
        `{"AccountSetupId":14}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "NameAr": "4دمات إضافية للشعر  3",
            "NameEn": "4دمات إضافية للشعر  3",
            "OnlineDescription": "خدمات إضافية للشعر",
            "Id": 4555,
            "AccountSetupId": 3095,
            "Services": []
        },
        {
            "NameAr": "خدمات إضافية للشعر  3",
            "NameEn": "خدمات إضافية للشعر  3",
            "OnlineDescription": "خدمات إضافية للشعر",
            "Id": 4554,
            "AccountSetupId": 3095,
            "Services": [
                {
                    "Id": 13201,
                    "AccountSetupId": 6221,
                    "NameAr": "ser7",
                    "NameEn": "ser7",
                    "CategoryId": 8301,
                    "Price": 50,
                    "Description": null,
                    "BookingMinutes": 45,
                    "RankScore": null,
                    "OffersSettingId": null,
                    "DiscountType": null,
                    "DiscountValue": null,
                    "PriceAfterDiscount": null,
                    "HasOffer": false
                }
            ]
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  Get Offers And Most Popular    :
- Api : `api/GlameraHome/GetOffersAndMostPopular`
- Method: `POST`
- parameters: ` {"CountryId":1938}`

 
 - Response : 
	- *Success*:
    `{
    "PopularCompanies": {
        "Result": [
            {
                "Id": 10,
                "Name": "beauty  company   1",
                "NameCulture": null,
                "MostPopular": 1,
                "CompanyImages": [
                    {
                        "Id": 0,
                        "CompanyId": 0,
                        "DomainUrl": null,
                        "ImageFolder": "CompanyImgs",
                        "ImageName": "Whatw_20190227180138532.jpeg",
                        "MainImage": true,
                        "SharedForAll": null,
                        "RowStatus": 0,
                        "AddToSentAccount": false,
                        "AccountSetupId": null
                    }
                ]
            }
        ],
        "DetailedMessages": null,
        "Code": null,
        "Exception": null,
        "State": 100,
        "ValidationResult": null
    },
    "SponsoredOffers": {
        "Result": [
            {
                "CompanyId": 3089,
                "Id": 1035,
                "NameAr": "عرض علي باقه العروسه",
                "NameEn": "عرض علي باقه العروسه",
                "Status": 1,
                "Value": 200,
                "Image": null,
                "ImagePath": null,
                "StartDate": "2019-05-12T00:00:00",
                "EndDate": "2019-06-12T00:00:00"
            }
        ],
        "DetailedMessages": null,
        "Code": null,
        "Exception": null,
        "State": 100,
        "ValidationResult": null
    }
}
`

##  Glamera Create  new  booking   :
- Api : `api/Booking/GlameraCreate`
- Method: `POST`
- parameters:
- Example:`
{
    "AccountSetupId": 14,
    "BookingServices": [
        {
            "AccountSetupId": 14,
            "ClientId": 4,
            "Date": "2019-03-26 04:00:00",
            "BookingMinutes": 15,
            "EmployeeId": 5,
            "ServiceId": 386
        },
        {
            "AccountSetupId": 14,
            "ClientId": 4,
            "Date": "2019-03-26 04:00:00",
            "BookingMinutes": 15,
            "EmployeeId": 5,
            "ServiceId": 387
        }
    ],
    "ClientId": 4,
    "Date": "2019-03-25",
    "IsPaid": false
}
`
 
 - Response : 
	- *Success*:
    `{
    "Result": {
        "Id": 5400,
        "AccountSetupId": 14,
        "SlotId": 0,
        "bookingStatus": 0,
        "ClientId": 4340,
        "Client": null,
        "Date": "2019-03-26T04:00:00",
        "Time": "04:00:00",
        "Comment": null,
        "IsPaid": 0,
        "ServiceTotalTime": "00:00:00",
        "TotalTime": 0,
        "VisitNo": 4,
        "GlameraLoyaltyPoints": null,
        "CancellationReasonId": null,
        "CancellationComment": null,
        "CancellationDate": null,
        "BookingServices": [
            {
                "Id": 5762,
                "AccountSetupId": 14,
                "BookingId": 5400,
                "ServiceId": 386,
                "EmployeeId": 5,
                "ClientService": [
                    {
                        "Id": 5971,
                        "AccountSetupId": 14,
                        "BookingId": 5400,
                        "BookingServiceId": 5762,
                        "ServiceId": 386,
                        "Service": null,
                        "TransactionsProductOrService": null,
                        "Price": 0,
                        "NetPrice": 0,
                        "TotalNetPrice": 0,
                        "TaxRate": 0,
                        "TaxInc": 0,
                        "Quantity": 0,
                        "DiscountType": 0,
                        "DiscountValue": 0,
                        "PayedFlag": 0,
                        "PayedValue": 0,
                        "RemainingValue": 0,
                        "VatValue": 0,
                        "EmployeeId": 5,
                        "Employee": null,
                        "OffersSettingId": null,
                        "OffersSetting": null,
                        "AddedDate": "2019-05-09T13:27:37.9541518+02:00",
                        "ModifiedDate": null,
                        "IPAddress": null,
                        "AddUserId": null,
                        "ModifyUserId": null
                    }
                ],
                "Employee": null,
                "Date": "2019-03-26T04:00:00",
                "PerformedBy": null,
                "Description": null,
                "OffersSettingId": null,
                "EstimatedTime": 0,
                "Note": null,
                "Service": null,
                "BookingServiceSteps": null,
                "Price": null,
                "TaxRate": null,
                "Quantity": 0,
                "PackageID": null,
                "BookingMinutes": 15,
                "AddedDate": "2019-05-09T13:27:37.8683827+02:00",
                "ModifiedDate": null,
                "IPAddress": null,
                "AddUserId": null,
                "ModifyUserId": null
            },
            {
                "Id": 5763,
                "AccountSetupId": 14,
                "BookingId": 5400,
                "ServiceId": 387,
                "EmployeeId": 5,
                "ClientService": [
                    {
                        "Id": 5972,
                        "AccountSetupId": 14,
                        "BookingId": 5400,
                        "BookingServiceId": 5763,
                        "ServiceId": 387,
                        "Service": null,
                        "TransactionsProductOrService": null,
                        "Price": 0,
                        "NetPrice": 0,
                        "TotalNetPrice": 0,
                        "TaxRate": 0,
                        "TaxInc": 0,
                        "Quantity": 0,
                        "DiscountType": 0,
                        "DiscountValue": 0,
                        "PayedFlag": 0,
                        "PayedValue": 0,
                        "RemainingValue": 0,
                        "VatValue": 0,
                        "EmployeeId": 5,
                        "Employee": null,
                        "OffersSettingId": null,
                        "OffersSetting": null,
                        "AddedDate": "2019-05-09T13:27:37.9760934+02:00",
                        "ModifiedDate": null,
                        "IPAddress": null,
                        "AddUserId": null,
                        "ModifyUserId": null
                    }
                ],
                "Employee": null,
                "Date": "2019-03-26T04:00:00",
                "PerformedBy": null,
                "Description": null,
                "OffersSettingId": null,
                "EstimatedTime": 0,
                "Note": null,
                "Service": null,
                "BookingServiceSteps": null,
                "Price": null,
                "TaxRate": null,
                "Quantity": 0,
                "PackageID": null,
                "BookingMinutes": 15,
                "AddedDate": "2019-05-09T13:27:37.8693792+02:00",
                "ModifiedDate": null,
                "IPAddress": null,
                "AddUserId": null,
                "ModifyUserId": null
            }
        ],
        "ClientServices": [
            {
                "Id": 5971,
                "AccountSetupId": 14,
                "BookingId": 5400,
                "BookingServiceId": 5762,
                "ServiceId": 386,
                "Service": null,
                "BookingService": {
                    "Id": 5762,
                    "AccountSetupId": 14,
                    "BookingId": 5400,
                    "ServiceId": 386,
                    "EmployeeId": 5,
                    "ClientService": [],
                    "Employee": null,
                    "Date": "2019-03-26T04:00:00",
                    "PerformedBy": null,
                    "Description": null,
                    "OffersSettingId": null,
                    "EstimatedTime": 0,
                    "Note": null,
                    "Service": null,
                    "BookingServiceSteps": null,
                    "Price": null,
                    "TaxRate": null,
                    "Quantity": 0,
                    "PackageID": null,
                    "BookingMinutes": 15,
                    "AddedDate": "2019-05-09T13:27:37.8683827+02:00",
                    "ModifiedDate": null,
                    "IPAddress": null,
                    "AddUserId": null,
                    "ModifyUserId": null
                },
                "TransactionsProductOrService": null,
                "Price": 0,
                "NetPrice": 0,
                "TotalNetPrice": 0,
                "TaxRate": 0,
                "TaxInc": 0,
                "Quantity": 0,
                "DiscountType": 0,
                "DiscountValue": 0,
                "PayedFlag": 0,
                "PayedValue": 0,
                "RemainingValue": 0,
                "VatValue": 0,
                "EmployeeId": 5,
                "Employee": null,
                "OffersSettingId": null,
                "OffersSetting": null,
                "AddedDate": "2019-05-09T13:27:37.9541518+02:00",
                "ModifiedDate": null,
                "IPAddress": null,
                "AddUserId": null,
                "ModifyUserId": null
            },
            {
                "Id": 5972,
                "AccountSetupId": 14,
                "BookingId": 5400,
                "BookingServiceId": 5763,
                "ServiceId": 387,
                "Service": null,
                "BookingService": {
                    "Id": 5763,
                    "AccountSetupId": 14,
                    "BookingId": 5400,
                    "ServiceId": 387,
                    "EmployeeId": 5,
                    "ClientService": [],
                    "Employee": null,
                    "Date": "2019-03-26T04:00:00",
                    "PerformedBy": null,
                    "Description": null,
                    "OffersSettingId": null,
                    "EstimatedTime": 0,
                    "Note": null,
                    "Service": null,
                    "BookingServiceSteps": null,
                    "Price": null,
                    "TaxRate": null,
                    "Quantity": 0,
                    "PackageID": null,
                    "BookingMinutes": 15,
                    "AddedDate": "2019-05-09T13:27:37.8693792+02:00",
                    "ModifiedDate": null,
                    "IPAddress": null,
                    "AddUserId": null,
                    "ModifyUserId": null
                },
                "TransactionsProductOrService": null,
                "Price": 0,
                "NetPrice": 0,
                "TotalNetPrice": 0,
                "TaxRate": 0,
                "TaxInc": 0,
                "Quantity": 0,
                "DiscountType": 0,
                "DiscountValue": 0,
                "PayedFlag": 0,
                "PayedValue": 0,
                "RemainingValue": 0,
                "VatValue": 0,
                "EmployeeId": 5,
                "Employee": null,
                "OffersSettingId": null,
                "OffersSetting": null,
                "AddedDate": "2019-05-09T13:27:37.9760934+02:00",
                "ModifiedDate": null,
                "IPAddress": null,
                "AddUserId": null,
                "ModifyUserId": null
            }
        ],
        "ClientProducts": null,
        "Transactions": null,
        "ClientPackages": null,
        "AddedDate": "2019-05-09T13:27:37.8673851+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": null,
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

## Update  booking     :
- Api : `api/Booking/update`
- Method: `POST`
- parameters:
- Example :
      ` {"Id":5400,"SlotId":1,"EmployeeIdTmp":5,"NGBDate":{"year":2019,"month":3,"day":26},"Time":"04:00:00","Comment":null,"VisitNo":4,"ClientId":4340,"Client":{"Id":4340,"Name":"yousef","Phone":null,"Mobile":"01060499088","Email":null},"BookingServices":[{"Id":5762,"ServiceId":386,"OffersSettingId":null,"Price":null,"TaxRate":null,"Quantity":1,"PackageID":null,"EstimatedTime":0,"EmployeeId":5,"NGBDate":{"year":2019,"month":3,"day":26},"Time":"04:00:00","Date":"2019-03-26T00:00:00.000Z","AccountSetupId":14},{"Id":5763,"ServiceId":387,"OffersSettingId":null,"Price":null,"TaxRate":null,"Quantity":1,"PackageID":null,"EstimatedTime":0,"EmployeeId":5,"NGBDate":{"year":2019,"month":3,"day":26},"Time":"04:00:00","Date":"2019-03-26T00:00:00.000Z","AccountSetupId":14}],"ClientPackages":[],"AccountSetupId":14,"Date":"2019-03-26T00:00:00.000Z"}
`
 
 - Response : 
	- *Success*:
    `{"Result":true,"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}
`
##  Get my booking   :
- Api : `api/Booking/GlameraGet`
- Method: `POST`
- parameters:
- Example :
      ` {"ClientId":30}`
 
 - Response : 
	- *Success*:
    `{
  "Bookings": null,
  "GlameraBookings": [
    {
      "Id": 5393,
      "AccountSetupId": 14,
      "bookingStatus": 0,
      "bookingStatusDesc": "Unconfirmed",
      "ClientId": 30,
      "Date": "2019-05-08T00:00:00",
      "IsPaid": 0,
      "TotalTime": 30,
      "VisitNo": 20,
      "CompanyName": null,
      "Address": "",
      "RankScore": 0,
      "IsRatedByClient":0 ,
      "SiteName": "beauty branch 1  ",
      "TotalPrice": 5,
      "CancellationReasonId": null,
      "CancellationComment": null,
      "CancellationDate": null,
      "CompanyImages": [
        {
          "AccountSetupId": 14,
          "MainFlag": true,
          "ImageSort": 1,
          "ImageFolder": "CompanyImgs",
          "ImageName": "Capture_20190212180808181.JPG"
        }
      ]
    }
  ],
  "BookingsVM": null,
  "TotalCount": 20,
  "DetailedMessages": null,
  "Code": null,
  "Exception": null,
  "State": 100
}
`


##  Get  booking   Detail  :
- Api : `api/Booking/GlameraBookingDetail`
- Method: `POST`
- parameters:
- Example :
      ` {"Id":5409 } `
 
 - Response : 
	- *Success*:
    `{
    "Bookings": [
        {
            "CompanyName": null,
            "Address": "ehhev",
            "RankScore": null,
            "SiteName": "yoyo",
            "TotalPrice": 40,
            "ClassType": null,
            "Client": null,
            "Services": null,
            "BookingServices": [
                {
                    "Id": 7034,
                    "AccountSetupId": 7241,
                    "BookingId": 6581,
                    "ServiceId": 13215,
                    "ClientId": 0,
                    "Date": "2019-06-30T10:00:00",
                    "Time": "00:00:00",
                    "DateTo": "0001-01-01T00:00:00",
                    "Service": {
                        "Id": 13215,
                        "AccountSetupId": 7241,
                        "Image": null,
                        "Price": 40,
                        "CurrentPrice": 40,
                        "BookingMinutes": 15,
                        "Description": "",
                        "NameAr": "htc",
                        "NameEn": "htc",
                        "IsProcessTimeRequired": false,
                        "ProcessingMinutes": null,
                        "FinishingMinutes": null,
                        "Details": null,
                        "Code": null,
                        "CategoryId": 8311,
                        "DummyCategoryId": null,
                        "Category": null,
                        "Status": 1,
                        "TaxRate": 0,
                        "AverageCost": 40,
                        "ShowOnline": false,
                        "OnlineDescription": null,
                        "ServiceDetails": null,
                        "PointsRedemption": null,
                        "LoyaltyPoints": 0,
                        "LoyaltyMoney": 0,
                        "LoyaltyPointSystem": 0,
                        "RankScore": null,
                        "RanksCount": null,
                        "OffersSettingId": null,
                        "DoneInHome": 0,
                        "AppServicesId": null,
                        "CompanyServicesId": 8738,
                        "AppCategoryName": null,
                        "DiscountType": 1,
                        "DiscountValue": 50,
                        "PriceAfterDiscount": 40,
                        "serviceEmployee": [],
                        "ServiceResource": null,
                        "ServiceConsumables": null
                    },
                    "Booking": null,
                    "Employee": null,
                    "EmployeeId": null,
                    "PerformedBy": null,
                    "Description": null,
                    "Value": 0,
                    "BookingMinutes": null,
                    "OffersSettingId": 5156,
                    "Price": null,
                    "TaxRate": null,
                    "Quantity": 0,
                    "PackageID": null,
                    "BookingType": 0,
                    "EstimatedTime": 0,
                    "Note": null,
                    "EstimatedTimeUpdateStatus": false
                }
            ],
            "BookingPackages": null,
            "ClientServices": null,
            "ClientPackages": null,
            "Transactions": null,
            "Id": 6581,
            "AccountSetupId": 7241,
            "CompanyId": null,
            "SlotId": 1,
            "bookingStatus": 0,
            "bookingStatusDesc": "Unconfirmed",
            "ClientId": 7354,
            "Date": "2019-06-30T00:00:00",
            "Time": "10:00:00",
            "Comment": null,
            "IsPaid": 0,
            "TotalTime": 0,
            "VisitNo": 23,
            "GlameraLoyaltyPoints": null,
            "CancellationReasonId": null,
            "CancellationComment": null,
            "CancellationDate": null,
            "CancelledBy": null,
            "PackageID": null,
            "GlameraCreate": false,
            "IsRatedByClient": null,
            "IsRatedByProvider": null
        }
    ],
    "GlameraBookings": null,
    "TotalCount": 0,
    "DetailedMessages": null,
    "MessageCode": null,
    "Exception": null,
    "State": 100
}
`
##  Get salon  Offers    :
- Api : `api/OffersSetting/GetOffers`
- Method: `POST`
- parameters:`GlameraView == 1 in history of using offers by galamera user`
- Example :
      ` {"offerTypeId":3 , "AccountSetupId":14 }`
      ` {"ClientId":4343,"OfferTypeId":2 , "GlameraView": 1}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "Id": 3039,
            "NameAr": "jjj",
            "NameEn": "jjj",
            "Value": 0,
            "Services": null,
            "BookingService": null,
            "RetailProducts": null
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`


##  Get Service exist in packages or offer  :
- Api : `api/OffersSetting/GetPackageById`
- Method: `POST`

- Example :
      ` {"Id":1035}`
     
 
 - Response : 
	- *Success*:
    `{
    "Packages": [
        {
            "DescriptionEn": null,
            "DescriptionAr": null,
            "Id": 1035,
            "NameAr": "عرض علي باقه العروسه",
            "NameEn": "عرض علي باقه العروسه",
            "Value": 200,
            "Services": [
                {
                    "Id": 6322,
                    "Code": null,
                    "Description": null,
                    "NameAr": "ويفي مع اكستنشن - شعر متوسط",
                    "NameEn": "ويفي مع اكستنشن - شعر متوسط",
                    "Price": 8,
                    "BookingMinutes": 60,
                    "PackageId": 1035
                },
                {
                    "Id": 7437,
                    "Code": null,
                    "Description": null,
                    "NameAr": "x",
                    "NameEn": "x",
                    "Price": 50,
                    "BookingMinutes": 30,
                    "PackageId": 1035
                },
                {
                    "Id": 7438,
                    "Code": null,
                    "Description": null,
                    "NameAr": "y",
                    "NameEn": "y",
                    "Price": 60,
                    "BookingMinutes": 45,
                    "PackageId": 1035
                }
            ],
            "BookingService": null,
            "RetailProducts": null
        }
    ],
    "TotalCount": 0,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`

##  Get favourite Accounts :
- Api : `api/ClientFavorites/GetFavAccounts`
- Method: `POST`
- parameters:

- Example :
      ` {"ClientId":2099 }`
 
 - Response : 
	- *Success*:
    `{
    "Result": null,
    "FavAccounts": [
        11,
        14,
        12,
        9
    ],
    "TotalCount": 0,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`
##  Add to  favourite Accounts :
- Api : `api/ClientFavorites/Create`
- Method: `POST`
- parameters:

- Example :
      ` {"ClientId": 4342 , "AccountSetupId":14 , "RowStatus":1  }`
 
 - Response : 
	- *Success*:
    `{
    "Result": {
        "Id": 1021,
        "ClientId": 4342,
        "AccountSetupId": 14,
        "EmployeeId": null,
        "CompanyId": null,
        "RowStatus": 1,
        "AddedDate": "2019-05-13T10:22:08.9571168+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": "14",
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  Remove from   favourite Accounts :
- Api : `api/ClientFavorites/Delete`
- Method: `POST`
- parameters:

- Example :
      `{"ClientId": 4342 , "AccountSetupId":14  }`
 
 - Response : 
	- *Success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  Ranks Create   :
- Api : `api/Ranks/Create`
- Method: `POST`
- parameters:
- Model:`
        public int Id { get; set; }
        public int? ClientId { get; set; }// rank value  come  form client  may be ranking  account  or service or employee  
        public int? RankingAccountSetupId { get; set; }// rank value  come  form account  may be ranking  client  
        public int? RankingEmployeeId { get; set; } // rank value  come  form employee 
        public int? RankedEmployeeId { get; set; }  // rank value come for employee
        public int? RankedServiceId { get; set; }// rank value come for service
        public int? RankedAccountSetupId { get; set; }// rank value come for account
        public int? RankedAppPackageId { get; set; }// rank value come for application package
        public int? RankedClientId { get; set; }// rank value come for client  my be  emloyee whow rank  client or  account  
        public int  RankVaue{ get; set; }
        public string Comment { get; set; }
        public float? GlameraLoyaltyPoints { get; set; }
`
- Example :
      ` [
    {
        "ClientId": 4341,
        "rankVaue": 3,
        "rankedServiceId": 7438
    },
    {
        "ClientId": 4341,
        "rankVaue": 3,
        "rankedEmployeeId": 3137
    },
    {
        "ClientId": 4341,
        "rankVaue": 4,
        "rankedServiceId": 6446
    },
    {
        "ClientId": 4341,
        "rankVaue": 3,
        "rankingAccountSetupId": 3095
    }
]`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  ContactUs :
- Api : `api/Account/ContactUs`
- Method: `POST`
- parameters:

- Example :
      ` {
    "SenderEmail": "demo@demo.com",
    "SenderName": "joe",
    "MobileNumber": "01060499088",
    "message": "kjkkkjjjk",
    "Subject": "nnmnmnmnnmnmnm"
}`
 
 - Response : 
	- *Success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

##  Client Memberships :
- Api : `api/ClientMemberships/Get`
- Method: `POST`
- parameters:

- Example :
      ` {
    "ClientId": 4346
}`
 
 - Response : 
	- *Success*:
    `{
    "ClientMembershipss": [
        {
            "Id": 71,
            "AccountSetupId": -1,
            "ClientId": 4346,
            "MembershipLookUpId": 1946,
            "Password": null,
            "ExpiryDate": "0001-01-01T00:00:00",
            "Type": 0,
            "MembershipCardSerial": 0,
            "Notes": null,
            "RowStatus": 0,
            "CardCode": "G-126402165",
            "NameAr": "الباقه الأساسيه",
            "NameEn": "Basic",
            "SiteName": "Glamera",
            "LoyalityPoints": 0
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`

## Membership Points History  :
- Api : `api/ClientPointsBalance/MembershipPointsHistory`
- Method: `POST`
- parameters:

- Example :
      ` {"ClientId":2110 , "AccountSetupId":14}`
 
 - Response : 
	- *Success*:
    `{
    "ClientPointsBalances": null,
    "PointHistory": [
        {
            "ClientId": 2110,
            "LoyaltyPoints": 5,
            "LoyaltyValue": 0,
            "TransactionDate": "2019-04-24T19:35:53.89"
        },
        {
            "ClientId": 2110,
            "LoyaltyPoints": 5,
            "LoyaltyValue": 0,
            "TransactionDate": "2019-05-07T16:11:26.82"
        },
        {
            "ClientId": 2110,
            "LoyaltyPoints": 5,
            "LoyaltyValue": 0,
            "TransactionDate": "2019-05-07T16:17:27.167"
        },
        {
            "ClientId": 2110,
            "LoyaltyPoints": 5,
            "LoyaltyValue": 0,
            "TransactionDate": "2019-05-07T17:43:05.607"
        },
        {
            "ClientId": 2110,
            "LoyaltyPoints": 5,
            "LoyaltyValue": 0,
            "TransactionDate": "2019-05-07T17:43:24.23"
        },
        {
            "ClientId": 2110,
            "LoyaltyPoints": 5,
            "LoyaltyValue": 0,
            "TransactionDate": "2019-05-07T17:43:43.827"
        },
        {
            "ClientId": 2110,
            "LoyaltyPoints": 5,
            "LoyaltyValue": 0,
            "TransactionDate": "2019-05-07T17:44:44.95"
        }
    ],
    "PurchaseHistory": [],
    "TotalCount": 0,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`

## Get Voucher And Gift Card  :
- Api : `api/ClientsOffers/Get`
- Method: `POST`
- parameters:

- Example :
      ` {"ClientId": 4343  }`
 
 - Response : 
	- *Success*:
    `{
    "ClientsOffers": [
        {
            "Id": 57,
            "ClientId": 4343,
            "Value": 50,
            "Remaining": 50,
            "Code": null,
            "TransactionId": null,
            "RemainingCount": null,
            "OffersSettingId": 1038,
            "EndDate": "2019-05-12T00:00:00",
            "AccountSetupId": 3095,
            "OffersSetting": null,
            "AccountName": null
        },
        {
            "Id": 56,
            "ClientId": 4343,
            "Value": 200,
            "Remaining": 200,
            "Code": null,
            "TransactionId": null,
            "RemainingCount": null,
            "OffersSettingId": 1037,
            "EndDate": "2019-05-31T00:00:00",
            "AccountSetupId": 3095,
            "OffersSetting": null,
            "AccountName": null
        }
    ],
    "TotalCount": 2,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`



## Get All Notifcation By Client :
- Api : `api/GlameraUserNotification/get`
- Method: `POST`
- parameters:

- Example :
      ` {"ClientId":32 , "MessageState":1 }`
 
 - Response : 
	- *Success*:
    `{
    "GlameraUserNotifications": [],
    "TotalCount": 0,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`

## Get Available Payment methods to choose from while payment:
- Api : `api/Transactions/GetAvilablePaymentMethods`
- Method: `POST`
- parameters:

- Example :
      ` {
   "AccountSetupId": 14,
   "ClientId": 30
}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "NameEn": "Gift Card",
            "NameAr": "بطاقة هدايا",
            "PaymentTypeId": 5,
            "CurrencyId": 1026,
            "Value": 0,
            "Details": null
        },
        {
            "NameEn": "Voucher",
            "NameAr": "قسيمة شرائية",
            "PaymentTypeId": 9,
            "CurrencyId": 1026,
            "Value": 0,
            "Details": null
        },
        {
            "NameEn": "Client Down Payment",
            "NameAr": "مقدم حجز",
            "PaymentTypeId": 10,
            "CurrencyId": 1026,
            "Value": 0,
            "Details": null
        },
        {
            "NameEn": "Loyality Points",
            "NameAr": "نقاط الولاء",
            "PaymentTypeId": 7,
            "CurrencyId": 1026,
            "Value": "NaN",
            "Details": null
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`


## Update Client Profile SMS && Notifcation flags:
- Api : `/api/client/UpdateProfileSMS`
- Method: `POST`
- parameters:

- Example :
      ` {"ReceiveSMS":1, "ReceiveNotifications": 1} `
 
 - Response : 
	- *Success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
## Update client image:
- Api : `/api/client/UpdateImage`
- Method: `POST`
- parameters:

- Example :
      ` { "Id":30 "Image": "/9j/2wCEAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDIBCQkJDAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIAcwBzAMBIgACEQEDEQH/xAGiAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+gEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AOIZiTxULcGp1iPUkYqCQ4bmuVGjQgODTi2KhLYpd2aYh+aM0zNKOepoAkGKCAR1pgYA47Uwk9aCkyVcButOLYFVtxzUqEkjNJgO3E09HPIzgVGRk8Gk6VLRSZaLErwcCmq5piZPWnPgHH61JpdllHz1q3EgC7mOfRfWqMGcjv2rQTc7DH4CspOx00o3LMMbMc55FbtipbaCM1Qso8jmui0+2GBnv3rgr1LI7owVtSeC0yo8vhj1IFa9lpAcHeDuxkADv7+grU0vTFCq8qEbvur3aunt9OjQZdF9lB4Fc9DD1cQ9NjnrYmMNEc9ZaC0/QrGgH3gM5/xrdttItbVABGrMOrMOtaQAUcAAUhNexSy6lTj72rPOqYmpN+RXkVVXLHAHYVEytMuP9WnfHU1YbBOSM0xwXUqO9c1aEb2ITM6Sbcgjgh4JwCerfQVRk8u3c+afMlHG3OVB9/Wr8t5HExSFAZB1YjpWWbeaZnbaTzycY/KvLqPXR3Z20lproitdX0jPsB3yE4Bx/KmrZurAzYLf3c5q75a2ilRF+8PXv+dY+oXqWu4XE+GI+WKM5P4kdP1pRTb8zoXaJalf5/JiZVc/wryRVG41G3tAoaUzMOTzgf8A1/0rDbVJZmKLIIYz1IGAo/qfrWXd3CGY7N+zPAc5J9z711wptg42N648RmWFvKgXlvlLdAO/H5VUWea6YbyWz2PQfhVKCJ5druQsY4Ge/wBKnur6G1TYH2DPIXBJrdR6EmipgsUVpShOchFXJP0psuqFhhIo4FHXPLH8K5iXVJX3NGGjQn756t+NVTNvYF3KqewPzH6mto0mQ2joJdWj3bdzkn+6SBT4rySX5UDDNYsJwSQoVf1qwtwn3VLe4Byar2diGzfgYAZlkIH+yea0oXi6Ixbno5yPyrmIbwA4Cqg9xV+C7J53j3O0U2iTbutNttWgEM8NuwAwCEwVHsccVzl14CjQhoZEZV/vLyfrit+3u0IHzAkd8EVdF+j4QPtkPQNxms5c0dmK19Dz678KyRoTGke4f3a5jUtIljlxtOR1BFev3D28zFZhjspPHHasW60aOUtIjliR0bpRCtKL1IlST2PJLq0NvHkjGRzWc4I5wa9M1DSfIABiDAjo33T+Nc3MkkbHyreNMHgiMZ/lzXZSqqRzSg0cusUko4priZMp83+NbM80kWdxA74NUZJyx+bLA1unczM87lPPHtSAlmGOTVibDj7uCOlRBivUDiqAekBmBwQr5+6aZ5LiTbzkGnPMcghR9QKX7RKybTg+9JsAjUPlSfmXvTzDJ0jGR6Co2YpztwT3A4pUuHMbI2TkcEUtWArGSPIdhx2znFQu24dKjJ5pQ3arQCYpcGl3D0pQN54FMBFANSADHY0gTFLlPSgDT89uRu4NN381DupRWJqPJ5NBbApmaMgd6BMfuzinqM9DzUKk561Kr7eRSFYfz3608KGHNRCXc2CQKc7bRSZSQjKoPWlLIq/KcmomkyMEUIqlHYyYIHAx97/P9KAHh+eelTKAf8arL3FSK+BgUmUicME6Gnj5lH54quGzU8IJPfFQzWG9ixbId4rUtoiTUdhYzXDEwxO4XliFyAK3tO0xjJt4dupCsMKPdulcdWokehShZE+nWbzEKiknHOBXX6PptyWQLFtbOAT1pujaeJMKjMYwDmQHj/gP+P8AKuvswsIRIkIAG0t615k5c712CtWcVZFqxso7XlmMk5+87da0N4UcmqRmSFsOw3HoKN+0F52VfSu+hifZR5Y/16nlTTk+Zl0Ev7CmPKFOOg7k1TN8Fy7nZEBwO5qjJffanDONsK89etXVzBKNo/EOFCTfkaM1zkFkI8sdWz1qnPeORwPLQ9M9TVVrwSPuQKwUYVRyF/8Ar0gdLeUSSkNORkhjnb9fevNqVpVG22dMaSXQvW9jEqb3BMh5x6fX3qjd30cblIydwPTPQ+pqC81pTbMgciRuijjaPUmuR1DWSsLQxbefvP3P0ND99KMFobUqMm7zN/VNfisonit5VnnlH72UjOPYf41xks7zyM7uAOrMaqyXIXlzuY/wjoKzru9ZzsB59PeumFJs6OWMFZFm9vlLhIFP88mn20Owi4ucMRyFboPrUNvbrAhuJj8wHT0/+vVa5mknUynCQg4wTyTXVGPRGM5Fu51aaacLCclRjPZR/Ss55QGJdw5zyx6fh61WmnVgFCeXEvQDq3uaqM2/lyQvYCuiFMwdQ0JLlpZAFJbHA7ACjzQrjLZYd+pqhv8AM+VcIvc0vniIHkKPUjk1pYhyNUXYzk49s9qQXhHSQIfesQ3LMcRKfqeTViK2eX75IHpT5V1BTuaH9qndtTMjep6Vctru5fG5sD0HAqjDb7MBAo9yMmryQWwUGe6BJ/hTn9elS7Bqa1rJMvI2jP8AEz//AF6tB5IWEk0cbKe6dRWZA1r5ZIztHBJJFSkJ5ZaF8kDoW4NZSjcaOst5La5tgN3zHjnoKjMEsOTC3Ge3T8q4xdRuYWUI+3nG0cCtmw1Rj9/5Tx06GueUGjS3Y0GC3BZJVXPpWLqukb4f3IJAP3Sa6HdBfIpc/OvIIOCKY6gDZIAB13D+oqFJxd0KUE1qedzW1sLhRdR4b7vzZB/KsrULmxDtEtsyOOA7dvfFd9rnh2LVD5yMVlCgDb0ri7zSVjbyJ9ysvRm7/X2ruo1FLqcdSlynMsd0m3t6imy2743fwmrUtuIZMc59CKhZc8HO3612JmJTKFDgYoc4bpz7U6QjJCrgUwkFcE4NG4iRJcja3SjIQ5xketQHHUGniUDg459aLAPd45P4efWm+WCMg8VEwCnKsCKYJMHAqgJvKYYJHHrVhEAQAVTEmQQc4pyyEcgH86ALMg6fNgdzUIKjvmmne3OCc0zkUAX2ORTg3FQ596ep4rNmiYu6gnmm5GeaTd83ekBIHwacXyOlQE4PTFO3cUWGiTcetAck5JqLdzS5PXBxmgaJSeeauJbpHZQ3jTRMGmaNoQ3zrgA5I9Dn9Kzs+vSn7uwpAbniDUrLVdXkvLKwjsIXC5gj+6CBgkemfSsrOWx2qIN2NOXr1qbFIsRjkDgZPWtO2eGBgUQSuOjMPlH4d/x/Ks2IFjwtbNhahlEsoxED26n2FY1GrHTSi7mxp6y3OGlYsiHKr2z7DoK63R9Oa/kAbP2dDkkcKD7Duax9F0+bVGAjTy7fcMken1r0O2hSJI4LVAkcY/hGK8ivNXO2UrIvQ2xULGkYjXAxubnFSi9iizFDgyfdDep9qzp7pA/2e3LSTE84HNK0iWSAykGdumDkj2Fc9+xzuF9y2XEWZJDukPGWPA/GmSX6MpwxGeSw7D2zWbNK0zAty/UL2A9MDqapTTed8rSFYR99/X2FSk3ojRUk9y61410SRgQqeSe/tVGW8kvp/IiYiNcZPYCqVzdtORbWw2g9FzwB6n3pDNFaQ+VA+ZM/M/bPc1SpmySRr/bBaZhiIZ17j+H/AOvWTeauYxt3fMT0ByT9azL/AFRIk8qAlV7k/ec+p/wrH3+Yd88piQjg4yT7Ct4UL7gklqy/PqckjMkJ5PU5rKluNpILbm/ven0qK4vVwVhG1O3qfqe9UWkLvtGSTXbTpWFKdiWW43dM5P6VNbRC3XzX5ai3tlRTLJjb6sKiurmKQ4UMFH61slfRGMp9xbq7Mg3M2E7LnqaqtIWcyS8YHyr2qCSVeSAS/bJ6VA8hJySSa1jCxhKVyaWTjc34ZqsSWyTwB2FL0+Z+tMG6UYUED371ojJiGbHCDLdvQUqwGRt0rcnt1NTJEiJ6t3btT40LjgnHdj3p3GlcWBAjccY64P8AWrRm2ZEaYJHU1XklihGBmqklzJIcZwPSlqw2NBpUA+eRj6+lC36q2I0B+orOBDkbjx3AqcbQeB+tFg1NWO5eYbThQe65qcR4RysjGReeeM1lJMw5wo7ZNWEu23jnjoaTQxJWmM2512r61Ml0F/jLAd6huC8sZQMSqnis8+ZGOnJ70rJhqjqrW/iUqY5ihxzk8Gt+3vg0a5cle2DnFecCcpgFmU47VdtdVlikH7zj8qwnRvsaKaZ6I8yLGrsDt/56L/Ws3W9La/tWaIKsyrlCe/41nWPiFGXy5sEHg5rXgkEQLR/vIeoxzs/+tWFnB3G0mjzG9trhn/fA+avBBNZEqSRkg9a9V1nTkvgHXHmDuBiuE1HSnglfzI3BznOOBXdSrc2hxVKfKc4wJ6moT39q0prLCbk3MPcYqjJGYzhgQa6U0YsgJyKDjrSkZzgUwg/hVCA03GKdg5pDyPemAmeelSBuOgpmDTjgDmgCVZuMYppk55OfrUajNBBz0NAF0dcVIi8d6jP3sU8PhQB69azZaFZCOaYR3NOnlHRTUO8sKAHFgRSZpDg0YyPxoGhR71NE+0lWBKN94Z/WoQcjFPUZ4NA0PKFTg/h7ilU8GngZjwf4f5UgX0qRic5qWMUirkVbt7ZnOBUyaRUVdlvT7VriVUzherMRkKK6/TdMS7mSPy2ZQdscY9OuT79z/wDWrP0ywmTZaRrieY5Zh1UDt9ea9K0TR4rC3RODM3BI6j2rzMRWs9D0aa5VqWrC0+zQrCiqAo5IGB+HtTru6aBRBbAvIx6gfrU13L5WIoRhm6AnJqqojtUkkmYvIRzj+Vea1d3LXdiREWELP/rLyT36VQn1NYpdzETT/oPaqNxqxLMkCb5WOGLNgD/P1qKLJmXdgOf4VXAHvWkYN6su1tWaa3k8u55f3YIwqKMYFZlzeiZtqD92pwFX+I+lV9QviZPKjP8AvE+lUpbpLaLkneVwoH8I/wATW0KXULmg1wEJijYF25kcdz6VWkaSR/IgALdyTgKPUk8D61mpP5URmc4JGeOcVmXGpSTLjJCZ4XPf1reNElyRoXTwW7ndItxJ3YZ2D6etZFzePK+5nyTVaa4Y8ZwKrh/yFdMKdtzCVUtl8gc9TirttFtG5h8o6+/tVO2jAPmN0FSzXDsAiDGf0q2uxKl1ZLd3bSYUE4HpVJnA4z9TSM4XhTlu59Krkk4FVGNiJSuKWBPHekJWPk9e1NZgo45akRd/J5PUCtCBy5kbL5A7Cn5A4A59Kaquz7I/mb17CrISOzXcW3ymlcLAI8DdNxjnb/jUE1yf+WZAGMbvWopbhn5P4D0qEhpDwKaBsaXLNkk59aekbuMAYzU8FsWOQD9TWnb2WRnFJzSNIUnIoxWxxx09KspZNjjd+Fa8FhuPIrVttN3YJrP2jOhUopHOJpDOehq7H4dmPKBga6620wYGQK2bazVO1F2yJcqPPJ9IvrZcPGSvqO1ZVwhTPGT6GvYJ7RZIzkCuO17QgzGSMYbGeB1qtiElJHnswVm6FDVYllPJBFad7bSRMQw4rMkTB4rRWZlKNiWO4K/dJ+lbel63LaOPnJQn5ga5gsVY5GOaekpByDUzppoSlZnp1veRTOsiODG3p2qlq0RYOgHzHlW965jSdS8h2BGUb7yg8j3FdUkouLYj7zDkZ71yODgzRrmRx1zp9wCfO4x1PtWJdQKHI4Y+xrvLuCQhbi3d47hBuUA8j6EVymsXVxqFx9pnCeZtwzrGBu+uAOfeuqnPm1OOcOU5phtJFR5wa0Xtg4JBUEdcGqslrIq7uPpmulSuZEBOeDxSAZ4pwUlgDVpbPI4b86q4isVwuf1qPqea0eIMK+Cp9uarTCISfLwCOMdKLgQZ28Y7UgOetDn5vSm4pgW89804svY0q2/IGcd6YUO44BxWZRI0ROCOaiYEcCnZYYIzxSDJ7UJgC04/KfrSEGmkMTQO5PEu488cc1IfKTOOT2qEKyj7wwacpVeRg/WkxluB48rvXjvSlMvhenYYquvPtWhAGMa7cccVL0KQ6O2LEHaRXQ6PajmZgMY+XPc1lWsTzShAxJLbcD1ruNA0oecDMEY7ceXyQv1I7+3vzXJXqWjqdVCn1N7QdNMaCdo/vKNrY6+p+lbxkFujKoG/HXFRA/ZogznAxhUHGPaqDXLS3Alb/V5IWMdWPqa8iT5nc61G5a89IQXlbLevdj7VzGtaw8oKRcHHzEHhRUmsatFE2DKWY8YTt7e1ctJO91MR92MfdXtW1Kjd3ZWxeglBYFAGY9Cf51pSXIsbBt7EzOM89RVC0jWJPNkYdOFP8/pVKe5+2XRkdiYgenrW/Jdjv3J45hEn2iU5Zvuqe/uaovPuZp5DxzjPeo7q5E02QcRgYGOmKzrq58wBF4Ud66IQMpTsTyXfmrgn5RyfeqTzdT+VRSSjO0dBxUJbc1bKKRzym2PMm41Zt4GlwScKO9R20BnbttH3j6+1XJZFQ7E+72xVPyJQ+STAwB7CoXlEaFQfmPU0xnAGc54quxLNk8UkhtjsnPHU9qRpBGuAcn1prEgU1QpOXPHpVEpijnBYcVPCrSNtQEDu1RorSc4AUd/Wp2kWNNicE989KGNIe0iwArEBu7mqbtu5JyxpGfBwM59aWOEs2BzmktCrXGIrM3TPNaVtaGQcqBjtUttYE4OM59K3bSyO0AJg1nOp2OinRtqypbWOR0rVtrDJwQKv21iQMkVrQW6qBgDisbts2uktChb6ft4xWpBahMcDPvVhIQKsIo7itEzKTCKMAZxVtFGBUUYC/Spl46dKtGMiQxgjgYqjeWu9CODWgvIpHQHNXuQnZnnutaOCpOO/pXCX1m9vI3BxmvaL+1EiEYzXD6zpn3jt71KfKzaymjzx/n4PBqLBWruoWphlbjiqJbPDfrWyd0c0lZ2JY3ZXyCQa6bR9RyVjPpjGe9cmG2nnJWrlvI0TCVG4HpUVIcyFCVmejSSW724ZkJX+8pwQfx4rn9UttMlhW4t5mxJnKOu3BHUcZ/8Ar1b066FxaFQQQ3OD2NUrpIIJjG5LRz8ADja3r9a56badiqkLnJXduiSEwEbWH1rOLyIWUE47g1o3CvbzeWR0PDeo9apXJ3MTgBu+K7YnG0ynko+cVNHdmONuu49KhflSO9RH65rUksG5LRlH5z3qvnJpB0pOtMQvWigUuKALokIHJzThJyM9B2qsz9xwKek6/wAa/lWdii4kZc8ZNL5TMSAQee1VBcN1Ule2KYHIPBwaQFiQMn8IxUG5vwqQTMSS3Wl8zJ4A+lNANL54xgdqco44GaVI3kOccVPHFsY5/OkxiwxFmyx4rQtiCMLnJ4xVVJgHAX064rb0e0e9nSFEO4nJIHAHc1lUlZXNqauy7oUEpmQRR5kb+LHzAdgPT69a9S0bSV06APMQZn+ZsHIFZWiaWsIbykOOC8zDBH0/z61tSTyrKixPsZBnd/dHt715Fapzux3xjZWRXupjdMzyZjhQ4GeC59Kwda1YWyeQmN+3kj+BfQVNq2piJHO/L8/MT936e/vXD3txJdTGJT945Y1VCjzas0bsgaZry59FHp6VpYjjxnnAyaoKBZx7RgycD8TSGUJwTnByfdq65LsZpssXl5If3Qb535Zs9B6VQknwPKjPHrSzuFVn4LZ5zVQH5SSOpyaqENCJSY932R8HA9aovJuGB0zSTT+Y3Bwi9Peq5kIz647dq2jEyk7iu5zwe9SWsTzSBU5JOBVdVZ22r371qoVs7cIDmRhzx0qmyCWR0tU8tCCfX3qs0mOhy3c56VXeUs2QPpSA7lxk8fqalIdyXd3JyKa0+MgY+tV3fqOPemqxY59KoLk5ckhnPI6CpYYzIS7cKO3rUEK+ZISR8o61aMnRF6/ypXGkTSSYwiDp+lV2fBK9fWh32DaOW7mliiLcfxGkWld6CJGWPeteysskZFSWNjyMit+yseQSOKxnM6qdNLViWViWIBHA71u29oFA4p9tbBQtX0TtWSRbkMigx2q0kQHanIo9KlC+lMyuIo49qdS4IGKTFCYh6nipFbtUPTvTgau4mi0jGpcg1URqlV+auMjNoJYwRXOapZ7lbiunzmqF9EHQ1UtUEXZnk2tWQBY4Oa5GaPY+K9P1uz5fBrz7U4CspOKdKXQdZaXM3ORjv/OpLeQxycf/AK6gJ96eeRuXg1szlOj0ufy5CV/759K1dWhWe0EqA7kwwI7GuUs52+V1zvQ54712drMs1kwIHIDr/n6Vy1FyyubR95WOOvIzOpAGXUbl96yG2iI5+/nFb1zGbe52sSFVtvHof8Kx7+IpMx6gk8j1710QZyzRmuSCajIzzUrjmo8GuhGVhtJjmpAKfhfxpiI1Wn7Qeq0pXvT9/TcoJ9aBEOCuQRTc+tBlZutITnsKgoeOlGcEYpgJA4pfM3dOPXigZKuc5qVcqc4qJH+X3qRWA65qWFiYSkAgcCprc5cenvVTJfgCtC0t02+ZcF1j/hA4Ln/Ck2NK5r6TpYv5ZG8yOOCIZkkcgAfTJGT7V1mnXVrERa2AURoMFl5LtnGSe59B0riLi/eRUhRtqL0QdE9hXYeBrN7p/tMoPlRthQOm4d/1rixF+XmZ10Uk7I7y1U2lmsbHc7fMzHuaydU1Bo/3ETkO2S7e1S6xqkVpGwRhkjAb2ri7+/ZVZ5mO5ui561wUqbm7s7NIq7C8vFeRy7fKvH+fWsp5fm2qMFj2HWq4naVstznoB2pWbBwrc9CRXpRhyqxhKdy6qRrEWeYCRDhE2kknufSke5jCAJHgL15yWNUCRtGTwP1po3Scdvanyi5rFkMZOW4Uc5qnPPvYop+UfrRPNjEanC+vrVN3wvHT+dWomcmK0ig8c+gqLJP1NM3ZJJq3axK7/MfkHJP9BV7E7lu1QW8QnkOcdAarSzmRifWlmnMr7FGI17VCQC2B071ICqwPU8dz60F8k44H8qjZwenSm7snAzVWAUnPB6U+NS5AAzk0xQXwBVqNQO+PegaRIpwNkY6DmguANq9e5phbHypx61JFCZCABlqhs1jG4+KIuQAOfWtyxsOhIGaSwslXGRlq6OztBgcVz1KnRHZCnyoWyshn29q27e3AwabbwAdqvIvTisdQbFRMCp0X2pFX2qQDA6VZm2OC08cGhcUuaZIuc0UClxQA3FLQab3oAkBApwfFRdjSg9qpMTRaVge9RTLlT3pobjrSO/yVfMRy6nNaxDkNx2rzzWIMbse9emaiNwbjNcLrSDnHpUwdpGrjeJwkvDd85pYmG7B6GnXS7XP1qtu6127o4HuW1PkTq38BOa6nSpgkJXJ2nkfSuVVvMhOeSOlaej3AWTy2YjuP61lUV0XB2Zqa9bkWzSjKBGG76dP8DXMv88bo33sZH1Xr+ldxcRNdWTRsA4eLCn34rh7gNBMwIPynOD3/AMipou6JqqzMyT73tUdT3KBJiEJKdVz6VAQRXWjnYZwKQHNLTwARyeaZA3NP49cUmBnANKVxxQBVPWhTk05gd2BSFD1qRi4wcZoGB2zSBTnmnFcc0APQZ56VIDgUyJl25JAxUsRWSQ54Qck1LKRNACcu5xGO3rVnzvPYOWAC/cHYVHKC21Any4/h5wKntbESYaVtnovTA9c/5/Cs21uzRIn07T5b+4WOMEDOGcjIH+J9q9Wt1t/DujRW0YG/y8sW6n1/M1yPhO2eS6Mz4W3tfmCjqWPSrviPVTLM8KKN2cE5/D8q4K7dSfItjvoxUY8zKd/qH2id5nOETovqfSufurpp5SC27FJeXXyhFOVXPOepPU1RR+pHBPQ1006aSM6tS+hOZSDw2Md81LG29evSqDN0C/hVqJtibRya0a0Mk7kpfJ2j5nPSiaYW0WwEF261E8htix4Lnrg9PaqbO0jZPWhRBsezszEnrj8qjYlyBnjpTHcgYHrUkanbknntV2sRuOjiLNtA+p9KsPIEXyo8cdajyYV2j7x/So88YFSUh3TgHk96azYGBTGbH1qPcScDpTSAcW3HHalX5sAHHvTCwI4qWCP17+lUCLES4wO3epCxOQKiLdFFWrW3Mh749aiTtqaxi3oghgZsDvW5ZWaqBgZJ6mkt7XAGPxrZtLbHWuWczupU1FXLFla4I4rct4AMe1QWluMA1qwRYFY2uOch8aAVOi05I8VMIwKtRMXIjVSetPHWn4xRinYTYAc04CijvQId2xRR0oNK4DSMGm4Oaeab34pgJ603PNO7UzOM0hj84xTGb5eCfwo3HFQs3B4ouFijfHIb1rjNZXdu5zxXYXhBzzXIatwCKIvU0itDgr5f3h+tUM4PrWpfrhjx0rKbqRXoweh51TRk8D7XxnANWYpDBOrjsaz1Pf0q1u3KGHpzSkiEzubOUS2jqGwwUFG9uxrntYgY3Ad1GXyrYHGSCM/yq94fuvNQQtztGB9Kk1OEqhEhHyPnPscf/rrmj7srG01zRucfJHvt94IJjOCPUHofzz+YqqVNaslsYdQlgyMFigP16H+VUWHqK64s42V1GTS4qwsanpkmgxZ6VdySuBmrKyLt+ZAcd81GU2kc808Jx1FNiMxJNpyec04TZcE8CoM0ucUWGXXKqm5SKaJUIGQQaql84GaUmpsBIq+ZKVX6/QdzVlXBIRPuJ0/2m9arA+WmAfnYc+wqaA449AWNJlIuLJ5e2KNQ0zdXPUf0q4txMZIre1aaSVyoBGdztnjH49B/WqSfuYdxA8yTueoHp/j+FdT4NsAk76vc8JD8sRP97uf5D8fasaklGN2bwi5OyOhz/wAI9ov2ePic4Mjj+KQ9fy7VzF7K+zzCTuIwM1b1K6kvbgjeSqsWOOmaxtQlO4oDx2rmpQ1u9zrqOysitKRIy7mwp6+tRyEA4UH61F5u0sxILfTOKjaRnPLZNdaVjkbuWUPzgk8KO1WkcIM9x1NVIx5abjyewoeQgBT1POKT1GnYWaTzH3fwimu235RkU1ztFQhiGznj1qkhEqkKdz5A9KnRsfvGHzEfKPQVWTn52Hy9venhiWyaGhomyWySev60wnFBfg4qEnnrSSBsczFvpSDngfnQisxwOpp5GPlU5Hc+tMAjXJ+lSgkHA60xQQeO/WtC1szJyVJFTJ2NIQctBLW2MrDcDW7a2m3AAp1pZgKPlrZtbM8ZFcs5tndTgooLW2JI4ratrTvin21qABlea04LcA81juypTFt7fAHFXo4h6URJjGOgqzGua1jExcgVBTthqUKB1pxAxV2M7kG3ijFSkZpCPWpaHcjoPWnn6U09aloaEpKUcUoGetSMacUnal6UUANPTim4p9JtPWnYCJhUMnOatEZGKryr3x0pNDRk3f8AF9K5XUuc811l5wGrldQHJzzUx3NonF6gnzHgmsSYYbPvXRahHzkCsK4HB46GvQpu6PPrxsysOtWIT8u3FVvapY2961ZgjU0e6NveIx6d66jVTDJCJP76gY9u/wClcTE2yTP1rt7JF1DRBISDIoZD+VctZWfMaxd1Y5q/UGRuzRYQkH2qhe7BLvXo4DD+v6g1eukzdhecOm3HuBj+n61RmBNuhx91mU+3Qj+tbQOeSK6SbTyM07c2dwPFRhTVtI4jCM8fjWhmyoeWJyacAcc1K8BCllBKDvjpUJammBj0DvQaQdaoBRU0KBizMBsQbmyf0/GoamJxCqjv8x/pSY0IzF3LHqTmrkI+UZ74/If/AF6pKpLAetXyNkKnA+Yf/W/pUspE8ET31+kQOdzYz7Z5Nd9qsyaXpcFnbL5aKAMd8n19+p/GsrwtpkUMQvrhclj5gGP4R0H4kfypmoXH2zUTI+SinJB7muKo1OaS2R3Uo8kLvdkDSGKD+fPWsa6mLvuHVhj8K0NQuook2bSzt13dB/j/AJ4rEkmZlyxzzwK2hHqZVJdBjHt6U6I/Nn+dQMc1IhKrnPtWxlcurJ2z07+9M9XJ5PSo05Iz0FEkoxxwD2qbDYrNnAqMcnmkVsnNOxk+3emCHjnrwtPLHHoKYDntimls/SgY8sSMA8UhyKYOKkUZ7UAh6Mdu0evWpUjJOAMmn28DS4CD8K6PTNJ4XcMk9sVnKaR0U6bkZ1nphcAsD9K6W00zAX5Tz7dK17DSPmGQPyreg09U7D61zSk5HSuWGxiW+m9MitWCzA7Vprarx0qwkIA6Cp5LidRsqRQYGe9XEQ4HFOEeOmKeoIFUo2JuPRQBiplwKhU4p+/Apk2JxS1CJPajeDT5gsSZ55o7moy3GQaTf71LY7DycUxuTSZz3pvT3pNhYcDijcRRnApCc1IxTxSAgUmaB0pgPCg5pwXjFItOUkVSRLGFcZxVeYfIeKuHnNVZuBzRJaDRiXnykiuX1AcntxXUXnJPNc1fj1PNYLc6IHJ6gtYNwPvV0l8pziueuV5JrupM5a6M33pyNgkUxjgkUmcEGuk4i2rYYH2rq/C0+9Z4N2CQGGK5Dd8o9a1tDuDDqMTDvkfpWFWN4mkHZlq9OZ2JUfJOV47Ht/Kst9vlzoCezj88f+zVs6jl7ic4wJVWTA6ZBA/x/OsSOT/SQM/f3IfxyKcNUZ1FqVWbnFIkmGOTxTGPr1puCTW3QxZet7gp1I59elV2OGIqMDnFKBQlYRlMeeKbQaWqAcg3MFPen5LEk96ag+Vj6DH5/wCTQhwaQyaEbifTFbOlWaaje28LA+Ug3Sf7vX/AVjoNsOc8sa7zQ7FLWwSSRcG4bDHuFX/DH61jWnyxN6MOaRoXt+I4BFFFs83lRj+EcKP0z+VYzbYIBKxyCT17mrTlr3UGkbhVHPoBWDrV6JZNqDbGOFHtXLTjrY66krIz7iYzTMxOcmqpJJx2FIWxjmmk4Hua7EraHG3fUCcsKkB7ColORThxVCLAO1een86YSWySaYWyR6CjeOnNIY5Tg5xUpJwMVCpBPWpS3HWgEIWyMd6UdKZkCpI1L9qBiquTWlaWLy4+Xin2On+ZgkV1FlYoqj5c1hUqW0R1UqN9WN0/Swm3Cc+uK6rTrEJtYiobO3HHH1rdt4gqjiuZttnQ3bRFi3gAHSrscYHaoYcLznipS+K0ijNkwAHag4qlJerGpy3zegqnJrSJkZ575qromzNdmA9KjLgDOawX8QQ/wkfnUS69CfvSLUtlKLOiMwx1pPOrEXVoXAIkFPGoRu/DZ96zbLSNoTgUqzAjFZX2pF6uM1Klzu7/AJVHMVymmJOKUSVQWcetSebkcGpcg5S5v9KUN7VXRxjrUgcUuYTRLkUm7mo99AYU+YViTIpe1RBwDTt/eqUgaJQakDccVWMo7Gk83HeqUhcpbL8ZP4VSuHBU5o87kgnioJ5AV61TlcSVmZ12Rk+lc7fYNb1y3JxzWDed+fpWSWpvE5u+AOa5y6PXrXR3+ME45rm7s9RXZSMMQZUn3j9aQUjn5jSA811HnMmT5gfzq5Yy+TcwsONrjOfrVGM81Mh+apkrqw46HYaoFMbFduVHy+44/wAP0rkWYq+QeQa6feX0qG4yC3KHNc3PEVbIztPSsqRVbcinAFxIo6BiB9KjXrU06kSBscMqn9Of1zUYUjn1rc52GOeKUU4LjtT9vtTuIwaKKAKYD84ix6t/L/8AXSLTiPkX8akigkfBC4U/xHgUrlJGho9k1/qUEA6A7mz2HWupllDJLJHJuDt5MIJ5EYPX8eT+NVdF05fstwqSDzZx5W4AnaMcnP04/GrjqkU2wACODJ+p7flwK46kuaR3UoOMbkFxcfYLR4wB83Jz+WP8+tcrcSmaYscY7CruqXpnmbB4z+dZhxj72CfatacLasxqTu7DWYk+5pGPvQAM5LDH40hwSOa2MhynCkU9enPApgPPA/GgnjFAC78npxTcnNNzSgd6AuSA9DTw/NRjlaVepphcnVcsK1bCEEjpWVGQHA7CrsNzJCw2EY9DzWc02tDWnJJ6nW2EIA6VuW0e3pzmuW0/VWEYaRF2hgCc46mug1bUE0axsWA8y6u4ROVDYEan7o9yetc0qUmzs9vBI6O3ChFY8HuQa0EuI4wMuBxXlT+Lb/ny2VAfTNQN4kv2OTO34Gl7GRHtYnq9xq0EK58xT7Buax7nxBM3C8LnjHWvOf7Wu2b/AFh/SnnULth/rD+QqvZtBzo6+611mUjnPc5rLbUictXPtdXIPOSPoKQ3Nx3UEfQVSgg9p2Nx7xpOQ5qs97Ijcn86y/tsi9Yx+Roa/wB67Xj4p8gKqao1Fxg7v1qzDrLxjg8exrnPtEe/gkD0NTRmOQ4DgfWk4IpVWdSmv9Pmz7mrtv4hyw3MMCuK2srEA5qRGYOOorN04miqHo1vriP0INaMOoo4+8PwNeaQXMkZB3Gty1vsgcnPrXPOnY3jaR3sdwcckYqysoIzmuVtL/8AdjJJI4xWzBPuGRWDTQONjUEnFODiqXmjFHnYpXJsXTIB1NRNOOmazp7oAEE81kzaoVLAnBHIwapXY1BnSGceopDOMVyv9sAN948nPWopddABG/r71ajIHGx1bXKgdf1qrLdj1z+Ncm+vgg/OQR3zVZteAY8kfjWqpyIdjq5ZlPINZd4wIJrMXW1LAkjOPXrTJ9TjkJ5FUoMSkihqDfKa5m6PU+9bd7MHXI/nWBctktXTSRz15XM1/vGjNDdabmuk4SWM/Nxiph1quOCDU47VLGmdLp0gl0eSEnkHI/z+NZpADshztbv6GrOkN+7kwCcc8e1GEmBkUYP8Q9/WsVo2VUeiKV0n7iA4wwDIR7hj/iKqkcVtSJHJAoIwwbr+A/wrPeAxuVNapmDIR0p2DUgi/WplgJHYUxHJ96UCkIxzUqRuwB24X1PA/WqAfjEaHtzVmwha4v4Y0HzFuMmkjjiMH71ycEjCDP6muj8K6eskhvjGscMR+V2+ZiR6dh9cVlUmoxbZtThzSSOjubRrTTkhgBV2AVWJ2579+9ZGryRadp5tt4a6YZcjoPate5+1Rj+07pj90mIyfXqBXCaldyXEjO5JLHua5KMXJ3Z31ZKMbFCR9zn0qNzkilJ5pnU5ruPPA8AClzmm96F5NUIlUdMVYWA7SSMfWoIf9Yo961vK3HAH1qJOxtTjzIzDHg0m0dKv/Zwxx0qN7Ro25H60KQ5U2iqFwMVLFbtKcKMmnpDk1vaXYhmDFeBUynyocKTk7GC0LK21lKsKsFTsjYA8Dkiu/GiQ3MK74Q3HGRyKzrnwm2xjAzeoVvX61msRF7mssLJbHNRmTyox5+IyeVPbmr3iS5ae6txk/u7eKPnthRTLnSLu2B86GRRjOVGR+dVL3dI+5sk4Az+AH9K0Uk3oYum1uUCxJqaGJpXCqOTTFSuk8N2AlmErD5Y+c+/alOfKrjpw5nYuab4ZLKHuBgf3QeT9a24dCtEwDAje7c1bSWNMAtzTJtRSJDyK4XOUjuUYxA6fpsQw1tDx/sCqk40lMgwwD6KKyb3UZptxDYUnoKyJJy2R0960jBvdkSqRXQ3pToxH+pT8AaoSx6NJkAbT9TWQzH+9+VRFsnJY1qoW6mDqeRoSabYOf3c4BPrzVd9HcgmORWHbmqp4XIYH8akjuZI+jGr1QrxfQabW7gPRse1SR3ZjH7+PIHcVcjv2IBcbh3q2qWt0OVXPqKhy7o1jHsyC3ktpmwrgE9AeDV6MFWx+dZ9zpGAWgJI9D1psV5c2jbZAWUdjwcVDinsbwqOPxI6G2diRnIrorOUjau6uYsLqC6ICPyP4Twa6WxjGSSK55w7m3OmjT396ilmKjOeKcV+XgVSu9yrkGs+UhMzdQvsNwSePWsKa7YktkmrV9uLE1mGNm6d+9bRiW32I7i8YZweazpbpiDkmr8loApJJ+prPnWMcbwfpzW8Ejnqt9SqZ3PcijzmOMlqbI0QxyOOv+RTGmX+EMa6Ejjcibz2yOacLvI+YnNVPNOMbBSFz/dH6/wCNFkLmfQum6BBBNUJicE0b8HOwD8/8aRm3cFQfoTTUUiJSbRU5JoPWrAjh3c71/I/4UNbAn5JVYe/y1oZWK46VYByoppt5VGdh2+o5H5inKOM0mCNnSc+XNjOQhIxUUc3lybh+XrVjRFzI4I4KlT9MGmyWSoxHIOSevWserLnsh7ZxxnBGVwahaTeAD1FWtvlxAd8ZH171UUbj0wapGLRJFGXIIHTtVkRN3U0ttFlfrWvHaPsGc0NgeY+cVOEAX3HX86RSTySSfemCrtkbQN/pMM7heSY5VXj0wVNavQaJLSPehTBLsw2oB1r1DTbEQQxJPGdkABZAOHb0+grL8I2ehzW8mpSaXqKmNsRzPeIUJ6YAEY6Z6/1roda1+3j0/CWkauy7EIZsjtnrj26dSa87EzcnyRPQw8OVczRyvizVVurjyoyNiE/d6ZriZWJJz36Vo3s4LtkhnJOcdqynb5ia6qMOSNjCtPmdxrNimihzmkFbGAE9aF6000CmBKrFWyK243DwiRec8HmsEVoadJgshPGM1nNXVzajKzsakaB26ZzTpYGKEgGpdOVpC2ecVJcz7WaLFYXd7I7rLluyjDbncBn34rtNBsweSBniuWtv9YCRXoHh+MGMHGe9Z1pOwUUlqbttahYwMdO2KmNouORjNW7deAMdauLCGxkVzpXLcjCk08MpG0/lWLqXh+2ulw0YHYMo5FdyYBVaa0VhnFUk47E8ye54hfaY9leSQvnKHGfUV1PhqHbYucHlvT2FWPF+mlLlJ9uA64/Ef/rFXfDEf/EvPA++f5CtZzco6kxgou6IbiENwYdynvis+fThIPuSDHvXZmEMpGKYYFA+6AKyUrF81zz6fSZQhIBqh9idThlI9zXprwptIZR+VZN9piyjMeB+FXGt3M3TTOCa3BJHIIqNrRiqnsTge9dLc6U2SGjOezAVmzWUqtkEHFaqa6GTpNGbLaiJc9atwaeJLYN0P0pHtpyRvHA71ZM7rGIwuAB603IIw7meIvLJDCnrGA+VP5UrmRnJx2pIY5FZeaTZqlY07dyQAee1TSW0dzHtYc9iOoqOOMgZxVyBT71le2p0KzVjm7q2e0mwfqGFbWleLJrQiK7TzohxuHDD/GrOq2XnWJbHzJyK46XK5I9K2jaa1OaovZvQ9atNUsr+MNbzo+R90HBHtior5xtPPavJEldGVlZlPqDirD6jeAbTdSlfQsTR7DsQq3U6e+vIo8hnGR2FYlxrPaNVHv1rIkuJZGAZyRTeWJq40kglXk9iaa9mnYlmJ+tQFHY80/GOAOacsTPz2rTRGLbe4xUAOSMir9q9t3gTPvzVaG3Z5xGOc+tEtu8D7TnPYik7ME7am9BJbMoUIgP0FOniQqdoX8BXPrIUYHcSB3rViuN0XNYyi0dEKias0UbhNrEiq74C5KqavTjcM1QlztwfpWsXcwmrEJVW4A5ppiZRkdKMFe9PRznHUVrcxIwzIcg4PYjircd6rIEuII5QP4jw/wD30Ov45qo/3iKarc0yWjqNHW3M26APgrna5yVP1GM9PQVSnl81QD95DtJ/l/WpNGJUqynDAnFVXYtLMVXk5DA9uetYfaZpP4UEbtyCcjBxmpxtYA4+aq0eCCOfarcCH0zVGDLtspDZxmtyAyeUOtULGFm5xxmu00vw5eXtp5lusRQNtO9wDnj/ABpN6iPnpetbVhprtJGJUBLHKxdWb3I64/8A1Dmo4rSPT4jNdEC4/ghbt7t6fTr6479FoUclpby37Ji5lGxHkPzEdDgdh0H/AOqqq1LLQ3o0+aR0UAktLJFuNqRovyxqMAH27Yrm9c1MuMD72MLnnAq3qF46p80hfA49zXLXs2XOfmbHJ9K5qNO75mdtapaPKiozknnqaiY80ZJOTTGNdqR59wzzTVPrS9RTapCFbpSClPSm9qBEg6Vcs+Jfwqmg4q3akmYD2qZbGkPiR1WmBEtXcjk96qTlWlJzyasQN5dptPAx1rNeUtPtQZOcDHeuWKu2ehKSUUjSsot7jHr1r0nw/APs4OOh61wen2tzHzLDJEWxt3qRn869J0OMJbKP51jW3sXD4Tdhj5FW1XGKijXgYq0g4FQtCJMNmR0oMYx0qZVzT9vGKu1zNs4/xdZCTSt6ryjgn6Hj/Cs3wqgNpMhH3XB/T/61dnqtoLrTLiADJZDj69RXG+Gj5N9LAcguM/iP/wBdJroawd4m8IwAeP0qJ0x0U4rQ8vnB6VG8Y9azcQuZMgKscg1DtVxnpWrInBBGR9KpPEoJwpFZSTLTKUkAdSMA5qhPYKedorTdG/hbpVORZCSN36UJspGNPZDkZH0FUp7RMHgVtTW0h5yfyqo9k55bmtoyY7IxHiUHgD8KVI+R8tbA07J+UZqRNOYYwv6VdwsjPSMsuAPrWha2xGM1cisPmGAa07ey2L8wqWw0Rkaki2+nSs4HTA+teeTLuY4Hriu78TTgxiEN8q8kj1rjoLZp51UZrak7RuYVIubSKUNjLLtYJ8o7k4pLq2eI5YDp2rqY9OdVA2kYqpqVgfKbIPAqlVuypYZKOm5yuMn1q5HbEwgkdeajMJCgj15zW3DDiKPjIxWk52OeFO5jNbOOgqRC6Lt8s5raFuCc7acbJW5K81Hte5fsGZlm3lSmRl5xxUd0zTMeMj2raTTh3HBpTpqA9OlT7RXK9i7HNeQ+enFWIhIBgt8v0rXltFHOORVR4gDx0qvacxDo8upA33apzL8hOO9XGAqrMccdiauBlNFJzx0qMHB70+Tg0wVsYCHlqULzSdGpQfmxQKxuab91fof6VVZ/3rEcHJqaybamOnFVJWPmMR/eNYrdl1NkWFIYZXAbuPX6VetJMcbec1lRy4YbhxWjA2MHoKpmDOq00ICvTnmu+0vT5Lm03xFNobHMgHYepryq2u2QhQcjtW1FqMixgCQis07MLHmmkadPqFwAg3EDzGZugwe9dcyQxb5PM3KqBEA6dOee5x1qhEogsTbo/wC8lwzMDkKvYD2xVe+uPKhCD5QFwAD+ZqJtzkejTj7ON2UtTu2kkYA/KKxJGJPJqxcScdciqZPrXTCNkctSV3cQmkY84pCaQ1ojIcDgUmaKSmIVulJSnoKTvTAkQ1as223Ck1UXg1NGcMDjpUSLhvc6WSVEg8sglpDtAx0q9crpdpqFjJYHapiHmhiW2Oc5OW7jPbpiseZsLbTgEhTzT70oZUKHORk1zpWO2fvHoV1Nb3wiu1nLStjhmy3qc/jxXU6Oo8rI7mvMtLgmUx5J2k9K9S0hdkaiuWe50JWibcY6Yq1GOKrxirKDAFBlIlUU/GKYKdjirTIYhX6ZrhZof7O8U8/dMmfwb/8AXXdkcVy3iq0ZjHdr0Hyt/Q/zqZMunvY2NlMMWabp10t5ZJKvUjDD0PerW046U7A9Co0I9Kry2wPatIpn0qKRMDihwTFcyHtcgkDFVXtSOgrd8sHmmvCCOn50vZoakYDW3y98Gk+xLjBHWtow4HTpTWjHSlyWKUjKWzC9eakFsPSr2wc4oCDAGMU7DTII7VcA96Lt0tbdmON3RR71aAIX0rB1y8WIY6kZAx69zRYe5yGuSCSUKpyOg98df1qz4b0zzrncwyMZrOIN3dDAOO1egeHdPEMClgM4rTyL2XMKuljHTgVnaxpIa1dx1ANdl5AK9MVT1C3H2SQY6qalwsQqjueHyweXLIp69RW3boxtEOOCo61DqVqYZxLjCk7TWnpiCawjH8Skr+X/ANaqm7q44rlkyr5JxUiqyjGDzV94CPw70zyztIx9KxZsityO1DBz0X9asrFweKd5Z465qR6Ga0b7sEVWmhOCTxW00eR0qpNFlcHmqUiZK5z8kWOnes64OCOlb1xDgDisW8TAyTznGK6qbuzhrRsZsrZakBHp9aGxmmk10nICnJ3Yp0fLj3NN6LgVLAPmBx0pMaNSDiJ27gj8uaoZyzfWrURKoBntz+VUBnJrOKFULcaBhnPSrKsQAmaooCTjNXIR8wBpsyuX4XIILduK14YmmTcCPTrWTCpf5O3WtOOP5OTWUgMf5iXdyq4JyeuP88ViXs3myMRyAcL2rQ1K48hPKG0buSoHIrCmk2oWU9sUU4vc7q0uhWnbLewqImlamZrpSORid6DSE0hNUiReaU9aQdqU9aAFPSkpOc0tMBVqZetQrnNSg81LKR0mkbLm1aJxuwMEGo5bVo7gRKCecAdzVXRLnyL0An5X4Ndpb28UjrIVBYdDXLUlySPRpQVWC7lvR5IrmIYTa6nDIexr0DThhAa85t/3GtDHCygZ+tekacMRjnNcsviOiW1jYjHtVhOlVojyKtJTOaRIvSngU0U8dM0yRpqpfWi3lpLAwGWHHse1Wyc0w0xo5HRLz7BdyWVxlAzYXPZv/r/0rqcjjFY2v6SbqP7TAP36DlQPvD/GqWl+JFyIL5ghHAlPH5+hpJ20Zq48yujpSBxUTjOaTzw6hlIKnoRyKTzMg8iruZ2G429aaeaR261GZMHipciuUcWFRMwOeKRpM81C0mDUuZSiPyBSAhiDUJlA61HLdxwxl5HCqKSlcLEt1cpbQO7MAAK4HWL03FwQmctx71Z1jWXunMcZwnZRUNnpzqokmB81uikfd/8Ar1ptqzSMCbRNM3yjK5PU16RYWojhGR2rF0PTljG4ryfauriTCgVdNX1M609bIgaPsKr3kZaAgjjFaJUY96hmQNERVSRimeUa3YHE6BfutuH0xWdochS4aInhxkD3HWu31i0AuQ2Mhhg1w91bvpmobkHAbcmayWuh1b2Z0GwN9KiaDByAfrVi3dJ4ElXowyOan2/If5Vlcu5niIbhxwetPEYyeKsrHzipBFxjGTQDZReIVTni5rZeIBfSqFzHx0pDWphXSccCuavh85rrLlRg5/OuRv2JuGHbpXTQZzYlaGc3H1qLqfpUr9cComOOPWutHnsUc1agXgnvjiqyjJ4q9AvCr3OKmTHEnxtjz2/p/k1X2qG4HFXZEJLY4AU4qOOAsehqEyJ7jI4SWBAq5BES445qe3s2JHBxW3a6X5m0gHPvQ5GZHZac6gSGMlD0NdHaaTugDC2EgJ6lsV0fhrw7a3emSiZW8wHCnPA98Vtnw+iJGowpCANt6E+tJU29SWz5hnmZ5WYksT3JqjM2WxmpSQRyeB1qqxySa1irHRJ3GseKaTxSk9qQ9BVozGmg9KQ0dKokWlzzTaUUhh3p1NNLigBwp4IplKDQxosocMK7fQdQE9kAxHmJwfeuEU8ZrT0y6a3u0ZScE4Iz1rGrDmR1YerySO6eN0mtJ24zJ09ORXpOncRivLDrUV7BCgUq6MDivUtOO6NSDxXDNNWud8mmro2Yuoq0gqtH0BqynSkYSJB70uabS0EiGkNKTioy2adx2Gtz1rD1TRLa/bzP9VKerqOv1Hetsn3qBx61LkaQ0OKfT9X0hjJayF4x2jOR/wB8/wCFIviq8hO24tkJHplT+tda4+hqvNCkg2vGrD0YZqPaG+j3MH/hL7b/AJawSrx2wacPFennG5pR9Vq3Lo+nycm0iz7Lj+VZ8/hzT3OQrp/uuaXPHqPkRKfFGnFciRv++arSeKbEZ2lz9BVeTwxZqCRJN+LDj9Kanh62VhgOf95qfNAOQjn8U7uIoG+rHFU1/tPWnIjQ7O56KPxrpdK0W288kwR/L6rmuglgCoAo+law5baESaTsjjrLQo7EieZvMmA49FPt/jWlbWhlnEhHHpWj9kaSTnn2q/bWezgj86aTb1E52RasoBGn+FaIyOlRQr8vSpyMV0LRHJJ3Yx3xmo9wcEU2VueRTEYZ470mxpaGNqcO9SQeVOa5XU7JbqIoTg9VOOhruruHJbI4IrlrqFllII5Brlq+67nTS10OUsr2TSZ2trlcxFuccke49q6GJ1kQSRsGU9CDUN3Yx3UZSROezdxWO+j6lZsXspiy/wB1WwfxHSkpKRs42OjA+bFTBMckVyY1zUbJttza9OMspB/OpV8XgfftGOPST/61Pkb2IkzpZEH4VnXOAORWVL4wRl/49G+m/wD+tWZdeKXkB2WwX6tn+lNU5C50i1eNhTk1xV3LmV+/Jq1e6pdT5LMVU9l4FZMkucnvXXSp23OTEVVLRA7beaiHJz3pjEk8mnJwQK3OMsRrkitSxj3zp6+lZ0Yx1rY05CCG9wKzmzRFyCDz0K5AJ4FbWkaSsrsQQeMcjI5/lWdbQ/uRzg7uv4123hmxE2qISqMrKCwzwfWstzOodJa+ArD+x41aSOO6PLyFtwP09ulLJ4OgtRmC7d0C7myncD+VdTbWsMSbUTAA6Zq1cc28me8Z/lW6irGNzFsL2OC2SNY2dlUAlR1xU8l++7/j3bp61UspljtVY8dRQ9+M9ulVfQR8mOxK1FTmNMJwKaNWJ1NDUo7mkfrTQhhpM8UtN7VQhRTqb2pwoAKWkzSikAopwpvelFAyUdKswf6xeT1FVlHGKtW6kkYz9allRN63j8uQsrblyQCPava9IcNaxnPVQf0rxS2SVAisTtOT09a9l8PsH0y3cd41/kK4MR0PRp/CdFGe1Wk6VUiGTVpO1YoTJO1HSgU002SITzTD1pWNNOe9RcpDT9aifPNSnjio26VEmaIrnPcVG2KmaoHODWbkaxIm4qtIQM1LK9VnJIqL3NURnGfelApAvPNTImauKE2WbAhHYdzWgHDDk1kEMhyvB9aDf7flJ2t61qpuJm4cxuRqg5x1qdFB5rAj1F1IDAEeorQivVZetawrIylSZqqVAyMU1pOvPNUhcryAc0jT571brIhU2SSSZzmolfDdaiMhOTxURcq2azdU1UC9I4kVayNStxy4HPerqSfLzVO/ul8spnk0pzTQoRaehl+Up5p6xjGAKYrg8frUy9q5WzrEMSsMEA+tRNpto/3rSE/WMVaUU/Hp0p8zIZlvo+nj/lyt/wDv0tZV7YW0QPl28S49EArpZOAawNTcENjsK0jN3EjzzxAwNyFUfd61z78HFa+qyGW8lIPG4isiTrXrUvhR5ldpybQzvUiD5qYOKkQ4HIrRmBbhAeQCuisEBKrjtXPWnGWNdPoWHukDruUDpn2rCozWB1Hh3Sob6eFJg2xnwR0zXeXOn2+mXyLaRCNTGCQPrXOeE5/Ls2n+zIxRiNzZ+XkntW9N4ui+85jyOM+X/jU02rakThJvQ3LK8kLBXU7ccHFazfNCBzymK85n8YXSk/vmABxwAKqy+KLx87pZcbtpy/etVUSM/YyOjSzvBwYiFBOASB/OhrSXPLwg+8orjZddmd2Vjna+3JY0xNTlffwow2KftEV7HzPCyTTW7Ailx3pDya0IBelI3Wn+lNbrTQMiJpMU4ikqiQpw6U2lFAAad2pp6UooAdTl60wU8UhkqdRWxYQKWTcQehx+NY8fLCtjT2CyAsQoIOM/nWc2aQ3O2i0+0PhaSZX3XlvdsJCO6MOD9Mg/ma7XwtIG0iEAHgYrh9Pm3afqHmAFzCi8AjgYPIxjtXVeDZidP2ejGuCtqjvpLQ7eI8CrSHiqcJyBzmrS9KyTCRNu4ppNJmjNDJEzRQeRQKgZG3WoiMd6mkqFqzkWiGQ1VkarEvTOaquPyrI2iVnbJqMnmnuCxyBxTBGx7c04o0uOXDNjFWlACgVFHARhjVgKfSt4ohsaVqrcQK6njir2wn8KjdCRTaEnYy/IK5AbAqVMpjn/AOvVhowDyKQKCMVlLQ0vccs2B7+lSpNk1VOM570gIHPelcLF/fzxUUj9yelV/NK9KiZy5JY9aa1FYnaYtwDgVUkBJBzUwHqKZIfl9abWgkVghU564qxGeM1EhBqdFrJs0bJVHtUgGOaag44GKdgk89KLmbZXnPykVzWrP5VtIx7AmukmBORXIeKZhFZsucF+BWlJXkkK9k2eeXLZYk9zk1SYZNW5+p5qscZr2o7Hlz3GAcZperAUhNOi4+Y1RmW4zgADtXR6OWWQFeuf6VzUXbiun0kqOcc4P8qxqm1M67Srgw2QQc5JPNQak8jypHkAE5/z+dMsiFt4h7ZIqTVBxEdynB6+orCN7mjWhDKjv5OR8vAbB61PLbtv3A8H5se9NUE2kDckb+9asql4l2od1Vexn1Mi1tjK8rMAW3dxUktnGj/MHyeeDWlpkLfaphICDu71cvbXbMuB/COgquhim+Y+eQh8vcOuaYo3NVj/AJYfXNRxrlhngV1XJaG454qNutWnXgmqzcmmmDImFJTyM02qRAlL3oo6UwCgHmgdaSgBw5qSoxTgaAJk4NbViGZsDZyhDF+gGKxY2wwNbelvhmAbAYbT7jNZVNjanudTpkLNb3TFRlIQpOfYeh/zgV0Pg+YrDIM9JKxNOITTL+4WRfLcCNEXuc9fyqbwpeH7dNB0BXcPwP8A9evPndpnowsrI9Vt5MqDVxHGB61j2cuUAzWnE3SsIsUkWgaG5FNBzTqtkB2pQQBTScjFJvxxWYA3NQsKlaoyKiRaIJMAGqLnORV+VeKoyDbk1kzWJGR6U5AOveqFzfrbLyc1ht4vEE2w25K+u7FXBNmnI2dmuMClwfSuZt/FMU0W/wAtwv51ZXxJEyFgrflW6dheykb3amsMdq50+Jowen4UHxVHxwfpmqug9hM2Zsg5qq0uM5OKyn8So6kCMfiarnWw5+aMAE9jUOFzWNKSNaSVsHBqEXLDg1BFdJMvyHgetMkljVslwAetRyDUeheWcMeTVhTu71kpc2yncZR+BrRgureRQUlX6Zo5bEyTRZUdQajkTPOKnDKTwQabI21OtD0RkU0B3dutWo1zzUW0HJ7mpYefXrWDLbLIXj+tIw4pw447UjcChGbKcuQpzXnnjC5zdJCDwq5/OvQbuRY0dmOFUEk+1eQ6xem7u5JT1Y8ewrswsbyuZ1JWiZEpyT3qEjAqYjHJ60wIXcBRyeK9NbHAxiIXOKeU2PtxWilg0DBXX5vftUMsWZDwemelLm1BxsMhXLrXR6SuHf8A3T/SsSKL5k9+a6zRbLzY5WJ4WM1nUZcNDQgP7pR7cVNcqZI04zzyetb1loERgUySMcjoB7VNFpUciAlmP1HpWI3JGALeT7GBtIYN0HFbENu3kqG3OxH0xWiuloOGdic9akW0Cn7xZTjg1W5DZj2seL1w8jKDjvWi9tCxBEzHjqWqRdNjDk/NUclgAR85GR0HFCZm1rc+d2jItgRRGv3QR0XpVmaMi3TA5pqJtPvn0rpuO2o2RAVIz+HpVFlAJ9RWlsAyxJBxj8aoupznHeiLFJEBHNRngmpSKQr8mfetUzJoj7Uhp/bFMNNCDvSn1pKU0xCA8U4Uwd6cOTQMlU1oafdeVOjZAwwyfbvWbnilBOeKlq6KjJxdzp5NVRQLeKRnRc/Mf4if84qzpF81tqkcwPAYA89u9ctCf3i/WtCKU7zzWMqatZHTCq73Z7xYTbkX3Ga2IX6ZriPDOofatNgYnLhdrfUV11vICBXlNcrsd71VzWV+lP3ehqmjkYzUwc4FLmM2iQZ3A549KXAJyaj3cCnb+1K4WHU09fSkzSZ5qWwGvjFULkfKcVfbmqk65BqGaRepzl7b781i6jpAkty6jkV1kkQJPeohADlSBg1cJNG6mef2ztaFo36ZNdR4YZJ4JUYgtu/So9W0XOXROvcVk2VxJpFySBlD94DvXS4860LfvR9066TS4muVOBjvx1qe40eJ7f7i5+lc1/wl6reRF4n8vB3Hj8Mc1tr4q0yRQv2g8j+43+FR7N2OeTqJlaPw7FI7BwPwGKhuPCQGTBOy+zDNdHpt1Fc2kcyggOMjPWrW9Tyeman3kL280zg49F1COYrHIvHckint4f1F8s0kf0DH/Cusi2iaVjjrU+UxS5mW8TI4WDRLqZ8bwDnFJc2U9hBLK8ilY/rzXTvNDA8rh12qxLHPQ1y2uXw1IiOA5hDbmPTce1XG7ZpGrOTIrXXJgMRByfc1t2upSTbRIKztG0vdE0jLgHgVrQWoU7cYFRVavZFOUWaUZBXIqzAnGaggTAAA4q6gGK5jGTHAetQyHBPSpiaqXEojjZmICryST0qoog5fxhqYttOa3VsSS8YHYd68zlJb5ielbXiHUxqWpyyqSYgdqA/3R/j1rEkGecYFerQhyxOarK7ICC7cCr+nIFuY32htrA885qsgweAOnetrQbQTXAyM8Hj1PatpuyMYrXU3dM0Vr+VppVG3A2KB1Zjhfw7/AErE1mzNve+UVA8tdpOMZ5PWvT9B08xxIfKjd2lJ3djtHH4cVwnidW/ty53LjOBgfTNYwY27uxjLD8sXHSuq0CA750Y8GMfzrDMJRYgR0JX8RXVaBATPjbwwAqKktBpHaWsJWKIHoFAqSKNimGPKkirIjI2gDoKSMfOwHXdzWamZtDRENw55yKhaH5qug/N0FI/O4AVfOKzKqx4J780ksQcgg8AY61a284xQsZCLlT0ouKx8zyKfs4APIODTVjdmUgHJFX3tiLSRyp+VwPzq3Z2Li7iXy85AAz/ECDXTzKxXK7mMYixU9s46VWuIjg4HFb81sRO6HAwcjGOazJImMROODxmnGQSi0jIK5bpQykRD0JNXzbOoR9u5SpOQOOBz/OmT2xis4nbneSRj6/8A1q1uZcpmY5xSGpGGBUdaIzY36UtL/OkIxz3piEpRxSUue1ABThTacBxmhgTxHDrz3qwj4brVSHiQVKDg1DRcWdv4S1Q29x5Jb5W5A969QsrgOgORXhmmTGO4Vh26V6fo2ob4EyecY615uJhZ3R6dCXNGx3MUnGc8VYV81kW0+9Qcir6OCK5C2i3up4NVQ9L5hQ9iPWkFixmkY+lMWTcM0ucGpFYcTxUUgyvvTs4pOetA0VGjHpTPLG7NWmQk8UKmD7mmirleSFZU2MODXN6ppPlsxC5WuuC5HSmXEKyoVI61vCTiOE+Vnld1Yjnb8uOxqiUkgPBNdrq+lGM7kHB7iuZnjZchh9cV1RlGSOvlTV0Q2+vahZoBDKy47dR+Rq8njPUujtF9Sn+RWS6KD9P1qF41yeD/AIUOEWQ6S7G0PFl5GrbSjsxzlgf8aVvFuqTIUCxJ/tBT/jWKIo9x/Sp4toPA596OSIeyXYcBcXEjM7MzOcsSeprWsbEllXGWJ6VUgDHHHWuq0HTzjz5BgA/LnvUTaihyXKjShtlt4AigYAyaXyh6c1bZKaVrgm9TmTEiAAqfOBUajA4oLYFZibFZ8CuL8Z6wI7Q2MUg82Q/vAOy+/wBeK29a1iLS7RpmwzdETP3jXkd7ftdXMskrbmdizH1Jrsw1FyfMzKpPlQx3Ut1yaZIcg1ADtY1MQWz16Z6V6NrHNzXETk8eldToUTRL5gXLdvwrl4vvAGu48PqjNBGy5DEA469ayq7F0rN6noujWuy2iXf86x44zyx5Nec67bSNrtzG2SRJtr2m0a3gs0MKZyPvCvJZLiK51wzMn3rjfjPQZrBNkxs2xt9pxSZTt6zScj6j/Guj8M6fLLdKApIQbqq6tdILgQqB/rZG5PPO0f8AstdX4OP7qaTC4OAOfSs5JvQqUrRuay2c+MbRTk09wxbCg4rREq98fnS+cg7Uez8zDnZnixfdnK/lQdOLN97H4Vo+cmehpvnj0o5PMOdma+myHOD+NINOmCgFhwK0zcD+7+tN+1jsg/OnyeYc7Pme5VTYSgA87GOeas2EUUt5ZNc5MHmR+YCD93PPSmzIxjddvGOfw4psazvDCrE4xhc+gPQVunodfL3Lesx2a31y9ooSBmzGig8D05rm5mDMFUDk5Fbl3DK7eYQViGDgnkjtWBOhWQ49DV035mdVOwIIGt4AvmCYCXdyCvIG0j075qtc5GnRbv7xA9qb8yuck4x6+tNuX/0RVJH3un4V0dTnb0M5x8uc1CambpiomrVGDEpDSijFMQgHSkPWntxUZ96YDl5NLSdB9aCcYoAlh5kFPz81Rw8vS55NJjRo2L4lFdppc5QJg1w9iR54BrstOAKADtXJXR6GFZ22n3/ABJya3oLjcMZrh7d2TGG5HetyzuiDgnk9682cep2tXOoR8mph83Has2GYEDmrscmRzWZFrFhBgU7NRoe2akPH1pEsM07tmmgin4NAhpHekweOlPI9qaRk1SQXFpMU7sKaTTuMrTRK4IIH41z1/ogYloxnuRXTP06VAy5o52tjWnUcdjgbnSmQ8oRz3FUW04hjjg16HJEpzlaqvbI33o1P4Voq7OpVk90cINOYHkc1Zg0x3YbUzXYpaxKf9WoP0qzHGoOAlJ12J1ktkY2naEAUeUAAc49a6NYwi7VAAFIvSpR0rNzbOWpNyepG3I4phFSHmmP92oZAwkDJqle3aW0EksjBVRSSakuJljVmZgFUZJPQV5j4q8RHUJvJgkItYz2/jPr9K0pUnUlboKUlFXZQ8Q63LqV0XYkIOEX+6K5xpCW606aXcTznNVS3NexTpqKsjz6k3Jl1TuGfSp0k+dSRz6VTgY8Grjx9HXO09P8ACm0JMWM5fNdbp88iJGBk4UYx2rkkJWVSAOtdjpt0to4lWPdgYOe1Y1djSmdqmqGx8ITNJI5lKsUkU8gnpmuI0+Q7i55GQCPbNS6rqsl9CIE3bQ2XOeGPYcUWtoYo13g46kVko2Q76lqN2nvS3UsS3PoTkV6VoUa21jEg67ct9TXA6NbtLdmWQY5yTiu9truBABk4x6VDg29CJz6G2svpT1eRunWqKX9vjK4H1FTpfQ/3qfs2RctYcjrQwIPLYqIXkOM76uW8cV1EXIJCng5peybeg00tWQ7GpNknoKa1yI3KswGDR9riPSQUezYjyW70aKMSEMclN6qR3H3h/n1qPTfsWxcwgSRTqQcE5HfP5V6BrGgo1sksbkPGctjvXG/Y1tr5GIIVsBvzwf6muCNRtWZ6UZKaK3iz7KEV4LfYhyQACMnOR/PpXn1/FtkIK4yOM16/q629zbxxAIRgNk8E+361574it1juA0e3bzxjp/n+tdOFqdCZw5oXORnwGXjHyiqUzZjAPrkGrt3w+AcgdDVCYAqPWvUiedLQgILVGRkVNu2jGKj7VqjJjO1KBgE0oHSkc9uwpiIzkmjHNHNGaYCGkNBPNHXtTEyWH7zH0FANJGcROfWkU0Ai3bNtlU12mlSb1VsiuGjPzZrotGugSIy2GzxXPWhdHVh52lY7iNONy96uQ5U4qjYPviUnkGtFU715k0eoi/a3RTCsePWtiC4VlHNYA6e1WIJmiYc5GaxaG0dIkmelThwTg9ayoLjdjmr0cuQM1Bm0TbT5gYH6irKnI6mq4epkaqRLJQPWgr6U4HIzTsVokRci2elMZTnNWCOlJtzxVcg+YqEHBqJ1YcjirpQDjFRvGOc9KXsxqZnOcZzULOrdRV94Qe2ajaEYxiocDVSKSjI4FTKDgcVOsIA6EUoTHNL2YnMYi4qTHy55p6p3pSMf/Wo5bEORDwBVaaTaCM1NK2wHvXIeK9fGmWZSJ1+0ycKM8qO5ojBydkF7K7MTxj4ifc+n27DYR++Ydc/3f8fyrz6eYkkZqxc3JlUlmyx5z61lyMc161GkoKyOGrU5mOL89ab1qImno2Tiuixz3J4n2njrWrZzqytHINyMORWPjHP6VZifBHrUSRSZpNF5brgg+hrodOeS8kSDy8BsDKjJPsPc1ztvIWIB7+9dPpkkiokaqoGeDnmspI1TPQtH8B29vCbzVP3GPuQF/uj1Y+tS3ekRXl6sdojMpwMnqx9T6D61J4aSbU3jt7q7YMB8pLbifYE9DXf2OmW9im2EEE8knqaajzGUpOJj2Hg+xt4FDoTJj5iD3q8PDtmp4U/99VsBfelwfWtFBGXMzI/sC1HZvzpToVv2z+dawB9aPxo5EHMzGOiRDoxx9arzh7FPKimYAnOBXQEHHaqctn5vUj8qxq0217pvSqJP3jMgsFu03tLmQ/eqQ6GhP3wKjsi0F+UxwWwa3MH+7SpRTWo67alo9DlbrVU8tl2A7hjnmuA1ZzvcYO4cqSa6mZCecflWVqlmskJKrlgo+Yc4rwov3rs7qdkY1pch7YM3JUlcd+a5PxDMJJG2ZCqSRuGDg+1Xp3e3mIL4x1xWRcr54lZp4wSP4gcn8ga9CjBKVzao/daRy9yeckmqzEblUjvk1duFG7GcCqTKSc9q9OJ5MtyJ8M5I6DpUeKlJJPAx2pv3P97+VWQRuQowOpqP3NPOAMmo2NUiWITzTTS9TTWNUJsTrSjpTaVRkgetMRKeIlA7800U+bAk2jovFMoC5IrVaglMbBlOCDxVMGpUb3oauUmd74d1dXHlucNnOK7WFlZAQM5614zbTtFIroxDDoRXoHhvXFuB5UkgDgZwfSvOxNBr3kelh63MuVnXRrg9Kf5GRx3p8JEiD0IqQArgV5zOq5Am6E5Gav290G4quVB61HtZTkVIzdilBA5yKmR8MPSsWG4I4PWrcc+eM0Ilo2UcZqcNxWTFcYPU1cSXOOa0jIzcS3ilyBUSyZHWguDWikRYe7KRUZII6UwPx1qNn9zQ5oaiKcVExGTQzU0tntUORdh/UUDPTrioy2BgmjzAB1o5hWJwCB15qN3ABxUZlyOtVri4WONmZsDGSaV7isUdZ1SGxtnnnfCIM8DqewrxbV9Tk1G9mupT80jdPQdhW14v8RjVboQwO32aLp23t6/SuOmck9a9DDUeVXe5zV6nRCPLktzVdzmhjyaYxrtscjdxc0qmo+tOHvRYktR4YYPWpox26EVVQ4NXYgGGQeallotRblUHt61tafdMjKck47VjWxIYxngHpV+DdHICAVIPWspGiPRtA1ZEkjYKQynIweleu6PqsepWofgSDhh/WvBtMkMiqRtD/wB5TjNdzoV3dW8ylZCqMQG4B4rONTldgnTurnqgNLWfaB9gJdWJH8IxVxT7muhM5miSikzQWxTuIXpTTik3A0EjFFxpFBVUTscDOa0ByKxzdRJO4LjrWhBcxyR5Dd8VnFq5pKL3ZzD2QPUEVl6parBaFhkEnFdQYyfT86xdc5CxtyAc14cqdtTtpybZ5jq9nslLj7rc5NcvfPKqFCpTnvwa9A1WF5Y32gKAM5H9K4y9tP8ASQGcHHUIM5rsoSVtTpndx0OXmXcWYjHPAqnICVUHr/Kt/ULZQwIUDjkZzWRN8megrvhK6PPqQ5XZlEoF5HWon4PzHJp8jDPHWoDmtUYjHJzzUfU08jNNPANWiGxpNMpSabTELUkAzMn1zUdSwcSE+ik0xCO25yaKTvRmqSAcKepOKYKctMCZGIq7a3UkEoeNyrDoRWepqVGpON0XGVj1Tw54jjvIlilcLKByPWusiZZFGDkGvC7W5eCVZEbDKetej+GvEgu0WCbCyL+teVicNb3onpUK6krPc7ILg8jikYYPA4ohlDLg4wamEQ6r0rz2jqTKrJ3HBoWVk6mrHl9aiZeORUDJEn55NXIrnBBzWWUYDjBpFkYH+lFwtc6BLnd3qXzwawUuD+PtU6XfHPanzC5DVZx1BqNn5qmLjIpyzA+maOYOWxMzZ6Gk3EfSoDN2oMvWi4WJmcd6jMmRURlxUZk47UrhYsFwFJJ4rjPGWsvHZNBDJt8wEMR3HpXQXEjMhAOBivOvFlzvn2DsK6cPHmkjKo7RbOPnkO481VY8/WpJj81Qk17UVoeVJjW9aYelPIphqyLjBTwM96TrSjikFx6mrMMhRvUGq45qdVKj1HrUtFI043VgATx/KtOALNjLfvMY/wB6sOAlegyK0YT8wdGxjtWMkbRZv2jvAcbseoPQ11Gn6tLEoXdkehrjYrwlgrqCR3B610GnyxOCSSrDpkY/OueaNo2Z6bpfiUtCkcp3BRjoeBXRwazDIowSeOwNedaJbCS9ijlnBEnA2dj2rtINFCsoW6OPQAZojKfQynGCeptLfhxlQfxNSCYsRjvVWHT4o8ZZm9iavRokYwiAVsubqYu3QeuT14pxHBFN347GmPcKo5q72JtfYyJbaNrhyc5z61dhijjTAFc/daz5Vww2A8+tIPEDgfcFYKrBM7Xh60kja85I/vkViamVu7jEQJJ6D1NdS2lQt1Uf99Gue1fSAudmVGK5a1CpGOpFKpByOJ1Rh5TxyfJ2wOK5TUp7eLAU7zjOTwBXW6npbBZD6DiuN1DT9qhjtx05NRRitmd6emhzep3vnHCgAAnpWHLvetq7iROw64rKuSMkIuB6Zya9KCSWh51VtvUoSLt69qi5bip3QkZbioWIAwoxWqMCMgDio2PWnscCoj3qyGNNJSmkqgFq1CmLWaUnuEH8zVQVclwlnBGOrZkP4nA/QU0SV8UtApa0SAAaUdabinCmA8GnA0wGlBoC5OjYq9Z3TQyrIpIZTkHNZoNSq2KzlFM0hJp3PVvD+vLdxBXJ3jg5rrIJ8qCD9K8P0+/e1mVlYjnmvSNG1lZ41DN29a8jE4fld0epRqqat1OyzuUUxojioLe4D4GR0q2rbunavPkjcrlecdKaY+OKtsob/ComhPY/hUlXKvllW4P4Up68jipSCPvCmHFIAUgcgkfU1IOe9MyB2FOG31ouMdyPU5pMHjqaXcPWlwTnGfrTFcglLYwoHvmjaSKmEWDk9KCoUZpolsy9QkEVs5J7V5frkxluGNega9cbY3UHtivNdTk/fMT3r0cLHqc1d+6YkhO6mE8U+UjPFM6rXpxPNkKRxTCMGpcc0xhzWtiCMjBpy+tKRQBSaEX7CS1Vttwo2njdtz/9cfWtK40iS1iW4tpBc2jrneq8Y9/pWGnStGwvrixk3QSEA/eQ8q3sRUOJakSQ2rTECAMz/wBzHP4etPjHzehrprLRLfxKGl0aEwXUa75bYn5R7qeuM+tUrrQ72yybu1kjOfvMpA/PpWMjWLK0TNjaVDD9fwrWsrp4WAYl1PqOlZiwsv3atRb1GCBzWMkbRdjsdJ1BY5EYj7pBBHevVdO1C3u4o2hgYFgD1XA/I14XbB0wUYj8a7HQrzVljUW6u6juASP8KyXuscocyuesgjHIx+NPGAM1yOn3WoyHNwXXHYR10Fu7scvI/wCOK1jO5zyhYv5pkmNpyKdmIAgZz9aikYFTzVtijueeanJi8k+tUZGZmBBOMetSau4F7J/vVUWZSOa81uzZ9RGnemj2o9KydVJIBB4xitYVR1JR5IOOc169ZXifJ037xwerRs4f6Vxmo2cRhcbHL/X/AOtXoWoIrZBFcpqIVIjhFyQTkivKa5ZHq03oeZ6hANxXkYPesa4RVPyiut1TG8oFAUH0rl9RXZIcf3sV3UndHPWVnczJFznNVJgAeKvSnHSqMvPNdCOVkDGozUhqM1ZLGmkpTSUyR0UbSypGgyzEKB7mrN6R9pZVOUT5FPqBxSaedtzvHVUYj64x/WoWJJP1poBRS96aKcK1RItFFBoGLSg00dKKAJAaeG96ipwoBE6uc1taXqT27gbjj61gqamjJBzWNSCktTWE3F3R6tpWr7wuWByOldNbXIYD5q8o0u5lRVw1drp1xIUHNeNXpKLPYpy51c7CNwe9ScY9M1lW8rEVoJ83WuJqxbQ/aM01olJzjindMUuakRB9nXdmnLAPQ1MetKaLDuRLERnOMdqftFLSN3oEI3QAHmq11J5cVTY461Qv2Owj2NXFaiZyusSGRpK4TUl/0jJPGK7jVB8rVxGpnLkelerh1ocmIZjyrtb2qL1qZ+WqM9a7onDIkxTSvNSY/lSV0paGTIytGKf1oAyaGhAi55q7AgNV1FX7cDIpWA7r4bxk63Lhd37g/wDoS16adMgu3mjvElKTJ5bfN8p9Dz0PFcH8MBjxJxx/o7j+Veozov2Z2xg5BrCcdSkzxrxRoE3h7UTGWL28g3RSEfeHv71gG5wc9CK9r8U2UOoeDLt51y1viSMjqD0rw6UYYj0rNwRops17W93LlcAjrivUPh9frPbXFuDuwQ+frxXjVo7JMAD1r0X4dSMmtzKvAMTZ9+RWU4JFqTasesBVHIp/SoVJwPcVIORWQDyx9aikYhTTieKjbkGkyo7nmussRfSj/aNZnmH1q9rrEalN/vGscSMetcT1Z9ZR/ho//9k=
"} `
 
 - Response : 
	- *Success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`


## Get Account Offers:
- Api : `api/OffersSetting/GetAccountOffers`
- Method: `POST`
- parameters:

- Example :
      ` {  "AccountSetupId" :3095 }`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "Id": 1035,
            "NameAr": "عرض علي باقه العروسه",
            "NameEn": "عرض علي باقه العروسه",
            "DescriptionAr": null,
            "DescriptionEn": null,
            "Services": [
                {
                    "NameAr": "ويفي مع اكستنشن - شعر متوسط",
                    "NameEn": "ويفي مع اكستنشن - شعر متوسط",
                    "Id": 6322,
                    "BookingMinutes": 60,
                    "RankScore": null,
                    "Price": 8,
                    "OffersSettingId": 1035,
                    "CompanyServicesId": 3791,
                    "DiscountType": 2,
                    "DiscountValue": 50,
                    "MaxDiscountValue": 8,
                    "PriceAfterDiscount": 4
                },
                {
                    "NameAr": "x  service ",
                    "NameEn": "  service x",
                    "Id": 7437,
                    "BookingMinutes": 30,
                    "RankScore": null,
                    "Price": 50,
                    "OffersSettingId": 1035,
                    "CompanyServicesId": 4204,
                    "DiscountType": 1,
                    "DiscountValue": 30,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 20
                },
                {
                    "NameAr": "y",
                    "NameEn": "y",
                    "Id": 7438,
                    "BookingMinutes": 45,
                    "RankScore": 3,
                    "Price": 60,
                    "OffersSettingId": 1035,
                    "CompanyServicesId": 4205,
                    "DiscountType": 1,
                    "DiscountValue": 10,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 50
                }
            ]
        },
        {
            "Id": 3040,
            "NameAr": "عرض جديد 50%",
            "NameEn": "new offer 50%",
            "DescriptionAr": null,
            "DescriptionEn": null,
            "Services": [
                {
                    "NameAr": "ويفي مع اكستنشن - شعر متوسط",
                    "NameEn": "ويفي مع اكستنشن - شعر متوسط",
                    "Id": 6322,
                    "BookingMinutes": 60,
                    "RankScore": null,
                    "Price": 8,
                    "OffersSettingId": 3040,
                    "CompanyServicesId": 3791,
                    "DiscountType": 1,
                    "DiscountValue": 2,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 6
                },
                {
                    "NameAr": "x  service ",
                    "NameEn": "  service x",
                    "Id": 7437,
                    "BookingMinutes": 30,
                    "RankScore": null,
                    "Price": 50,
                    "OffersSettingId": 3040,
                    "CompanyServicesId": 4204,
                    "DiscountType": 1,
                    "DiscountValue": 0,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 50
                },
                {
                    "NameAr": "y",
                    "NameEn": "y",
                    "Id": 7438,
                    "BookingMinutes": 45,
                    "RankScore": 3,
                    "Price": 60,
                    "OffersSettingId": 3040,
                    "CompanyServicesId": 4205,
                    "DiscountType": 2,
                    "DiscountValue": 10,
                    "MaxDiscountValue": 0,
                    "PriceAfterDiscount": 54
                }
            ]
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
## Get blog posts:
- Api : `api/BlogPost/GlameraBlog`
- Method: `POST`
- parameters:

- Example :
      ` {}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "Id": 1,
            "Title": "Title ",
            "PostGMT": "2019-01-01T00:00:00",
            "ImagePath": null,
            "Author": "Maha  hassan"
        }
    ],
    "DetailedMessages": null,
    "MessageCode": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`


## Get blog posts comments  :
- Api : `api/BlogPostComment/get`
- Method: `POST`
- parameters:

- Example :
      ` {"BlogPostId":1}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "Id": 1,
            "BlogPostId": 1,
            "GlameraUserId": 1050,
            "CommentDate": "2019-01-01T00:00:00",
            "CommentGMTDate": "2019-01-01T00:00:00",
            "RowStatus": 1,
            "Content": " my    comment  on  blog  ",
            "NoOfLikes": 1,
            "NoOfDisLikes": 1
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "MessageCode": null,
    "Exception": null,
    "State": 100
}
`

## update  booking Status  :
- Api : `api/Booking/UpdateBookingStatus`
- Method: `POST`
- parameters:`ClassType == 'Glamera_User' if request  from  glamera   ` 'Glamera_Business' if request  from  Glamera Business   `
    

- Example :
      ` {
    "bookingStatus": -1,
    "AccountSetupId": 14,
    "Id": 7446,
    "ClassType": "Glamera_User"
}`
 
 - Response : 
	- *Success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "MessageCode": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
## Get Account OffDays   :
- Api : `api/WorkingHours/GetAccountOffDays`
- Method: `POST`
- parameters:`  `
    

- Example :
      ` {"AccountSetupId":14}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        5
    ],
    "DetailedMessages": null,
    "MessageCode": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
## getDropDowens lockup  data  :
- Api : `api/SystemCodes/GetDropdown`
- parameters:` 
2,	TimeZone
3	,Culture
4,	County
5	,SalesTaxSystem
6	,CurrncySystem
7	,PaymentType
8	,TransactionType
9	,Cities
10,	Area
11	,Classifications
12	,Memberships
13	,BusinessTypes
14	,BusinessSizes
15	,CancellationReasons 
`
- Example :
      ` {"MasterCodeId":13}`
      
## Get Glamera Offers  :
- Api : `api/GlameraHome/GetGlameraOffers`
- Method: `POST`
- parameters:` CountryId  is   required `
    

- Example :
      ` {"CountryId":1938}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "CompanyId": 3089,
            "Id": 1035,
            "Description": null,
            "Name": "عرض علي باقه العروسه",
            "CompanyName": "dodo",
            "Value": 200,
            "DiscountPercentage": 50,
            "ImagePath": "api/Upload/Images?na=7be58482-547d-40d9-a669-72284bc720f4.jpg&fld=Offers",
            "StartDate": "2019-05-12T00:00:00",
            "EndDate": "2019-06-12T00:00:00"
        },
        {
            "CompanyId": 10,
            "Id": 3039,
            "Description": "jjj",
            "Name": "jjj",
            "CompanyName": "beauty  company   1",
            "Value": 0,
            "DiscountPercentage": null,
            "ImagePath": null,
            "StartDate": "2019-05-08T00:00:00",
            "EndDate": "2020-05-11T00:00:00"
        },
        {
            "CompanyId": 4124,
            "Id": 4065,
            "Description": "Des of offer ",
            "Name": "offer for less than 5 visits",
            "CompanyName": "sara",
            "Value": 0,
            "DiscountPercentage": null,
            "ImagePath": null,
            "StartDate": "2019-05-30T00:00:00",
            "EndDate": "2019-06-15T00:00:00"
        },
        {
            "CompanyId": 4124,
            "Id": 4066,
            "Description": "Des of ",
            "Name": "off",
            "CompanyName": "sara",
            "Value": 0,
            "DiscountPercentage": null,
            "ImagePath": null,
            "StartDate": "2019-05-30T00:00:00",
            "EndDate": "2019-06-15T00:00:00"
        },
        {
            "CompanyId": 4136,
            "Id": 4071,
            "Description": "Des",
            "Name": "خصم علي منتجات الشعر",
            "CompanyName": "fathy",
            "Value": 0,
            "DiscountPercentage": null,
            "ImagePath": null,
            "StartDate": "2019-06-02T00:00:00",
            "EndDate": "2019-06-12T00:00:00"
        },
        {
            "CompanyId": 10,
            "Id": 5062,
            "Description": "fffff",
            "Name": "3rd esraa",
            "CompanyName": "beauty  company   1",
            "Value": 0,
            "DiscountPercentage": null,
            "ImagePath": null,
            "StartDate": "2019-06-03T00:00:00",
            "EndDate": "2019-06-12T00:00:00"
        },
        {
            "CompanyId": 10,
            "Id": 5063,
            "Description": "eeee",
            "Name": "eee",
            "CompanyName": "beauty  company   1",
            "Value": 0,
            "DiscountPercentage": null,
            "ImagePath": null,
            "StartDate": "2019-06-03T00:00:00",
            "EndDate": "2019-06-11T00:00:00"
        }
    ],
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`      
## Get packages :
- Api : `api/Ranks/GetAccountRankDetail`
- Method: `POST`
- parameters:` CountryId  is   required `
    

- Example :
      ` {"CountryId":1938}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "CompanyId": 2055,
            "Id": 1019,
            "Description": null,
            "Name": "باكدج عروسة 3500",
            "CompanyName": "Mado Beauty Center",
            "Value": 3500,
            "PriceBefore": null,
            "ImagePath": null,
            "StartDate": "2019-02-27T00:00:00",
            "EndDate": "2029-02-27T00:00:00"
        }
        
    ],
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`      
## Get account reviews    :
- Api : `api/Ranks/GetAccountRankDetail`
- Method: `POST`
- parameters:` RankedAccountSetupId  is   required `
    

- Example :
      ` {"RankedAccountSetupId":14} `
 
 - Response : 
	- *Success*:
    `{
    "ClientComments": [
        {
            "ClientId": 8,
            "ClientName": "اسراء محمد",
            "ClientImageUrl": null,
            "RankVaue": 5,
            "Comment": null,
            "AddedDate": "2019-05-12T16:16:02.077"
        }
    ],
    "RankingSummary": {
        "NoUsers": 1,
        "OverallRank": 5
    }
}
`      
## Get Offer Branches   :
- Api : `api/GlameraHome/GetOfferBranches`
- Method: `POST`
- parameters:` Id  is   required `
    

- Example :
      ` {
    "Result": [
        {
            "CompanyId": 6209,
            "CompanyName": "btot",
            "Address": "3 mahmoud roshdy street",
            "SiteName": "btot",
            "RankScore": null
        },
        {
            "CompanyId": 6209,
            "CompanyName": "btot",
            "Address": "3 dawod street",
            "SiteName": "btota2",
            "RankScore": null
        }
    ],
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`      
## Check Glamera Phone  is  available   :
- Api : `api/Account/CheckGlameraPhone`
- Method: `POST`
- parameters:` MobileNumber  is   required `
    

- Example :
      ` {
    "Result": false,
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`      

# Check Glamera PopUp Data  :
- Api : `api/GlameraHome/PopUpData
- Method: `POST`
- parameters:` ClientId  is   required ` 
    
- Example :
      `  {"Id":6342 , "CountryId": 1938}`
      
 - Response : 
	- *Success*:
      ` {
    "Result": {
        "NotRatedBooking": {
            "Id": 6523,
            "AccountSetupId": 7241,
            "bookingStatus": 3,
            "bookingStatusDesc": "Completed",
            "ClientId": 6342,
            "Date": "2019-06-24T00:00:00",
            "IsPaid": 0,
            "TotalTime": 0,
            "VisitNo": 23,
            "CompanyName": null,
            "Address": "egypt",
            "RankScore": 1.5,
            "SiteName": "yoyo",
            "TotalPrice": null,
            "CancellationReasonId": null,
            "CancellationComment": null,
            "CancellationDate": null,
            "CancelledBy": 0,
            "Currency": null,
            "IsRatedByClient": null,
            "IsRatedByProvider": null,
            "CompanyId": 7227,
            "CompanyImages": null
        },
        "NewOffers": {}
    },
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`      