## Wedding Day Event Application
## App url 
- http://localhost:2228/
- http://192.168.1.100:3000/
# # contant-type: 
- `application/json`

# 1- Create new  card  
- api : `api/WeddingEventData/Create`
- Method: `post`
- parameters:
	- Code [`require | max: 300`] 
	- Type [`required ` `1 ` or  `2`]
	- Description [`required `]
	- ImageFolder [`required `]
	- ImageName [`required`]
	- RowStatus	[`= 1`  `1  new  ` `2  assigned ` `3 executed `] 
	- Amount	[] 
	- ExecutionDate	[`required `] 
	- WinnerMobile	[``] 
	- Notes	[``] 
	
	
- Example :
		`{
  "Code": "AA001 ",
  "Type": "1",
  "Description": " Description here ",
  "ImageFolder": "ImageFolder",
  "ImageName": "ImageName.png",
  "RowStatus": "1",
  "Amount": null,
  "ExecutionDate": null,
  "WinnerMobile": "",
  "Notes": ""
}

`

- Response : 
	- *faild*:``
	- *success*:
	`{
    "Result": {
        "Id": 1,
        "Code": "AA001 ",
        "Type": 1,
        "Description": " Description here ",
        "ImageFolder": "CompanyImgs",
        "ImageName": "client01_20190319212029562.jpg",
        "RowStatus": 1,
        "Amount": null,
        "ExecutionDate": null,
        "WinnerMobile": "",
        "Notes": "",
        "AddedDate": "2019-04-08T14:35:05.1749068+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": "14",
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`


# 2 -Search  for cards  
- api : `api/WeddingEventData/Get`
- Method: `post`
- parameters:
	- Code [`require | max: 300`] 
    - RowStatus [`1 = new `]
		
- Example :`{"Code": "AA001 "}`

- Response : 
	- *faild*:
	``
	- *success*:
	`{
    "Result": [
        {
            "Id": 1,
            "Code": "AA001 ",
            "Type": 1,
            "Description": " Description here ",
            "ImageFolder": "CompanyImgs",
            "ImageName": "client01_20190319212029562.jpg",
            "RowStatus": 1,
            "Amount": null,
            "ExecutionDate": null,
            "WinnerMobile": "",
            "Notes": ""
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 0
}
`
# 3 - Check card State  
- api : `api/WeddingEventData/CheckState`
- Method: `post`

		
- Example :`{
  "Code": "AA001 ",
  "CheckState":1,
  "AccountSetupId":87878
}`

- Response : 
	- *success*:
	`{
    "Result": [
        {
            "Id": 1,
            "Code": "AA001 ",
            "Type": 1,
            "Description": " Description here ",
            "ImageFolder": "CompanyImgs",
            "ImageName": "client01_20190319212029562.jpg",
            "RowStatus": 2,
            "Amount": null,
            "ExecutionDate": null,
            "WinnerMobile": "0121212121",
            "Notes": ""
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 102
}
`
    - *faild*:
	`{
    "WeddingEventDatas": [
        {
            "Id": 1,
            "Code": "AA001 ",
            "Type": 1,
            "Description": " Description here ",
            "ImageFolder": "CompanyImgs",
            "ImageName": "client01_20190319212029562.jpg",
            "RowStatus": 3,
            "Amount": null,
            "ExecutionDate": null,
            "WinnerMobile": "0121212121",
            "Notes": ""
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": [
        {
            "Property": "WED1002",
            "Meta": "عفوا  هذا الكود تم إستخدامه بالفعل   ",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 102
}
`
# 3.2 - Check  ProviderId  
- api : `api/WeddingEventData/CheckProviderId`
- Method: `post`

		
- Example :`{
   "AccountSetupId":87878
}`

- Response : 
	- *success*:
	`{
    "Result": null,
    "TotalCount": 0,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}
`
    - *faild*:
	`{
    "Result": null,
    "TotalCount": 0,
    "DetailedMessages": [
        {
            "Property": "WED1006",
            "Meta": "عفوا  هذا الكود غير صحيح   ",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 102
}
`

# 4 -Update  status  or any data 
- api : `api/WeddingEventData/Update`
- Method: `post`

- Example :
		`{
    "Id": 1,
    "Code": "AA001 ",
    "Type": 1,
    "Description": " Description here ",
    "ImageFolder": "CompanyImgs",
    "ImageName": "client01_20190319212029562.jpg",
    "RowStatus": 2,
    "Amount": null,
    "ExecutionDate": null,
    "WinnerMobile": "0121212121",
    "Notes": ""
}
`
- Response : 
	- *faild*:
	``
	- *success*:
	`{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
# 4 - Upload  image file  as  MimeMultipartContent
- api : `api/Upload/PostImageFile`
- Method: `post`
- form-data:
	- AccountSetupId [`required = -1`] 
    - ImagesFolder [`WeddingImgs`]

- Response : 
	- *faild*:
	`{
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "",
            "Meta": "Processing of the HTTP request resulted in an exception. Please see the HTTP response returned by the 'Response' property of this exception for details.",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 101,
    "ValidationResult": null
}
`
    - *success*:
	`{
    "Result": {
        "FileName": "eticod_20190410140004544.jpg",
        "FolderName": "WeddingImgs"
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

