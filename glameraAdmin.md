## Glamera Admin Application
## App url 
- http://localhost:2228/
- http://192.168.1.100:3000/
# # contant-type: 
- `application/json`

##  Register 
- api : `api/Account/Register`
- Method: `post`
- parameters:
	- FirstName [`require | max: 300`] 
	- LastName [`required `]
	- Email [`required `]
	- BusinessName [`required `]
	- Email [`required `]
	- Password [`required`]
	- ConfirmPassword [` validate with password`]
	- BusinessType	[`= 1`] 
	- BusinessSize	[`= 49`] 
	- MobileNumber	[`required `] 
	- UserName	[`required `] 
	- Agreed	[`= true`] 
	- GlameraAdmin	[`= true`] 
	- ConfirmBy	[`= 2`] 
	
	
- Example :
		`{
  "FirstName": "han ",
  "LastName": "han101",
  "Email": "han@han.com",
  "BusinessName": "han salon",
  "Password": "123456",
  "ConfirmPassword": "123456",
  "BusinessType": "1",
  "BusinessSize": "49",
  "MobileNumber": "01100797944",
  "UserName": "han123",
  "Agreed": true,
  "GlameraAdmin": true ,
  "ConfirmBy":2
}
`
- Response : 
	- *faild*:
	``
	- *success*:
	`{
    "Result": {
        "UserId": 2098,
        "UserName": "han123",
        "AccountSetupId": 2075,
        "EmployeeId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  Verify or check  email  not taken    :
- api : `api/Account/CheckEmail`
- Method: `post`
- parameters:
	- Email [`require `] 
	
	
- Example :
		`{"Email":"test@tesffft"}
		`
        `"Result": false == > this mean that email already taken  `
- Response : 
	- *faild*:
	`
	`
	- *success*:
	`{
    "Result": false,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
	` 
    
##  Verify or check  user name    not taken    :
- api : `api/Account/CheckUserName`
- Method: `post`
- parameters:
	- Email [`require `] 
	
	
- Example :
		`{"UserName":"test@tesffft"}
		`
- Response : 
	- *faild*:
	`
	`
	- *success*:
	`{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
	` 
    
###  Verify Phone Number  :
- api : `api/Account/VerifyPhone`
- Method: `post`
- parameters:
	- UserId && PhoneNumber [`require `] 
    - UserName [`require  when  u  not  provide UserId && PhoneNumber `  ]
	- Code	[`require `]
	
- Example :
		`{
		    "UserId": 2098,
  			"PhoneNumber": 01100797944,
  			"Code": 900328
			}
		`
        `{
 
  "UserName": "ali3",
  "Code": 762308
}`
- Response : 
	- *faild*:
	`{
    "Result": false,
    "DetailedMessages": [
        {
            "Property": "",
           "Meta": "Failed to verify phone",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 107,
    "ValidationResult": null
	}
	`
	- *success*:
	`{
    "Result": {
        "UserId": 2098,
        "UserName": null,
        "AccountSetupId": 0,
        "EmployeeId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
	}
	` 

###  Resend Verify Phone Code:
- Api : `api/Account/ResendVerifyPhoneCode`
- Method: `post`
- parameters:
	- UserId [`require `] 
	- PhoneNumber [`require `]
- Example :
		`{
  	"UserId": 2098,
  	"PhoneNumber": "01100797944"
	}
	`
- Response : 
	- *faild*:
	` `
	- *success*:
	`{
    "Result": {
        "UserId": 2098,
        "UserName": null,
        "AccountSetupId": 0,
        "EmployeeId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
	}
	` 

###  Glamera Business Forget Password:
- Api : `api/Account/GlameraBusinessForgetPassword`
- Method: `post`
- Example :
		`{
    "UserName": "01100797944",
    "MobileNumber": "01100797944"
   }
	`
- Response : 
	- *faild*:
	` {
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "",
            "Meta": "No user with such name. ",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 107,
    "ValidationResult": null
}
{
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "",
            "Meta": "Phone number you entered  and   user  phone number dosen't match .",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 107,
    "ValidationResult": null
}
`
  - *success*:
	`{
    "Result": {
        "UserId": 10,
        "UserName": null,
        "AccountSetupId": 0,
        "EmployeeId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
    }
	` 

## Token  :
- api : `token`
- Method: `post`

- Example :
		`username=demo@demo&password=123456&grant_type=password&accountSetupId=null
		`
- Response : 
	- *faild*:
	`{
    "error": "1",
    "error_description": "The user name  is incorrect."
}
	`
    `{
    "error": "2",
    "error_description": "The password is incorrect.."
}
`
	`
    {
    "access_token": "PsOEgjvpblHp1LuJelrlWDFe-IH5r4du5_XBDRzGA_RY42y00mjCg4BquP4F8o5HXRdpyo0UO5C9zcewn_-QqR8ka0bAov0y36rtjNjPsSLzljlRCMeyW0jNWEiXfiCBpbXhXYo1Jrab-dPPCiiUbErlPal_Xo94HIMIjpgiUDJ_uKdmXtODN3coZYrB-UQv0BFJh_rSZof-6GAXzSQr_TuZyEMfwnSAW-ZnwdQyc2REYZ09dBkPMGgSF6nVfh-K-a2BpQOGcqkQQE4bcQAszl7rCkWakceuCnD94OcLu5R5R0AUYeGwdwT6xcfyJMqRYMHXJkyYiXj_pz0gDYcFAxPujCUND7za3RdPhnAHzXMhjhB-lt8c_JKQb75P8Wkw_7ozB3BCQ2DCNItdifSLVpNLR1roq38mcZI_EcxZ9iEDEAfzzlbaK-i3_TE3ayp_xeLChXJTO6HPPUrokF-ZbISbi2-jWgy2JrXRFJdKAFS8Vc3Hd6NmhTrhz4NCxOGwvZiW2jSBWFMlE0wiuo6-1ljbDNAednFIqydq42Qy3rzzNM4o13OFDpvXCQ2FLeUazPWF-gm2I480fv6DXBe5eI--z16e56-4jLd5oprYX1-EN--0KZtLxfIEBYdyyrh0Fc_U0sRgVrKpTBItW1s4-RhWQMWNHvRigP9v9vZI1rSsCvU78IFdxM3opHoqsmsBLWHoW-isesRFppWXpX9SHXdQaQVm_v8gOe8cdO3ZWZo",
    "token_type": "bearer",
    "expires_in": 1209599,
    "userName": "demo@demo",
    "userId": "14",
    "AccountSetupId": "14",
    "CompanyId": "10",
    "CountryId": "",
    "employeeId": "1",
    ".issued": "Sun, 09 Jun 2019 08:26:54 GMT",
    ".expires": "Sun, 23 Jun 2019 08:26:54 GMT"
}
	` 

###  Get Dropdowns:
- Api : `api/SystemCodes/GetDropdowns`
- Method: `post`

###  Get User  Branches:
- api : `api/Company/getBranches`
- Method: `post`
- parameters:
	- UserName [` `]   
	
- Example :
		`"yu123" `
- Response : 
	- *faild*:
	``
	- *success*:
	`{
  "Result": [
    {
      "RegionTimeZoneId": 60,
      "RegionTimeZone": null,
      "CultureInfoId": 15,
      "Culture": null,
      "SiteNameCulture": null,
      "AddressCulture": null,
      "Country": null,
      "PostCode": null,
      "ManagerFirstName": null,
      "ManagerFirstNameCulture": null,
      "ManagerLastName": null,
      "ManagerLastNameCulture": null,
      "CompanyNameCulture": null,
      "CompanyAddress": null,
      "CompanyAddressCulture": null,
      "CompanyRegionId": 0,
      "Regions": null,
      "CompanyStateId": 0,
      "States": null,
      "CompanyPostCode": null,
      "CompanyPhoneNumber": null,
      "CompanyMobileNumber": null,
      "CompanyEmail": null,
      "CompanyWebSiteAddress": null,
      "CompanyOwnerFirstName": null,
      "CompanyOwnerFirstNameCulture": null,
      "CompanyOwnerLastName": null,
      "CompanyOwnerLastNameCulture": null,
      "SunStartTime": null,
      "MonStartTime": null,
      "TuesStartTime": null,
      "WednesStartTime": null,
      "ThursStartTime": null,
      "FriStartTime": null,
      "SatStartTime": null,
      "SunEndTime": null,
      "MonEndTime": null,
      "TuesEndTime": null,
      "WednesEndTime": null,
      "ThursEndTime": null,
      "FriEndTime": null,
      "SatEndTime": null,
      "IsSunOff": false,
      "IsMonOff": false,
      "IsTuesOff": false,
      "IsWednesOff": false,
      "IsThursOff": false,
      "IsFriOff": false,
      "IsSatOff": false,
      "SpecificDays": null,
      "AllowTransactionVoiding": false,
      "TaxSystemOneId": null,
      "TaxOneRate": null,
      "TaxOneOrderInBill": null,
      "TaxSystemOne": null,
      "TaxSystemTwoId": null,
      "TaxTwoRate": null,
      "TaxTwoOrderInBill": null,
      "TaxSystemTwo": null,
      "TaxSystemThreeId": null,
      "TaxThreeRate": null,
      "TaxThreeOrderInBill": null,
      "TaxSystemThree": null,
      "AllowDeposit": false,
      "AccountCurrencies": null,
      "EnableTradingSystem": false,
      "DeclareTheRegistersBeforeTrading": null,
      "DeclareTheRegistersAfterTrading": null,
      "PctPaidForServicePerformed": 0,
      "PctPaidForProductSold": 0,
      "GiveLoyaltyDollarsPointsToYourClientsOnEveryTransaction": false,
      "NewClientsAreIncludedInTheLoyalitySystem": false,
      "ServicePointsAndDollarsAreGivenForEveryXDollarSpent": 0,
      "ServiceLoyaltyPointsGivenForEveryXDollarSpent": 0,
      "ServiceLoyaltyMoneyGivenForEveryXDollarSpent": 0,
      "ProductPointsAndDollarsAreGivenForEveryXDollarSpent": 0,
      "ProductLoyaltyPointsGivenForEveryXDollarSpent": 0,
      "ProductLoyaltyMoneyGivenForEveryXDollarSpent": 0,
      "LoyaltyPointsAreGivenPerVisit": 0,
      "LoyaltyMoneyAreGivenPerVisit": 0,
      "XPointsCount": null,
      "XPointsValue": null,
      "DashboardDisplayAll": false,
      "DashboardUpcomingAppointments": false,
      "DashboardPendingOnlineBookings": false,
      "DashboardSalesGraphs": false,
      "DashboardTasks": false,
      "DashboardWaitingLists": false,
      "PrintReceiptAdvertText": null,
      "PrintReceiptAdvertTextCulture": null,
      "PrintReceiptIncludeClientphoneNumber": false,
      "MessageOnNewBooking": false,
      "MessageOnReminder": false,
      "MessageOnBirthDay": false,
      "MessageOnWelcome": false,
      "MessageOnRetention": false,
      "DoYouHaveReceiptPrinter": false,
      "DoYouHaveCashDrawer": false,
      "WorkingHours": null,
      "BusinessSize": 49,
      "BusinessType": 1,
      "CompanyId": 2064,
      "Company": null,
      "Id": 2074,
      "SiteName": null,
      "PhoneNumber": null,
      "MobileNumber": null,
      "Email": null,
      "WebsiteAddress": null,
      "CompanyName": null,
      "State": 1,
      "RankScore": null,
      "Latitude": null,
      "Longitude": null,
      "CityId": null,
      "AreaId": null,
      "HaseInHomeService": null,
      "ServedGender": null,
      "HasTaxiService": null,
      "MostPopular": null,
      "MakeupArtist": 0,
      "Address": null,
      "CountryId": 910,
      "DefaultStartTime": null,
      "DefaultEndTime": null,
      "DefaultCurrency": null,
      "ActiveClassifications": null,
      "AccountImages": null,
      "AddedDate": "2019-03-31T12:12:49.293",
      "ModifiedDate": null,
      "IPAddress": null,
      "AddUserId": null,
      "ModifyUserId": null
    }
  ],
  "DetailedMessages": null,
  "Code": null,
  "Exception": null,
  "State": 100,
  "ValidationResult": null
} ` 



### Update Login Status(used to update device token to receive notification through firebase):
- api : `/api/GalemeraUsersToken/UpdateLoginStatus`
- Method: `post`
- parameters:
  
	
- Example :
		`{
    "DeviceId": "6ce14ccdde87ae7b",
    "Token": "dTuS_FY1GQU:APA91bFR3Ynd_iur403JAwa3DyC8VbFbBYdPCWgztzm_ujAtjFzfjSJ5O_UaR-LDlFZfmy5eKwgq3uZvEmIP81hSr1ckLCVjqtFxX0EKZc0nk9iyA4ydTKwLNDYpnOdEfivn6LOiXEmB",
    "UserId": 1046
}`
- Response : 
	- *faild*:
	``
	- *success*:
	`{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}` 

##  update account info :
- api : `/api/AccountSetup/UpdateInfo
- Method: `post`
- parameters:
	
	
- Example :
		`{
    "Address": "13st talat 7arb",
    "AreaId": 1233,
    "BusinessSize": 1952,
    "BusinessType": 1949,
    "CityId": 1044,
    "CountryId": 101,
    "Id": 47,
    "MobileNumber": "01060499088",
    "SiteName": "taha beauty"
}`
- Response : 
	- *faild*:
	``
	- *success*:
	`{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}` 
###   Get Account Working Hours :
- Api : `api/WorkingHours/Get`
- Method: `POST`
- parameters:
	- AccountSetupId [`require `] 
- Example :
		`{
        "AccountSetupId":2074
        }`
	- Response : 
	- *faild*:
	` `
	- *success*:
	`{
    "Result": [
        {
            "Id": 9491,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 6,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9490,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": true,
            "EmployeeId": null,
            "DayNo": 5,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9489,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 4,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9488,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 3,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9487,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 2,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9486,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 1,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            "Id": 9485,
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 0,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        }
    ],
    "TotalCount": 7,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100
}` 
###   Create Account Working Hours:
- Api : `api/WorkingHours/CreateList`
- Method: `POST`

- Example :
		` [
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 6,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
           
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": true,
            "EmployeeId": null,
            "DayNo": 5,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 4,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 3,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 2,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 1,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        },
        {
            
            "AccountSetupId": 2074,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "EmployeeId": null,
            "DayNo": 0,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0
        }
    ]`
    
	- Response : 
	- *faild*:
	` `
	- *success*:
	`{
    "Result": [
        {
            "Id": 9506,
            "AccountSetupId": 2074,
            "DayNo": 6,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9507,
            "AccountSetupId": 2074,
            "DayNo": 5,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": true,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9508,
            "AccountSetupId": 2074,
            "DayNo": 4,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9509,
            "AccountSetupId": 2074,
            "DayNo": 3,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9510,
            "AccountSetupId": 2074,
            "DayNo": 2,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9511,
            "AccountSetupId": 2074,
            "DayNo": 1,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        },
        {
            "Id": 9512,
            "AccountSetupId": 2074,
            "DayNo": 0,
            "StartTime": "10:30:00",
            "FinishTime": "23:30:00",
            "DayOff": false,
            "ShiftId": 1,
            "Date": null,
            "Description": null,
            "SpecialDayFor": 0,
            "EmployeeId": null,
            "RowState": 0,
            "AddedDate": "2019-04-03T18:18:21.4221389+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "14",
            "ModifyUserId": null
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}` 

###   Update  Account Working Hours:
- Api : `api/WorkingHours/UpdateList`
- Method: `POST`

- Example :
		` [
        {
            "Id": 240,
            "StartTime": "10:00:00",
            "FinishTime": "23:45:00",
            "DayOff": false,
            "DayNo": 6,
            "RowState": 0,
            "ShiftId": 1,
            "AccountSetupId": 3095
        },
        {
            "Id": 239,
            "StartTime": "10:00:00",
            "FinishTime": "23:45:00",
            "DayOff": false,
            "DayNo": 5,
            "RowState": 0,
            "ShiftId": 1,
            "AccountSetupId": 3095
        },
        {
            "Id": 238,
            "StartTime": "10:00:00",
            "FinishTime": "23:45:00",
            "DayOff": true,
            "DayNo": 4,
            "RowState": 0,
            "ShiftId": 1,
            "AccountSetupId": 3095
        },
        {
            "Id": 237,
            "StartTime": "10:00:00",
            "FinishTime": "23:45:00",
            "DayOff": false,
            "DayNo": 3,
            "RowState": 0,
            "ShiftId": 1,
            "AccountSetupId": 3095
        },
        {
            "Id": 236,
            "StartTime": "10:00:00",
            "FinishTime": "23:45:00",
            "DayOff": false,
            "DayNo": 2,
            "RowState": 0,
            "ShiftId": 1,
            "AccountSetupId": 3095
        },
        {
            "Id": 235,
            "StartTime": "10:00:00",
            "FinishTime": "23:00:00",
            "DayOff": false,
            "DayNo": 1,
            "RowState": 0,
            "ShiftId": 1,
            "AccountSetupId": 3095
        },
        {
            "Id": 234,
            "StartTime": "10:00:00",
            "FinishTime": "23:00:00",
            "DayOff": false,
            "DayNo": 0,
            "RowState": 0,
            "ShiftId": 1,
            "AccountSetupId": 3095
        }
    ]`
    
	- Response : 
	- *faild*:
	` `
	- *success*:
	`{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`
##  Get Company Images:
- Api : `/api/CompanyImages/Get`
- Method: `POST`
- parameters:
	- CompanyId [`required `]
- Example :
		`{"CompanyId":10}`
   - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":[{"Id":23,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"45221903_109322770069809_4678415314692603904_n_20190217172822743.jpg","MainImage":null,"SharedForAll":null,"RowStatus":1},{"Id":22,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"48427771_134789634189789_6830310714444349440_n_20190217172757403.jpg","MainImage":null,"SharedForAll":null,"RowStatus":1},{"Id":21,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"gdLKDDcC_400x400_20190217160701495.jpg","MainImage":null,"SharedForAll":null,"RowStatus":1},{"Id":2,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"Capture_20190212180808181.JPG","MainImage":true,"SharedForAll":null,"RowStatus":1},{"Id":1,"CompanyId":10,"DomainUrl":null,"ImageFolder":"CompanyImgs","ImageName":"download_20190212165258406.jpg","MainImage":true,"SharedForAll":null,"RowStatus":1}],"TotalCount":5,"DetailedMessages":null,"Code":null,"Exception":null,"State":0}`     

##   Get image  accounts  (where image should appear):
- Api : `api/CompanyImageAccounts/Get`
- Method: `POST`
- parameters:
	- CompanyImagesId [`required `]
- Example :
   {"CompanyImagesId":21}
   
    - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":[{"Id":16,"CompanyImagesId":21,"AccountSetupId":14,"MainFlag":true,"RowStatus":1,"ImageSort":1}],"TotalCount":1,"DetailedMessages":null,"Code":null,"Exception":null,"State":0}`     


##   Delete Image :
- Api : `api/CompanyImages/Delete`
- Method: `POST`
- parameters:
	- CompanyImagesId [`required `]
- Example :
   22
   
    - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":true,"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}`     

##   update  company list of  Images by  adding or  updating data of  existing ones :
- Api : `api/CompanyImages/UpdateList`
- Method: `POST`

- Example :
   `[
    {
        "RowStatus": 1,
        "ImageFolder": "CompanyImgs",
        "ImageName": "light-in-the-darkness_20190404094204011.jpg",
        "AccountSetupId": 3096,
        "CompanyId": 3090,
        "MainImage":true,
        "AddToSentAccount": true
    }
]`
   
    - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":true,"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}` 
##   update  company  Image (******* you can   use  updatelist) :
- Api : `api/CompanyImages/Update`
- Method: `POST`

- Example :
   `{"Id":36,"CompanyId":3089,"RowStatus":1,"ImageFolder":"CompanyImgs","ImageName":"apple-icon_20190502121300439.png","DomainUrl":null,"MainImage":true,"SharedForAll":null}`
   
    - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":true,"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}`     
##   add    company  Image to account (******* you can   use  updatelist)  :
- Api : `api/CompanyImageAccounts/Create`
- Method: `POST`

- Example :
   `[{"Id":null,"CompanyImagesId":36,"AccountSetupId":3095,"MainFlag":true,"RowStatus":1,"ImageSort":1}]`
   
    - Response : 
	- *faild*:
	` `
	- *success*:
    ` {
    "Result": [
        {
            "Id": 1029,
            "CompanyImagesId": 36,
            "AccountSetupId": 3095,
            "MainFlag": true,
            "RowStatus": 1,
            "ImageSort": 1,
            "AddedDate": "2019-05-06T07:42:29.9018858+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "3115",
            "ModifyUserId": null
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`     


##   Post Company Image File :
- Api : `api/Upload/PostImageFile`
- Method: `POST`
- parameters:
	- ImagesFolder [=`CompanyImgs `]
    - AccountSetupId [`Required `]
    - image [`binary `]

 - Response : 
	- *faild*:
	` `
	- *success*:
    ` {"Result":{"FileName":"light-in-the-darkness_20190404094204011.jpg","FolderName":"CompanyImgs"},"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}`  

##   create   Company Images (******* you can   use  updatelist)  :
- Api : `api/CompanyImages/Create`
- Method: `POST`
- parameters:
	- *AddToSentAccount*:` boolean ` ` only if  you  want  to  add this image to your account `
    - *AccountSetupId*:`only if  you  want  to  add this image to your account `

- Example :
      `[
    {
        "RowStatus": 1,
        "ImageFolder": "CompanyImgs",
        "ImageName": "light-in-the-darkness_20190404094204011.jpg",
        "AccountSetupId": 3096,
        "CompanyId": 3090,
        "MainImage":true,
        "AddToSentAccount": true
    }
]`
 - Response : 
	- *faild*:
	` `
	- *success*:
    ` {
    "Result": [
        {
            "Id": 35,
            "CompanyId": 3090,
            "DomainUrl": null,
            "ImageFolder": "CompanyImgs",
            "ImageName": "light-in-the-darkness_20190404094204011.jpg",
            "RowStatus": 1,
            "MainImage": true,
            "SharedForAll": null,
            "CompanyImageAccounts": [
                {
                    "Id": 28,
                    "CompanyImagesId": 35,
                    "AccountSetupId": 3096,
                    "MainFlag": true,
                    "RowStatus": 1,
                    "ImageSort": 0,
                    "AddedDate": "2019-04-21T17:40:14.3988394+02:00",
                    "ModifiedDate": null,
                    "IPAddress": null,
                    "AddUserId": null,
                    "ModifyUserId": null
                }
            ],
            "AddedDate": "2019-04-21T17:40:12.0271836+02:00",
            "ModifiedDate": null,
            "IPAddress": null,
            "AddUserId": "10",
            "ModifyUserId": null
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
} 
`


##   Add App Services To Account from wizard :
- Api : `api/AppServices/AddAppServicesToAccount`
- Method: `POST`
- parameters:
	

- Example :
      `[
    {
    	"AccountSetupId":3095,
        "Id": 381,
        "NameAr": "غسيل شعر + اكستنشن شعر طويل",
        "NameEn": "غسيل شعر + اكستنشن شعر طويل",
        "CategoryId": 44,
        "BookingMinutes": 30,
        "ProcessingMinutes": null,
        "FinishingMinutes": null,
        "Code": "",
        "RowStatus": 1,
        "Price": 30
    }
]`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`


##   Post new  employee :
- Api : `api/Employee/Create`
- Method: `POST`
- parameters:
	

- Example :
      `{
  "Id": 0,
  "Image": null,
  "NameEn": "صابر",
  "NameAr": "صابر",
  "MobileNumber": "01221323200",
  "Status": 1,
  "ShowInBookingScreen": true,
  "applyToAllFlag": true,
  "AttendMethodMasterId": null,
  "IdentityNumber": null,
  "AccountSetupId": 3095
}`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "Result": {
        "Id": 3137,
        "AccountSetupId": 3095,
        "Image": null,
        "ImagePath": null,
        "Name": " صابرين  ",
        "NameEn": " صابرين  ",
        "NameAr": "صابرين  ",
        "Phone": "",
        "MobileNumber": "01221322200",
        "WorkNumber": null,
        "Email": null,
        "CityId": 0,
        "AreaId": 0,
        "CountryId": 0,
        "Address": null,
        "AddressEn": null,
        "AddressAr": null,
        "PostCode": null,
        "BirthDate": null,
        "StartDate": null,
        "Status": 1,
        "Gender": 0,
        "AttendMethodMasterId": null,
        "IdentityNumber": null,
        "IdentityType": null,
        "FingerPrintCode": null,
        "ShowInBookingScreen": true,
        "ShowInRostersScreen": false,
        "Cost": 0,
        "PayrollType": 0,
        "CommissionProducts": 0,
        "CommissionService": 0,
        "ProviderNumber": null,
        "AAMTNumber": null,
        "ShowOnline": false,
        "OnlineDescription": null,
        "OnlineDetails": null,
        "RankScore": null,
        "Services": null,
        "WaitingItems": null,
        "Rosters": null,
        "ClockIns": null,
        "Transactions": null,
        "ServiceEmployee": null,
        "ClientServices": null,
        "PurchaseOrders": null,
        "BookingServices": null,
        "cashierEmployes": null,
        "StockEmployees": null,
        "ClientPackage": null,
        "CashierBalances": null,
        "MyUser": null,
        "AttendMethodMaster": null,
        "AddedDate": "2019-04-28T17:25:41.2629458+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": null,
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`

##   update   employee :
- Api : `api/Employee/Create`
- Method: `POST`
- parameters:
	

- Example :
      `{
    "Id": 3137,
    "AccountSetupId": 3095,
    "Image": null,
    "ImagePath": null,
    "Name": " صابرين  ",
    "NameEn": " صابرين  ",
    "NameAr": "صابرين  ",
    "Phone": "",
    "MobileNumber": "01221322210",
    "Email": null,
    "CityId": 0,
    "AreaId": 0,
    "CountryId": 0,
    "Address": null,
    "AddressEn": null,
    "AddressAr": null,
    "PostCode": null,
    "BirthDate": null,
    "Status": 1,
    "IdentityNumber": null,
    "IdentityType": null,
    "AttendMethodMasterId": null,
    "ShowInBookingScreen": true,
    "ShowInRostersScreen": false,
    "Cost": 0,
    "PayrollType": 0,
    "CommissionProducts": 0,
    "CommissionService": 0,
    "ProviderNumber": null,
    "AAMTNumber": null,
    "ShowOnline": false,
    "IsCashier": false,
    "OnlineDescription": null,
    "OnlineDetails": null,
    "RankScore": null,
    "WorkingHours": null,
    "ClockIns": null,
    "cashierEmployes": null,
    "AttendMethodMaster": null,
    "Rosters": [],
    "CashierId": 0
}`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`

## Get  account employee  with  basic  data only:
- Api : `api/Employee/Get`
- Method: `POST`
- parameters:
    - *ForListView*: `  to get basic data  only  ` ` 1  or  0  `
 
- Example :
      `{ "AccountSetupId": 3095 ,  "Id": 3137 , "ForListView":1 }`
      
- Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "Employees": [
        {
            "Id": 3137,
            "AccountSetupId": 3095,
            "Image": null,
            "ImagePath": null,
            "Name": " صابرين  ",
            "NameEn": " صابرين  ",
            "NameAr": "صابرين  ",
            "Phone": "",
            "MobileNumber": "01221322200",
            "Email": null,
            "CityId": 0,
            "AreaId": 0,
            "CountryId": 0,
            "Address": null,
            "AddressEn": null,
            "AddressAr": null,
            "PostCode": null,
            "BirthDate": null,
            "Status": 1,
            "IdentityNumber": null,
            "IdentityType": null,
            "AttendMethodMasterId": null,
            "ShowInBookingScreen": true,
            "ShowInRostersScreen": false,
            "Cost": 0,
            "PayrollType": 0,
            "CommissionProducts": 0,
            "CommissionService": 0,
            "ProviderNumber": null,
            "AAMTNumber": null,
            "ShowOnline": false,
            "IsCashier": false,
            "OnlineDescription": null,
            "OnlineDetails": null,
            "RankScore": null,
            "WorkingHours": null,
            "ClockIns": null,
            "cashierEmployes": null,
            "AttendMethodMaster": null,
            "Rosters": null,
            "CashierId": 0
        }
    ],
    "AttendMethodMasters": [],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 0
}`

##   Post new  consultation :
- Api : `api/ClientConsultation/Create`
- Method: `POST`
- parameters:
	

- Example :
      `{
    "ClientId": 3333,
    "Message": "My  new   consultaion   انا  محتاج استشاره  للتعامل مع ",
    "RowStatus": 1,
    "Notes": ""
}`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "Result": {
        "Id": 3,
        "ClientId": 3333,
        "Message": "My  new   consultaion   انا  محتاج استشاره  للتعامل مع ",
        "PickUpDate": null,
        "PickUpAccountId": null,
        "RowStatus": 1,
        "Notes": "",
        "AddedDate": "2019-04-07T12:36:37.3968307+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": "14",
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`
    	
##   Pickup  one consultation :
- Api : `api/AssignedConsultation/Create`
- Method: `POST`
- parameters:
	

- Example :
      `{
    "ClientConsultationId": 3,
    "AccountId": 14,
    "PickUpDate": "2019-04-07T12:36:37.3968307+02:00",
    "PickUpStatus":1
}`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "Result": {
        "Id": 1,
        "ClientConsultationId": 3,
        "AccountId": 14,
        "PickUpDate": "2019-04-07T12:36:37.3968307+02:00",
        "PickUpStatus": 1,
        "Replay": null,
        "ReplayDate": null,
        "AddedDate": "2019-04-07T12:58:22.4252709+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": "14",
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`
    
    
##   Get  pending  consultation :
- Api : `api/ClientConsultation/GetUnPicked`
- Method: `POST`
- parameters:
	

- Example :
      `{}`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "ClientConsultations": [
        {
            "Id": 3,
            "ClientId": 3333,
            "Message": "My  new   consultaion   انا  محتاج استشاره  للتعامل مع ",
            "PickUpDate": null,
            "PickUpAccountId": null,
            "RowStatus": 1,
            "Notes": ""
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 0
}`

##   Get  any    consultation  for me  as  consultant :
- Api : `api/ClientConsultation/Get`
- Method: `POST`
- parameters:
	- ConsultationState [ `new =1`  or  `PickedUp = 2` or `UnPickedUp = 3` or  `Canceled = -1` or  `Replied =4` `]
    - PickUpAccountId [`Required ` ` refers to my account id ` ]

- Example :
      `{
    "PickUpAccountId": 14,
    "ConsultationState": 1
}`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "ClientConsultations": [
        {
            "Id": 3,
            "ClientId": 3333,
            "Message": "My  new   consultaion   انا  محتاج استشاره  للتعامل مع ",
            "PickUpDate": null,
            "PickUpAccountId": null,
            "RowStatus": 1,
            "Notes": ""
        }
    ],
    "TotalCount": 1,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 0
}`
##   Replay on  one consultation :
- Api : `api/AssignedConsultation/Update`
- Method: `POST`
- parameters:
	

- Example :
      `{
    "Id": 1,
        "ClientConsultationId": 3,
        "AccountId": 14,
        "PickUpDate": "2019-04-07T12:36:37.3968307+02:00",
        "PickUpStatus": 4,
        "Replay": "  الرد  على  الأستشاره ",
        "ReplayDate": "2019-04-07T12:36:37.3968307+02:00",
}`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}`
    
    
##   Services   list  (my services) :
- Api : `/api/Service/GetBasic`
- Method: `POST`
- parameters:
	

- Example :
      `{"CategoryId":null,"PageSize":5,"PagingEnabled":true,"PageNumber":0,"AccountSetupId":2074}`
 - Response : 
	- *faild*:
	` `
	- *success*:
    `{
    "Services": [
        {
            "Id": 3994,
            "AccountSetupId": 14,
            "Image": null,
            "Price": 0,
            "BookingMinutes": 5,
            "Description": "ii",
            "NameAr": "ii",
            "NameEn": "ii",
            "IsProcessTimeRequired": false,
            "ProcessingMinutes": null,
            "FinishingMinutes": null,
            "Details": null,
            "Code": "ii",
            "CategoryId": 72,
            "DummyCategoryId": null,
            "Category": null,
            "Status": 1,
            "TaxRate": 0,
            "AverageCost": 0,
            "ShowOnline": false,
            "OnlineDescription": null,
            "ServiceDetails": null,
            "PointsRedemption": null,
            "LoyaltyPoints": 0,
            "LoyaltyMoney": 0,
            "LoyaltyPointSystem": 0,
            "RankScore": null,
            "OffersSettingId": null,
            "DoneInHome": 0,
            "AppServicesId": null,
            "CompanyServicesId": null,
            "AppCategoryName": null,
            "serviceEmployee": [],
            "ServiceResource": null,
            "ServiceConsumables": null
        }
    ],
    "TotalCount": 200,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 0
}
`

##   Service  update  :
- Api : `/api/Service/Update`
- Method: `POST`
- parameters:
	

- Example :
      `{
  "Id": 574,
  "employeeId": null,
  "Image": null,
  "CategoryId": 72,
  "Code": "1002",
  "Description": "غسيل شعر مع اكستنشن",
  "Status": 1,
  "BookingMinutes": 30,
  "IsProcessTimeRequired": false,
  "ProcessingMinutes": null,
  "FinishingMinutes": null,
  "Price": 2,
  "TaxRate": 0,
  "AverageCost": 2,
  "ShowOnline": false,
  "OnlineDescription": null,
  "ServiceDetails": null,
  "PointsRedemption": 1,
  "LoyaltyPoints": 5,
  "LoyaltyMoney": 2,
  "LoyaltyPointSystem": 2,
  "EquipmentId": null,
  "RoomId": null,
  "ServiceResource": [],
  "serviceEmployee": [],
  "ServiceConsumables": [],
  "AccountSetupId": 14
}`
	- *faild*:
	` `
	- *success*:
    `{"Result":true,"DetailedMessages":null,"Code":null,"Exception":null,"State":100,"ValidationResult":null}
`

##   Service  create  :
- Api : `/api/Service/Create`
- Method: `POST`
- parameters:
	

- Example :
      `{
    "Id": 0,
    "employeeId": null,
    "Image": null,
    "CategoryId": 3550,
    "Code": "code7",
    "NameAr": "test new service 7",
    "Status": 1,
    "BookingMinutes": "30",
    "IsProcessTimeRequired": null,
    "ProcessingMinutes": null,
    "FinishingMinutes": null,
    "Price": 40,
    "TaxRate": 12,
    "AverageCost": 44.8,
    "ShowOnline": null,
    "OnlineDescription": null,
    "ServiceDetails": null,
    "PointsRedemption": null,
    "LoyaltyPoints": null,
    "LoyaltyMoney": null,
    "LoyaltyPointSystem": null,
    "EquipmentId": null,
    "RoomId": null,
    "ServiceResource": [],
    "serviceEmployee": [],
    "ServiceConsumables": [],
    "AccountSetupId": 3095
}`
	- *faild*:
	` {
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "UnuniqueCode",
            "Meta": "Service code must be unique",
            "MetaPlus": ""
        },
        {
            "Property": "UnuniqueDESC",
            "Meta": "Service description must be unique",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 102,
    "ValidationResult": null
}`
	- *success*:
    `{
    "Result": {
        "Id": 6444,
        "AccountSetupId": 3095,
        "Image": null,
        "Details": null,
        "Code": "code7",
        "Description": null,
        "NameAr": "test new service 7",
        "NameEn": "test new service 7",
        "Category": null,
        "CategoryId": 3550,
        "Status": 1,
        "BookingMinutes": 30,
        "IsProcessTimeRequired": false,
        "ProcessingMinutes": null,
        "FinishingMinutes": null,
        "Price": 40,
        "TaxRate": 12,
        "AverageCost": 44.8,
        "ShowOnline": false,
        "OnlineDescription": null,
        "ServiceDetails": null,
        "ServiceResource": null,
        "Employees": null,
        "PointsRedemption": null,
        "LoyaltyPoints": 0,
        "LoyaltyMoney": 0,
        "LoyaltyPointSystem": 0,
        "RankScore": null,
        "DoneInHome": 0,
        "AppServicesId": null,
        "CompanyServicesId": null,
        "WaitingItems": null,
        "BookingServices": null,
        "ClientServices": null,
        "TransactionsProductOrService": null,
        "serviceEmployee": [],
        "AddedDate": "2019-05-01T10:17:09.8009654+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": "3115",
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

##   Get Cancellation Reasons:
- Api : `/api/SystemCodes/GetCancellationReasons`
- Method: `POST`
- parameters:
	

- Example :
      `{"Lang":"ar"}`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": [
        {
            "dbId": 1955,
            "id": "1",
            "cid": "",
            "cnid": null,
            "na": "حسب رغبة العميل"
        },
        {
            "dbId": 1956,
            "id": "2",
            "cid": "",
            "cnid": null,
            "na": "حسب حالة مزود الخدمة"
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

##   Get pending orders  :
- Api : `/api/Booking/GetDynamic
- Method: `POST`
- bookingStatus:
	`public enum BookingStatus
    {
        Canceled = -1,
        Unconfirmed = 0 ,
        Confirmed = 1,
        Arrived = 2,
        Completed = 3,
        SurveyCompleted = 4,
        DidNotShow = 5
    }`

- Example :
      `{"PageSize":5,"PagingEnabled":true,"PageNumber":0,"AccountSetupId":14 , "bookingStatus":0 ,"Date": "2019-4-15"  }`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": [
        {
            "Id": 3250,
            "ClientId": 4,
            "StartTime": "2019-03-25T04:00:00",
            "EndTime": "2019-03-25T04:00:00",
            "AccountSetupId": 14,
            "IsPaid": 0,
            "Comment": null,
            "CancelledBy": null,
            "CancellationComment": null,
            "CancellationReasonId": null,
            "CancellationReason": null,
            "Time": "04:00:00",
            "bookingStatus": 0,
            "TotalTime": 30,
            "BookingServices": [
                {
                    "Id": 3515,
                    "EmployeeId": 5,
                    "ServiceId": 386,
                    "AccountSetupId": 14,
                    "BookingId": 3250,
                    "Date": "2019-03-25T04:00:00",
                    "Service": {
                        "Id": 386,
                        "NameAr": "قص أطراف الشعر",
                        "NameEn": "قص أطراف الشعر",
                        "Code": "",
                        "Price": 0,
                        "HasOffer": 0,
                        "PriceBefore": 3,
                        "BookingMinutes": 15
                    }
                },
                {
                    "Id": 3516,
                    "EmployeeId": 5,
                    "ServiceId": 387,
                    "AccountSetupId": 14,
                    "BookingId": 3250,
                    "Date": "2019-03-25T04:00:00",
                    "Service": {
                        "Id": 387,
                        "NameAr": "قص غرة",
                        "NameEn": "قص غرة",
                        "Code": "",
                        "Price": 0,
                        "HasOffer": 0,
                        "PriceBefore": 2,
                        "BookingMinutes": 15
                    }
                }
            ],
            "Client": {
                "Id": 4,
                "NameAr": "تقي اشرف",
                "NameEn": "تقي اشرف",
                "Mobile": "02932383838"
            }
        },
        {
            "Id": 3251,
            "ClientId": 4,
            "StartTime": "2019-03-25T04:00:00",
            "EndTime": "2019-03-25T04:00:00",
            "AccountSetupId": 14,
            "IsPaid": 0,
            "Comment": null,
            "CancelledBy": null,
            "CancellationComment": null,
            "CancellationReasonId": null,
            "CancellationReason": null,
            "Time": "04:00:00",
            "bookingStatus": 0,
            "TotalTime": 30,
            "BookingServices": [
                {
                    "Id": 3517,
                    "EmployeeId": 5,
                    "ServiceId": 386,
                    "AccountSetupId": 14,
                    "BookingId": 3251,
                    "Date": "2019-03-25T04:00:00",
                    "Service": {
                        "Id": 386,
                        "NameAr": "قص أطراف الشعر",
                        "NameEn": "قص أطراف الشعر",
                        "Code": "",
                        "Price": 0,
                        "HasOffer": 0,
                        "PriceBefore": 3,
                        "BookingMinutes": 15
                    }
                },
                {
                    "Id": 3518,
                    "EmployeeId": 5,
                    "ServiceId": 387,
                    "AccountSetupId": 14,
                    "BookingId": 3251,
                    "Date": "2019-03-25T04:00:00",
                    "Service": {
                        "Id": 387,
                        "NameAr": "قص غرة",
                        "NameEn": "قص غرة",
                        "Code": "",
                        "Price": 0,
                        "HasOffer": 0,
                        "PriceBefore": 2,
                        "BookingMinutes": 15
                    }
                }
            ],
            "Client": {
                "Id": 4,
                "NameAr": "تقي اشرف",
                "NameEn": "تقي اشرف",
                "Mobile": "02932383838"
            }
        },
        {
            "Id": 3252,
            "ClientId": 30,
            "StartTime": "2019-03-26T00:00:00",
            "EndTime": "2019-03-26T00:00:00",
            "AccountSetupId": 14,
            "IsPaid": 0,
            "Comment": null,
            "CancelledBy": null,
            "CancellationComment": null,
            "CancellationReasonId": null,
            "CancellationReason": null,
            "Time": "00:00:00",
            "bookingStatus": 0,
            "TotalTime": 60,
            "BookingServices": [
                {
                    "Id": 3519,
                    "EmployeeId": 6,
                    "ServiceId": 574,
                    "AccountSetupId": 14,
                    "BookingId": 3252,
                    "Date": "2019-03-26T00:00:00",
                    "Service": {
                        "Id": 574,
                        "NameAr": "غسيل شعر مع اكستنشن",
                        "NameEn": "غسيل شعر مع اكستنشن",
                        "Code": "",
                        "Price": 0,
                        "HasOffer": 0,
                        "PriceBefore": 2,
                        "BookingMinutes": 30
                    }
                },
                {
                    "Id": 3520,
                    "EmployeeId": 5,
                    "ServiceId": 575,
                    "AccountSetupId": 14,
                    "BookingId": 3252,
                    "Date": "2019-03-26T00:00:00",
                    "Service": {
                        "Id": 575,
                        "NameAr": "غسيل شعر + اكستنشن شعر طويل",
                        "NameEn": "غسيل شعر + اكستنشن شعر طويل",
                        "Code": "",
                        "Price": 0,
                        "HasOffer": 0,
                        "PriceBefore": 5,
                        "BookingMinutes": 30
                    }
                }
            ],
            "Client": {
                "Id": 30,
                "NameAr": "yousef shaaban",
                "NameEn": "yousef shaaban",
                "Mobile": "201060499088"
            }
        },
        {
            "Id": 3254,
            "ClientId": 3164,
            "StartTime": "2019-03-26T00:00:00",
            "EndTime": "2019-03-26T00:00:00",
            "AccountSetupId": 14,
            "IsPaid": 0,
            "Comment": null,
            "CancelledBy": null,
            "CancellationComment": null,
            "CancellationReasonId": null,
            "CancellationReason": null,
            "Time": "04:15:00",
            "bookingStatus": 0,
            "TotalTime": 60,
            "BookingServices": [
                {
                    "Id": 3522,
                    "EmployeeId": 15,
                    "ServiceId": 510,
                    "AccountSetupId": 14,
                    "BookingId": 3254,
                    "Date": "2019-03-26T04:15:00",
                    "Service": {
                        "Id": 510,
                        "NameAr": "منكير عادي",
                        "NameEn": "منكير عادي",
                        "Code": "",
                        "Price": 3,
                        "HasOffer": 0,
                        "PriceBefore": 3,
                        "BookingMinutes": 30
                    }
                },
                {
                    "Id": 3523,
                    "EmployeeId": 5,
                    "ServiceId": 511,
                    "AccountSetupId": 14,
                    "BookingId": 3254,
                    "Date": "2019-03-26T05:15:00",
                    "Service": {
                        "Id": 511,
                        "NameAr": "منكير سبا",
                        "NameEn": "منكير سبا",
                        "Code": "",
                        "Price": 5,
                        "HasOffer": 0,
                        "PriceBefore": 5,
                        "BookingMinutes": 30
                    }
                }
            ],
            "Client": {
                "Id": 3164,
                "NameAr": "Dalia Ahmed",
                "NameEn": "Dalia Ahmed",
                "Mobile": "201096817506"
            }
        },
        {
            "Id": 4406,
            "ClientId": 30,
            "StartTime": "2019-05-22T00:00:00",
            "EndTime": "2019-05-22T00:00:00",
            "AccountSetupId": 14,
            "IsPaid": 1,
            "Comment": null,
            "CancelledBy": null,
            "CancellationComment": null,
            "CancellationReasonId": null,
            "CancellationReason": null,
            "Time": "10:15:00",
            "bookingStatus": 0,
            "TotalTime": 45,
            "BookingServices": [
                {
                    "Id": 4777,
                    "EmployeeId": 6,
                    "ServiceId": 574,
                    "AccountSetupId": 14,
                    "BookingId": 4406,
                    "Date": "2019-05-22T10:15:00",
                    "Service": {
                        "Id": 574,
                        "NameAr": "غسيل شعر مع اكستنشن",
                        "NameEn": "غسيل شعر مع اكستنشن",
                        "Code": "",
                        "Price": 2,
                        "HasOffer": 0,
                        "PriceBefore": 2,
                        "BookingMinutes": 30
                    }
                },
                {
                    "Id": 4778,
                    "EmployeeId": 6,
                    "ServiceId": 573,
                    "AccountSetupId": 14,
                    "BookingId": 4406,
                    "Date": "2019-05-22T13:00:00",
                    "Service": {
                        "Id": 573,
                        "NameAr": "غسيل شعر عادي",
                        "NameEn": "غسيل شعر عادي",
                        "Code": "",
                        "Price": 1,
                        "HasOffer": 0,
                        "PriceBefore": 1,
                        "BookingMinutes": 15
                    }
                }
            ],
            "Client": {
                "Id": 30,
                "NameAr": "yousef shaaban",
                "NameEn": "yousef shaaban",
                "Mobile": "201060499088"
            }
        }
    ],
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  rank app package   :
- Api : `/api/Ranks/Create
- Method: `POST`

- Example :
      `[
    {
        "RankedAppPackageId": 1,
        "RankVaue": 4,
        "ClientId": 30
    }
]`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##- rank client    :
- Api : `/api/Ranks/Create
- Method: `POST`

- Example :
      `[
    {
        "RankedClientId": 30,
        "RankVaue": 5,
        "RankingAccountSetupId": 14
    }
]`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##   Update Booking Status ( confirm  or  complete  no show  )    :
- Api : `/api/Booking/UpdateBookingStatus
- Method: `POST`
- If you want  to change  time or comment  
 `"BookingServices": [
        {
            "Id": 4720,
            "EstimatedTime": 75,
            "Note": "التعليق ",
            "EstimatedTimeUpdateStatus": true
        }
    ]`
- Parameters:
 	- *Id*:`Booking Id  `
    - *bookingStatus*:`bookingStatus -1  for cancel      `
- Enum : 
      `{
        Canceled = -1,
        Unconfirmed = 0 ,
        Confirmed = 1,
        Arrived = 2,
        Completed = 3,
        SurveyCompleted = 4,
        DidNotShow = 5
    }`
    
- Example :
      `{
    "Id": 4378,
    "bookingStatus": "3",
    "BookingServices": [
        {
            "Id": 4720,
            "EstimatedTime": 75,
            "Note": "التعليق ",
            "EstimatedTimeUpdateStatus": true
        }
    ]
}
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 

##   cancellation  with reason and comment      :
- Api : `/api/Booking/UpdateBookingStatus
- Method: `POST`
- Parameters:
 	- *Id*:`Booking Id  `
    - *Id*:`bookingStatus -1  for cancel    `
- Example :
      `{
    "Id": 5432,
    "bookingStatus": -1,
    "CancellationComment": " comment  on cancel ",
    "CancellationReasonId": 1955,
    "CancellationDate": "2019-04-15T00:00:00",
    "BookingServices":[]
}
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 


##   Missing Setup on glamera business      :
- Api : `/api/account/MissingSetup
- Method: `POST`
- Parameters:
 	
- Example :
      `{"AccountSetupId":3095}
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": {
        "AccountSetupId": null,
        "Services": 1,
        "Categories": 1,
        "Employees": 1,
        "WorkingHours": 1,
        "Images": 0
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 

##  create  new  Offers or  packge  (OffersSetting)      :
- Api : `/api/OffersSetting/Create
- Method: `POST`
- Parameters:
 	
- Example :
      `{
    "Id": 0,
    "NameAr": "باقة  تجهيز  عروس",
    "NameEn": "باقة  تجهيز  عروس",
    "Cost": null,
    "Value": 3000,
    "PercentageDiscountValue": null,
    "MaxDiscountValue": null,
    "OfferType": 4,
    "Status": 1,
    "IsOnlineOnly": null,
    "accountId": null,
    "SponsoredOffer": null,
    "OffersAccounts": [
        {
            "Id": 0,
            "OffersSettingId": 0,
            "AccountSetupId": 3095,
            "RowState": 1,
            "AccountSetup": null
        }
    ],
    "OfferDetails": null,
    "Image": null,
    "ImagePath": null,
    "Promocode": null,
    "DescriptionEn": null,
    "DescriptionAr": null,
    "OffersDetails": [
        {
            "Id": 0,
            "CompanyServicesId": 213,
            "Service": {
                "Id": 213,
                "CompanyId": 3089,
                "NameAr": "test new service 8",
                "NameEn": null,
                "CategoryId": 3550,
                "BookingMinutes": 30,
                "ProcessingMinutes": null,
                "FinishingMinutes": null,
                "AppServicesId": null,
                "Price": 40,
                "ShowOnline": false,
                "LoyaltyPoints": 0,
                "LoyaltyPointSystem": 0,
                "RankScore": 0,
                "DoneInHome": false,
                "PointsRedemption": null,
                "OnlineDescription": null,
                "TaxRate": 12,
                "STATUS": 0,
                "IsProcessTimeRequired": false
            },
            "RowState": 1,
            "RepetitionCount": 1,
            "InOutOfferNature": 1,
            "IsMandatory": null
        }
    ],
    "StartDate": "2019-04-30T00:00:00.000Z",
    "EndDate": "2020-04-30T00:00:00.000Z"
}
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": {
        "Id": 2043,
        "NameAr": "باقة  تجهيز  عروس",
        "NameEn": "باقة  تجهيز  عروس",
        "Image": null,
        "ImagePath": null,
        "DescriptionEn": null,
        "DescriptionAr": null,
        "Cost": null,
        "Value": 3000,
        "StartDate": "2019-04-30T00:00:00Z",
        "EndDate": "2020-04-30T00:00:00Z",
        "Status": 1,
        "IsOnlineOnly": 0,
        "PercentageDiscountValue": null,
        "MaxDiscountValue": null,
        "SponsoredOffer": null,
        "Promocode": null,
        "RepetitionCount": null,
        "OfferType": 4,
        "OffersAccounts": [
            {
                "Id": 2043,
                "AccountSetupId": 3095,
                "OffersSettingId": 2043,
                "AccountSetup": null,
                "RowState": 1,
                "AddedDate": "2019-05-02T09:22:15.5653956+02:00",
                "ModifiedDate": null,
                "IPAddress": null,
                "AddUserId": null,
                "ModifyUserId": null
            }
        ],
        "OffersDetails": [
            {
                "Id": 1082,
                "ComapnyCategoriesId": null,
                "CompanyServicesId": 213,
                "RetailProductId": null,
                "InOutOfferNature": 1,
                "RepetitionCount": 1,
                "IsMandatory": 0,
                "OffersSettingId": 2043,
                "ComapnyCategories": null,
                "CompanyServices": null,
                "RetailProduct": null,
                "RowState": 1,
                "AddedDate": "2019-05-02T09:22:15.5653956+02:00",
                "ModifiedDate": null,
                "IPAddress": null,
                "AddUserId": null,
                "ModifyUserId": null
            }
        ],
        "ClientPromotions": null,
        "ClientProducts": null,
        "ClientServices": null,
        "ClientPackage": null,
        "TransactionsProductOrService": null,
        "AddedDate": "2019-05-02T09:22:15.5653956+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": "3115",
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 
##   upadte  offer setting       :
- Api : `/api/OffersSetting/Update
- Method: `POST`
- Parameters:
 	
- Example :
      `{
    "Id": 2043,
    "NameAr": "باقة  تجهيز  عروس",
    "NameEn": "باقة  تجهيز  عروس",
    "Cost": null,
    "Value": 3000,
    "NGBStartDate": {
        "year": 2019,
        "month": 4,
        "day": 30
    },
    "NGBEndDate": {
        "year": 2020,
        "month": 4,
        "day": 30
    },
    "PercentageDiscountValue": null,
    "MaxDiscountValue": null,
    "OfferType": 4,
    "Status": 1,
    "IsOnlineOnly": 0,
    "accountId": null,
    "SponsoredOffer": null,
    "OffersAccounts": [
        {
            "Id": 2043,
            "AccountSetupId": 3095,
            "OffersSettingId": 2043,
            "OffersSetting": null,
            "AccountSetup": null,
            "RowState": 0
        }
    ],
    "OfferDetails": null,
    "Image": null,
    "ImagePath": null,
    "Promocode": null,
    "DescriptionEn": null,
    "DescriptionAr": null,
    "OffersDetails": [
        {
            "Id": 1082,
            "ComapnyCategoriesId": null,
            "CompanyServicesId": 213,
            "RetailProductId": null,
            "InOutOfferNature": 1,
            "RepetitionCount": 1,
            "OffersSettingId": 2043,
            "IsMandatory": 0,
            "OffersSetting": null,
            "ComapnyCategories": null,
            "CompanyServices": {
                "Id": 213,
                "CompanyId": 3089,
                "NameAr": "test new service 8",
                "NameEn": null,
                "CategoryId": 3550,
                "BookingMinutes": 30,
                "ProcessingMinutes": null,
                "FinishingMinutes": null,
                "AppServicesId": null,
                "Price": 40,
                "ShowOnline": false,
                "LoyaltyPoints": 0,
                "LoyaltyPointSystem": 0,
                "RankScore": 0,
                "DoneInHome": false,
                "PointsRedemption": null,
                "OnlineDescription": null,
                "TaxRate": 12,
                "STATUS": 0,
                "IsProcessTimeRequired": false
            },
            "RetailProduct": null,
            "RowState": 0,
            "AppServicesId": null
        }
    ],
    "StartDate": "2019-04-30T00:00:00.000Z",
    "EndDate": "2020-04-30T00:00:00.000Z"
}
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 
##   get  offer  by  id        :
- Api : `/api/OffersSetting/getById
- Method: `POST`
- Parameters:
 	
- Example :
      `1035
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": {
        "Id": 1035,
        "NameAr": "عرض علي باقه العروسه",
        "NameEn": "عرض علي باقه العروسه",
        "Image": null,
        "ImagePath": null,
        "DescriptionEn": null,
        "DescriptionAr": null,
        "Cost": null,
        "Value": 200,
        "StartDate": "2019-04-24T00:00:00",
        "EndDate": "2019-04-30T00:00:00",
        "Status": 1,
        "IsOnlineOnly": 0,
        "PercentageDiscountValue": 50,
        "MaxDiscountValue": 50,
        "SponsoredOffer": null,
        "Promocode": null,
        "RepetitionCount": null,
        "OfferType": 3,
        "OffersAccounts": [
            {
                "Id": 1035,
                "AccountSetupId": 3095,
                "OffersSettingId": 1035,
                "OffersSetting": null,
                "AccountSetup": {
                    "RegionTimeZoneId": 60,
                    "RegionTimeZone": null,
                    "CultureInfoId": 15,
                    "Culture": null,
                    "SiteNameCulture": null,
                    "AddressCulture": null,
                    "Country": null,
                    "PostCode": null,
                    "ManagerFirstName": null,
                    "ManagerFirstNameCulture": null,
                    "ManagerLastName": null,
                    "ManagerLastNameCulture": null,
                    "CompanyNameCulture": null,
                    "CompanyAddress": null,
                    "CompanyAddressCulture": null,
                    "CompanyRegionId": 0,
                    "Regions": null,
                    "CompanyStateId": 0,
                    "States": null,
                    "CompanyPostCode": null,
                    "CompanyPhoneNumber": null,
                    "CompanyMobileNumber": null,
                    "CompanyEmail": null,
                    "CompanyWebSiteAddress": null,
                    "CompanyOwnerFirstName": null,
                    "CompanyOwnerFirstNameCulture": null,
                    "CompanyOwnerLastName": null,
                    "CompanyOwnerLastNameCulture": null,
                    "SunStartTime": null,
                    "MonStartTime": null,
                    "TuesStartTime": null,
                    "WednesStartTime": null,
                    "ThursStartTime": null,
                    "FriStartTime": null,
                    "SatStartTime": null,
                    "SunEndTime": null,
                    "MonEndTime": null,
                    "TuesEndTime": null,
                    "WednesEndTime": null,
                    "ThursEndTime": null,
                    "FriEndTime": null,
                    "SatEndTime": null,
                    "IsSunOff": false,
                    "IsMonOff": false,
                    "IsTuesOff": false,
                    "IsWednesOff": false,
                    "IsThursOff": false,
                    "IsFriOff": false,
                    "IsSatOff": false,
                    "SpecificDays": null,
                    "AllowTransactionVoiding": false,
                    "TaxSystemOneId": null,
                    "TaxOneRate": null,
                    "TaxOneOrderInBill": null,
                    "TaxSystemOne": null,
                    "TaxSystemTwoId": null,
                    "TaxTwoRate": null,
                    "TaxTwoOrderInBill": null,
                    "TaxSystemTwo": null,
                    "TaxSystemThreeId": null,
                    "TaxThreeRate": null,
                    "TaxThreeOrderInBill": null,
                    "TaxSystemThree": null,
                    "AllowDeposit": false,
                    "AccountCurrencies": null,
                    "EnableTradingSystem": false,
                    "DeclareTheRegistersBeforeTrading": null,
                    "DeclareTheRegistersAfterTrading": null,
                    "PctPaidForServicePerformed": 0,
                    "PctPaidForProductSold": 0,
                    "GiveLoyaltyDollarsPointsToYourClientsOnEveryTransaction": false,
                    "NewClientsAreIncludedInTheLoyalitySystem": false,
                    "ServicePointsAndDollarsAreGivenForEveryXDollarSpent": 0,
                    "ServiceLoyaltyPointsGivenForEveryXDollarSpent": 0,
                    "ServiceLoyaltyMoneyGivenForEveryXDollarSpent": 0,
                    "ProductPointsAndDollarsAreGivenForEveryXDollarSpent": 0,
                    "ProductLoyaltyPointsGivenForEveryXDollarSpent": 0,
                    "ProductLoyaltyMoneyGivenForEveryXDollarSpent": 0,
                    "LoyaltyPointsAreGivenPerVisit": 0,
                    "LoyaltyMoneyAreGivenPerVisit": 0,
                    "XPointsCount": null,
                    "XPointsValue": null,
                    "DashboardDisplayAll": false,
                    "DashboardUpcomingAppointments": false,
                    "DashboardPendingOnlineBookings": false,
                    "DashboardSalesGraphs": false,
                    "DashboardTasks": false,
                    "DashboardWaitingLists": false,
                    "PrintReceiptAdvertText": null,
                    "PrintReceiptAdvertTextCulture": null,
                    "PrintReceiptIncludeClientphoneNumber": false,
                    "MessageOnNewBooking": false,
                    "MessageOnReminder": false,
                    "MessageOnBirthDay": false,
                    "MessageOnWelcome": false,
                    "MessageOnRetention": false,
                    "DoYouHaveReceiptPrinter": false,
                    "DoYouHaveCashDrawer": false,
                    "WorkingHours": null,
                    "BusinessSize": 48,
                    "BusinessType": 2,
                    "CompanyId": 3089,
                    "Company": null,
                    "Id": 3095,
                    "SiteName": "dodo",
                    "PhoneNumber": null,
                    "MobileNumber": null,
                    "Email": null,
                    "WebsiteAddress": null,
                    "CompanyName": null,
                    "State": 1,
                    "RankScore": null,
                    "Latitude": null,
                    "Longitude": null,
                    "CityId": 1054,
                    "AreaId": 1605,
                    "HaseInHomeService": 0,
                    "ServedGender": null,
                    "HasTaxiService": 0,
                    "MostPopular": null,
                    "MakeupArtist": 0,
                    "Address": "24 trablos street abbas el akkad",
                    "CountryId": 1938,
                    "HasOffer": false,
                    "IsFavorite": false,
                    "DefaultStartTime": null,
                    "DefaultEndTime": null,
                    "DefaultCurrency": null,
                    "ActiveClassifications": null,
                    "AccountImages": null,
                    "AddedDate": "2019-04-25T12:08:13.17",
                    "ModifiedDate": "2019-04-25T12:08:13.17",
                    "IPAddress": null,
                    "AddUserId": "3115",
                    "ModifyUserId": "3115"
                },
                "RowState": 0
            }
        ],
        "OffersDetails": [
            {
                "Id": 1069,
                "ComapnyCategoriesId": null,
                "CompanyServicesId": 6421,
                "RetailProductId": null,
                "InOutOfferNature": 1,
                "RepetitionCount": 1,
                "OffersSettingId": 1035,
                "IsMandatory": 0,
                "OffersSetting": null,
                "ComapnyCategories": null,
                "CompanyServices": null,
                "RetailProduct": null,
                "RowState": 0,
                "AppServicesId": null
            },
            {
                "Id": 1070,
                "ComapnyCategoriesId": null,
                "CompanyServicesId": 6418,
                "RetailProductId": null,
                "InOutOfferNature": 1,
                "RepetitionCount": 1,
                "OffersSettingId": 1035,
                "IsMandatory": 0,
                "OffersSetting": null,
                "ComapnyCategories": null,
                "CompanyServices": null,
                "RetailProduct": null,
                "RowState": 0,
                "AppServicesId": null
            },
            {
                "Id": 1071,
                "ComapnyCategoriesId": null,
                "CompanyServicesId": 6413,
                "RetailProductId": null,
                "InOutOfferNature": 1,
                "RepetitionCount": 1,
                "OffersSettingId": 1035,
                "IsMandatory": 0,
                "OffersSetting": null,
                "ComapnyCategories": null,
                "CompanyServices": null,
                "RetailProduct": null,
                "RowState": 0,
                "AppServicesId": null
            }
        ],
        "ClientPromotions": null,
        "ClientProducts": null,
        "ClientServices": null,
        "ClientPackage": null,
        "TransactionsProductOrServices": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 

##   Get Packages      :
- Api : `/api/OffersSetting/GetPackages
- Method: `POST`
- Parameters:
 	
- Example :
      `{"AccountSetupId":3095 }
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": [
        {
            "DescriptionEn": null,
            "DescriptionAr": null,
            "Id": 2043,
            "NameAr": "باقة  تجهيز  عروس",
            "NameEn": "باقة  تجهيز  عروس",
            "Value": 3000,
            "Services": null,
            "BookingService": null,
            "RetailProducts": null
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 
##   Create category      :
- Api : `/api/Category/Create
- Method: `POST`
- Parameters:
 	
- Example :
      `{
    "Id": 0,
    "NameAr": "خدمات إضافية للشعر  3",
    "Name": null,
    "NameEn": "خدمات إضافية للشعر  3",
    "OnlineDescription": "خدمات إضافية للشعر",
    "OnlineDetails": null,
    "Status": 1,
    "CatType": 2,
    "ClassId": 1929,
    "PointsRedemption": 0,
    "LoyaltyPoints": null,
    "LoyaltyMoney": null,
    "LoyaltyPointSystem": null,
    "ComapnyCategoriesId": null,
    "AccountSetupId": 3095
}
`
	- *faild*:
	` {
    "Result": null,
    "DetailedMessages": [
        {
            "Property": "UnuniqueDESC",
            "Meta": "Service description must be unique",
            "MetaPlus": ""
        }
    ],
    "Code": null,
    "Exception": null,
    "State": 102,
    "ValidationResult": null
}`
	- *success*:
    `{
    "Result": {
        "Id": 4555,
        "AccountSetupId": 3095,
        "Name": "4دمات إضافية للشعر  3",
        "NameAr": "4دمات إضافية للشعر  3",
        "NameEn": "4دمات إضافية للشعر  3",
        "OnlineDescription": "خدمات إضافية للشعر",
        "OnlineDetails": null,
        "Status": 1,
        "ClassificationCode": 0,
        "CatType": 2,
        "ClassId": 1929,
        "PointsRedemption": 0,
        "LoyaltyPoints": 0,
        "LoyaltyMoney": 0,
        "LoyaltyPointSystem": null,
        "AppCategoriesId": null,
        "ComapnyCategoriesId": 2033,
        "Services": null,
        "Products": null,
        "AddedDate": "2019-05-07T14:44:47.3411449+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": "3115",
        "ModifyUserId": null
    },
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 

##   Update category      :
- Api : `/api/Category/Update
- Method: `POST`
- Parameters:
 	
- Example :
      `{
        "Id": 4555,
        "AccountSetupId": 3095,
        "Name": "4دمات إضافية للشعر  3",
        "NameAr": "4دمات إضافية للشعر  3",
        "NameEn": "4دمات إضافية للشعر  3",
        "OnlineDescription": "خدمات إضافية للشعر",
        "OnlineDetails": null,
        "Status": 1,
        "ClassificationCode": 0,
        "CatType": 2,
        "ClassId": 1929,
        "PointsRedemption": 0,
        "LoyaltyPoints": 0,
        "LoyaltyMoney": 0,
        "LoyaltyPointSystem": null,
        "AppCategoriesId": null,
        "ComapnyCategoriesId": 2033,
        "Services": null,
        "Products": null,
        "AddedDate": "2019-05-07T14:44:47.3411449+02:00",
        "ModifiedDate": null,
        "IPAddress": null,
        "AddUserId": "3115",
        "ModifyUserId": null
    }
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 
##  edit  booking by adding services        :
- Api : `/api/BookingService/Create
- Method: `POST`
- Parameters:
 - Example simple required model :
      `[
    {
        "Id": 0,
        "ServiceId": 6407,
        "BookingId": 5414,
        "Price": 12,
        "Quantity": 1,
        "Date": "2019-05-13T00:00:00.000Z",
        "AccountSetupId": 3095
    }
]
`	
- Example full model :
      `[
    {
        "Id": 0,
        "ServiceId": 6408,
        "BookingId":5394,
        "OffersSettingId": null,
        "Price": 12,
        "TaxRate": 0,
        "Quantity": 1,
        "PackageID": null,
        "EstimatedTime": 45,
        "EmployeeId": "3138",
        "NGBDate": {
            "year": 2019,
            "month": 5,
            "day": 8
        },
        "Time": "15:30:00",
        "Date": "2019-05-08T00:00:00.000Z",
        "AccountSetupId": 3095
    }
]
`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 

##  edit  booking by deleting  services        :
- Api : `/api/BookingService/Delete
- Method: `POST`
- Parameters:
 	
- Example :
      `5753`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 
##  Get Dropdown (lookup)  :
- Api : `/api/SystemCodes/GetDropdown
- Method: `POST`
- Parameters:`
2	=TimeZone	,
3	=Culture	,
4	=County	,
5	=SalesTaxSystem	,
6	=CurrncySystem	,
7	=PaymentType	,
8	=TransactionType	,
9	=Cities	,
10	=Area	,
11	=Classifications	,
12	=Memberships	,
13	=BusinessTypes	,
14	=BusinessSizes	`
 	
- Example :
      `{"MasterCodeId":13}`
	- *faild*:
	` `
	- *success*:
    `{
    "Result": [
        {
            "dbId": 1949,
            "id": "3",
            "cid": "",
            "cnid": null,
            "na": "Beauty Salon"
        },
        {
            "dbId": 1950,
            "id": "4",
            "cid": "",
            "cnid": null,
            "na": "Laser Clinicn"
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
` 
##  Get categories with services  for account    :
- Api : `api/Category/GlameraGategories`
- Method: `POST`
- Example:
        `{"AccountSetupId":14}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "NameAr": "4دمات إضافية للشعر  3",
            "NameEn": "4دمات إضافية للشعر  3",
            "OnlineDescription": "خدمات إضافية للشعر",
            "Id": 4555,
            "AccountSetupId": 3095,
            "Services": []
        },
        {
            "NameAr": "خدمات إضافية للشعر  3",
            "NameEn": "خدمات إضافية للشعر  3",
            "OnlineDescription": "خدمات إضافية للشعر",
            "Id": 4554,
            "AccountSetupId": 3095,
            "Services": [
                {
                    "Id": 7437,
                    "AccountSetupId": 3095,
                    "NameAr": "x  service ",
                    "NameEn": "  service x",
                    "Price": 50,
                    "Description": null,
                    "BookingMinutes": 30
                }
            ]
        }
    ],
    "DetailedMessages": null,
    "Code": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

##  Glamera Business Forget UserName    :
- Api : `api/Account/GlameraBusinessForgetUserName`
- Method: `POST`
- Example:
        `{ "Email": "test", "MobileNumber": "01100797944" }`
 
 - Response : 
	- *Success*:
    `{
    "Result": {
        "Email": "Es************m",
        "MobileNumber": "011******44",
        "UserName": null,
        "CountryCode": null
    },
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
##  Get AccountInfo     :
- Api : `api/AccountSetup/GetAccountInfo`
- Method: `POST`
- Example:
        `{"Id":3095}`
 
 - Response : 
	- *Success*:
    `{
    "Result": {
        "Id": 3095,
        "SiteName": "Lili",
        "PhoneNumber": null,
        "MobileNumber": null,
        "Email": null,
        "CompanyName": "Lili",
        "BusinessSize": 51,
        "BusinessType": 1,
        "Address": null,
        "WebsiteAddress": null,
        "CompanyId": 3089,
        "CityId": null,
        "AreaId": null,
        "CountryId": 1938,
        "RankScore": null,
        "Latitude": null,
        "Longitude": null,
        "DefaultCurrency": {
            "NameAr": "L.E",
            "NameEn": "L.E",
            "Id": 3104
        },
        "MainImage": null
    },
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`




##  Update Service Price     :
- Api : `api/Booking/UpdateServicePrice`
- Method: `POST`
- Example:
        `{
    "ServiceId": "2359",
    "NewPrice": "96",
    "ModifyComment": "comment  on  price modify  ",
    "PriceModifiedBy": null,
    "PriceModifyReasonId": null
}`
 
 - Response : 
	- *Success*:
    `{
    "Result": true,
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`

## get Hot Line contacts   :
- Api : `api/GlameraHome/GBHotLine`
- Method: `POST`
- Example:
        `{}`
 
 - Response : 
	- *Success*:
    `{
    "Result": [
        {
            "MobileNumber": "01215545454",
            "PhoneNumber": "0244447"
        }
    ],
    "DetailedMessages": null,
    "MessageCode": null,
    "Message": null,
    "Exception": null,
    "State": 100,
    "ValidationResult": null
}
`
## Get Account Payments Detail:
- Api : `api/AccountSubscriptionPayment/GetAccountPaymentsDetail`
- Method: `POST`
- Example:
        `{"AccountSetupId":7241}`
 
 - Response : 
	- *Success*:
    `{
    "AccountPayments": [
        {
            "Amount": 1500,
            "CurrencyId": 0,
            "CurrencyName": "L.E",
            "TransDate": "2019-06-25T09:29:35.083"
        },
        {
            "Amount": 700,
            "CurrencyId": 0,
            "CurrencyName": "L.E",
            "TransDate": "2019-06-25T09:40:32.287"
        }
    ],
    "Summary": {
        "NextDueDate": "2019-07-25T09:29:35.083",
        "LastPaymentDate": "0001-01-01T00:00:00",
        "StartDate": "2019-06-23T10:53:49.827",
        "Status": 1,
        "NoOfMonths": 1,
        "SubscriptionNameAr": "sub 1",
        "SubscriptionNameEn": "sub 1"
    }
}

`


## dash board main  screen  (summary):
- Api : `api/DashboardIndicator/GetSummary`
- Method: `POST`
- Example:
        `{"IntervalCode":"10" ,   "AccountSetupId": 14}`
        `public enum TimeIntervals
   {
       CurrentDay = 1,
       PreviousDay = 2,
       NextDay = 3,
       CurrentWeek = 4,
       PreviousWeek = 5,
       NextWeek = 6,
       CurrentMonth = 7,
       PreviousMonth = 8,
       NextMonth = 9,
       CurrentYear = 10,
       PreviousYear = 11,
       NextYear = 12,
   }

`
 
 - Response : 
	- *Success*:
    `{
    "DashboardIndicators": [
        {
            "Id": 9,
            "Code": "10",
            "NameAr": "طلبات الحجز ",
            "NameEn": "Orders ",
            "ResultValue": "0",
            "Al_tamyizAr": "",
            "Al_tamyizEn": "",
            "ValueIsCurrency": false,
            "IconUrl": "",
            "GraphType": 1,
            "IndicatorOwner": 2,
            "Status": 1,
            "AccountSetupId": null
        },
        {
            "Id": 10,
            "Code": "11",
            "NameAr": "الدخل ",
            "NameEn": "Income ",
            "ResultValue": null,
            "Al_tamyizAr": "",
            "Al_tamyizEn": "",
            "ValueIsCurrency": true,
            "IconUrl": "",
            "GraphType": 1,
            "IndicatorOwner": 2,
            "Status": 1,
            "AccountSetupId": null
        },
        {
            "Id": 11,
            "Code": "12",
            "NameAr": "الخدمات  ",
            "NameEn": "Services ",
            "ResultValue": null,
            "Al_tamyizAr": "",
            "Al_tamyizEn": "",
            "ValueIsCurrency": false,
            "IconUrl": "",
            "GraphType": 1,
            "IndicatorOwner": 2,
            "Status": 1,
            "AccountSetupId": null
        },
        {
            "Id": 12,
            "Code": "13",
            "NameAr": "التقيمات  ",
            "NameEn": "Reviews ",
            "ResultValue": null,
            "Al_tamyizAr": "",
            "Al_tamyizEn": "",
            "ValueIsCurrency": false,
            "IconUrl": "",
            "GraphType": 1,
            "IndicatorOwner": 2,
            "Status": 1,
            "AccountSetupId": null
        }
    ],
    "TotalCount": 0,
    "DetailedMessages": null,
    "MessageCode": null,
    "Exception": null,
    "State": 100
}
`
